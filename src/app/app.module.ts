import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { UiComponentsModule } from '@certemy/ui-components';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedCommonModule } from '@certemy/common';
import { StandaloneModalComponent } from '../stories/modal/standalone-modal/standalone-modal.component';
import { CustomCellComponent } from '../stories/smart-table/custom-cell.component';

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot([]),
    UiComponentsModule,
    NgbModule,
    BrowserAnimationsModule,
    SharedCommonModule,
  ],
  providers: [],
  declarations: [AppComponent, StandaloneModalComponent, CustomCellComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
