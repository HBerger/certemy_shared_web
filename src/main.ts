import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

import cssVars from 'css-vars-ponyfill';
import { CSS_VARIABLES_NAMES } from '@certemy/common/src/lib/constants/constants';

if (environment.production) {
  enableProdMode();
}

cssVars({
  variables: {
    [CSS_VARIABLES_NAMES.FONT_BASE]: '"Noto Sans", sans-serif',
    [CSS_VARIABLES_NAMES.FONT_HEADING]: '"Merriweather", serif',
  },
  watch: true,
});

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.log(err));
