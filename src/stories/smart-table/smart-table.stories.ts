import { moduleMetadata } from '@storybook/angular';
import { SharedCommonModule } from '@certemy/common';
import { DYNAMIC_CELL_PROVIDERS, SmartTable, SmartTableModule } from '@certemy/ui-components';
import { action } from '@storybook/addon-actions';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import ColumnTypeId = SmartTable.ColumnTypeId;
import { ResolveTemplateVarsComponent } from '../helpers/resolve-template-vars.component';
import { CustomCellComponent } from './custom-cell.component';

const styles = [
    `
  cm-smart-table {
    height: 100%;
  }
`,
];

const rows: SmartTable.Row[] = [
    {
        prop1: 11,
        prop2: { inner: 'inner prop1' },
        prop4: '2010-10-10',
        prop5: 15,
        active: true,
        checked: true,
        prop8: 'privateVal_11',
        id: 1,
    },
    {
        prop1: 21,
        prop2: { inner: 'inner prop2' },
        prop4: '2010-10-10',
        prop5: 15,
        active: false,
        checked: true,
        prop8: 'privateVal_21',
        id: 2,
    },
    {
        prop1: 31,
        prop2: { inner: 'inner prop3' },
        prop4: '2010-10-10',
        prop5: 15,
        active: false,
        checked: false,
        prop8: 'privateVal_31',
        id: 3,
    },
    {
        prop1: 41,
        prop2: { inner: 'inner prop4' },
        prop4: '2010-10-10',
        prop5: 15,
        active: true,
        checked: false,
        prop8: 'privateVal_41',
        id: 4,
    },
];

export default {
    title: 'Smart Table/Smart Table',

    decorators: [
        moduleMetadata({
            imports: [SharedCommonModule, SmartTableModule, NgbModule],
            declarations: [ResolveTemplateVarsComponent, CustomCellComponent],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const TypeString = () => ({
    template: `
                <p>'value' is used as path in lodash.get</p>
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            {
                name: 'string with explicit type',
                value: 'prop2.inner',
                type: ColumnTypeId.STRING,
            },
            {
                name: 'string with default type',
                value: 'prop2.inner',
            },
        ],
    },
});

TypeString.storyName = 'Type STRING';

export const TypeActions = () => ({
    template: `
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            { name: 'ACTIONS', value: [{ type: 'EDIT', name: 'Edit' }], type: ColumnTypeId.ACTIONS },
            {
                name: 'computed ACTIONS',
                value: [
                    (row: SmartTable.Row) =>
                        row.active ? { type: 'DISABLE', name: 'Disable' } : { type: 'ENABLE', name: 'Enable' },
                ],
                type: ColumnTypeId.ACTIONS,
            },
        ],
    },
});

TypeActions.storyName = 'Type ACTIONS';

export const TypeDate = () => ({
    template: `
                <p>'value' is used as path in lodash.get</p>
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            {
                name: 'DATE',
                value: 'prop4',
                type: ColumnTypeId.DATE,
            },
        ],
    },
});

TypeDate.storyName = 'Type DATE';

export const TypeCustom = () => ({
    template: `
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            {
                name: 'CUSTOM',
                value: (row: SmartTable.Row) => `Format_${row.prop5}_as_pleased`,
                type: ColumnTypeId.CUSTOM,
            },
        ],
    },
});

TypeCustom.storyName = 'Type CUSTOM';

export const TypeCheck = () => ({
    template: `
                <p>'value' is used as path in lodash.get</p>
                <p>rows is not mutated, change 'checked' value to check/uncheck</p>
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                <ng-template #cellTemplate  let-row="row">
                  <span>template cell</span>
                </ng-template>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            {
                name: 'Check',
                value: 'checked',
                type: ColumnTypeId.CHECK,
                disable: (row) => !row.active,
            },
        ],
    },
});

TypeCheck.storyName = 'Type CHECK';

export const TypeCustomTemplate = () => ({
    template: `
                <cm-resolve-template-vars
                    [templateVar]="{cellTemplate: cellTemplate}"
                    (resolved)="ngAfterViewInit($event)">
                </cm-resolve-template-vars>
                <cm-smart-table
                  *ngIf="headers"
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                <ng-template #cellTemplate  let-row="row">
                  <span>template cell</span>
                  <pre>{{row | json}}</pre>
                </ng-template>
                `,
    styles,
    props: {
        rows,
        action,
        ngAfterViewInit({ cellTemplate }) {
            this.headers = [
                {
                    name: 'TEMPLATE',
                    value: cellTemplate,
                    type: ColumnTypeId.TEMPLATE,
                },
            ];
        },
        headers: null,
    },
});

TypeCustomTemplate.storyName = 'Type custom TEMPLATE';

export const TypeCustomComponent = () => ({
    moduleMetadata: {
        providers: [
            {
                provide: DYNAMIC_CELL_PROVIDERS,
                useValue: [CustomCellComponent],
                multi: true,
            }
        ]
    },
    template: `
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [{ name: 'custom COMPONENT', value: 'prop8', type: CustomCellComponent.type }],
    },
});

TypeCustomComponent.storyName = 'Type custom COMPONENT';

export const LongHeaderNameNgTooltip = () => ({
    template: `
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            {
                name: 'extra long name should be truncated and available on hover by ngTooltip',
                value: 'prop1',
            },
        ],
    },
});

LongHeaderNameNgTooltip.storyName = 'Long Header name (ngTooltip)';

export const Sorting = () => {
    const header1 = { name: 'header1', value: 'prop1', sortable: true };
    const orderedHeader = { header: header1, order: SmartTable.Order.SORTABLE };

    return {
        template: `
                <p>'orderedHeader' is not mutated, update 'orderedHeader' to sort table</p>
                <cm-smart-table
                  [headers]="headers"
                  [rows]="rows"
                  [order]="orderedHeader"
                  (orderChange)="action('orderChange')($event)"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
        styles,
        props: {
            rows,
            action,
            orderedHeader,
            headers: [
                orderedHeader.header,
                { name: 'header2', value: 'prop2' },
                { name: 'header4', value: 'prop3', sortable: false },
                { name: 'header5', value: 'prop5', sortable: true },
            ],
        },
    };
};

export const Popover = () => ({
    template: `
                <cm-resolve-template-vars
                    [templateVar]="{popoverTemplate: popoverTemplate}"
                    (resolved)="ngAfterViewInit($event)">
                </cm-resolve-template-vars>
                <cm-smart-table
                  *ngIf="headers"
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                <ng-template #popoverTemplate let-header="header" let-row="row">
                  <h3>Header</h3>
                  <ul>List
                    <li>item1</li>
                    <li>item2</li>
                    <li>item2</li>
                  </ul>
                  <br>
                  Header name: {{header.name}}
                  <br>
                  Row: {{row | json}}
                </ng-template>
                `,
    styles,
    props: {
        rows,
        action,
        ngAfterViewInit({ popoverTemplate }) {
            this.headers = [
                {
                    name: 'Popover',
                    value: 'prop1',
                    popover: { template: popoverTemplate, placement: 'left' },
                },
            ];
        },
        headers: null,
    },
});

export const Tooltip = () => ({
    template: `
                <cm-smart-table
                  *ngIf="headers"
                  [headers]="headers"
                  [rows]="rows"
                  (onCellAction)="action('onCellAction')($event)"
                  (onRowClick)="action('onRowClick')($event)">
                </cm-smart-table>
                `,
    styles,
    props: {
        rows,
        action,
        headers: [
            {
                name: 'Tooltip',
                value: 'prop1',
                tooltip: (row) => row.id,
            },
        ],
    },
});

export const WithRowDetails = () => ({
    template: `
      <cm-smart-table
        [headers]="headers"
        [rows]="rows"
        [rowDetailsRef]="rowDetailsRef"
        [config]="config"
        (onCellAction)="action('onCellAction')($event)"
        (onRowClick)="action('onRowClick')($event)">
      </cm-smart-table>
      <ng-template #rowDetailsRef>
        <div>This is row description</div>
      </ng-template>
    `,
    styles,
    props: {
        rows,
        action,
        config: {
            getBorderLeftColor: (row) => (!row.active ? '#d8635c' : '#5cd1d8'),
        },
        headers: [
            { name: 'header2', value: 'prop2.inner' },
            { name: 'header4', value: 'prop3' },
            { name: 'header5', value: 'prop5' },
        ],
    },
});

WithRowDetails.storyName = 'With row details';
