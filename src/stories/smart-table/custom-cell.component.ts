import { Component, EventEmitter, Input, Output } from '@angular/core';
import _get from 'lodash-es/get';
import { defaultIfEmpty, SmartTable } from '@certemy/ui-components';

@Component({
  selector: 'cm-custom-cell',
  template: `
    <span (click)="onClick($event)">i'm a custom component</span>
    <pre>{{getValue | thunk: data | json}}</pre>
  `,
})
export class CustomCellComponent {
  static type = 'SOME_PRIVATE_VAR';

  @Input() data: SmartTable.DynamicCellData;
  @Output() action = new EventEmitter();

  getValue = ({ header, row }: SmartTable.DynamicCellData) => {
    return defaultIfEmpty(_get(row, header.value));
  };

  onClick(event) {
    event.stopPropagation();
    this.action.emit({ val: 1, val2: 2 });
  }
}
