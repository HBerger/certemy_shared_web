import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { PortalModule } from '@certemy/ui-components';
import { SharedCommonModule } from '@certemy/common';
import { InjectorComponent } from './helpers/injector.component';
import { importProvidersFrom } from '@angular/core';

export default {
    title: 'Portal',

    decorators: [
        applicationConfig({
          providers: [
            importProvidersFrom(PortalModule.forRoot()),
          ]
        }),
        moduleMetadata({
            imports: [SharedCommonModule, PortalModule],
            declarations: [InjectorComponent],
        }),
    ],
};

export const Example = () => ({
    template: `
                  <div class="row">
                    <div class="col">
                      <p>'Template' home</p>
                      <div *cmPortal="'portal-name'">
                        <p>'Template' that want to travel by portal</p>
                      </div>
                    </div>
                    <div class="col">
                      <p>'Template' destination</p>
                      <cm-portal-outlet name="portal-name"></cm-portal-outlet>
                    </div>
                  </div>
                `,
    props: {},
});
