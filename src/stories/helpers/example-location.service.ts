import { Injectable } from '@angular/core';
import { LocationService, SmartForm } from '@certemy/ui-components';
import { of } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class ExampleLocationService extends LocationService {
  getCountries(): Observable<SmartForm.FieldOption[]> {
    return of([
      { id: 1, name: 'Argentina' },
      { id: 2, name: 'Belarus' },
      { id: 3, name: 'Iceland' },
      { id: 4, name: 'USA' },
    ]);
  }

  getStatesOrCities(countryId) {
    return of({
      states: [
        { id: 1, name: 'Alaska' },
        { id: 2, name: 'Arizona' },
      ],
      cities: [
        { id: 1, name: 'New York' },
        { id: 2, name: 'Washington' },
      ],
      countryId,
    });
  }

  getCitiesByState(stateId) {
    return of({
      cities: [
        { id: 1, name: 'New York' },
        { id: 2, name: 'Washington' },
      ],
      stateId,
    });
  }
}
