import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';

function mapValues(object: Object, fn: Function): Object {
  return Object.entries(object)
    .map(([k, v]) => [k, fn(v)])
    .reduce((acc, [k, v]) => ({ ...acc, [k]: v }), {});
}

@Component({
  selector: 'cm-injector',
  template: '',
})
export class InjectorComponent implements OnInit {
  @Input() tokens: { [name: string]: any } = {};
  @Output() resolved: EventEmitter<{ [name: string]: any }> = new EventEmitter();

  constructor(private injector: Injector) {}

  ngOnInit() {
    const injected = mapValues(this.tokens, token => this.injector.get(token));
    this.resolved.emit(injected);
  }
}
