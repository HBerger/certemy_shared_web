import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cm-resolve-template-vars',
  template: '',
})
export class ResolveTemplateVarsComponent {
  @Input()
  set templateVar(vars: { [name: string]: any }) {
    this.resolved.emit(vars);
  }

  @Output() resolved: EventEmitter<{ [name: string]: any }> = new EventEmitter();
}
