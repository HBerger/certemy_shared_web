import { moduleMetadata } from '@storybook/angular';
import { ColorPickerModule } from '@certemy/ui-components';
import _take from 'lodash-es/take';
const Styles = [];
const defaultColor = 'rgb(92, 209, 216)';

export default {
    title: 'Color Picker',

    decorators: [
        moduleMetadata({
            imports: [ColorPickerModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const ColorPickerExample = () => ({
    template: `
      <cm-color-picker
        [color]="color"
        [presetColors]="presetColors"
        [placement]="'left top'"
        (colorPickerClosed)="colorPickerClosed($event)"
        (colorChanged)="colorChanged($event)">
      </cm-color-picker>
    `,
    props: {
        color: defaultColor,
        presetColors: [defaultColor],
        colorChanged(color: string) {
            this.color = color;
        },
        colorPickerClosed(color: string) {
            if (this.presetColors[0] !== color) {
                this.presetColors = _take([color, ...this.presetColors], 14);
            }
        },
    },
    styles: Styles,
});
