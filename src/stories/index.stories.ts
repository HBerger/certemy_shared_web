import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import {
    ActionTabsModule,
    PageAction,
    PageActionsModule,
    PaginationModule,
    SplitButtonAction,
    SplitButtonModule,
    TableTab,
    TableTabsModule,
    SpinnerModule,
    OptionDropdownModule,
    ToggleModule,
} from '@certemy/ui-components';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { withKnobs, number } from '@storybook/addon-knobs';
import { SharedCommonModule } from '@certemy/common';
import { FileDownloadDropdownModule } from '@certemy/ui-components/src/lib/file-download-dropdown/file-download-dropdown.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { importProvidersFrom } from '@angular/core';

const splitActions: SplitButtonAction[] = [
    {
        label: 'Split Me',
        name: 'split-me',
    },
    {
        label: 'One',
        name: 'one',
    },
    {
        label: 'Two',
        name: 'two',
    },
    {
        label: 'Four!',
        name: 'four',
    },
];

const tableTabs: TableTab[] = [
    {
        label: 'All',
        name: 'all',
        count: 10,
        active: true,
    },
    {
        label: 'Selected',
        name: 'selected',
        count: 5,
        active: false,
    },
];

const filterRowActions: PageAction[] = [
    {
        name: 'Primary Action',
        type: 'primary',
    },
    {
        name: 'First grouped',
        type: 'first-grouped',
    },
    {
        name: 'Second grouped',
        type: 'second-grouped',
    },
    {
        name: 'Third grouped',
        type: 'Third-grouped',
    },
    {
        name: 'Fourth grouped',
        type: 'Fourth-grouped',
    },
];

export default {
  title: 'General components/General components',

  decorators: [
    moduleMetadata({
      imports: [
        NgbModule,
        SplitButtonModule,
        TableTabsModule,
        PageActionsModule,
        PaginationModule,
        OptionDropdownModule,
        ToggleModule,
        SpinnerModule,
        FileDownloadDropdownModule,
        TableTabsModule,
        SharedCommonModule,
        ActionTabsModule,
      ],
    }),
    applicationConfig({
      providers: [
          importProvidersFrom(BrowserAnimationsModule)
      ],
    }),
  ],

  parameters: {
    layout: 'centered',
  },
};

export const Buttons = () => ({
    template: `<div class="d-flex justify-content-between">
                  <button type="button" class="btn btn-primary">Primary button</button>
                  <button type="button" class="btn btn-default">Default button</button>
                  <cm-split-button
                    [actions]="splitActions"
                    (actionClick)="onAction($event)">
                  </cm-split-button>
                  <button type="button" class="cm-link">Link button</button>
                  <a href="" class="cm-link">Link</a>
                </div>`,
    props: {
      splitActions,
      onAction: action('splitAction'),
    },
  });

export const TableTabs = () => ({
    template: `<cm-table-tabs [tabs]="tableTabs" (tabClick)="onTabClick($event)"></cm-table-tabs>`,
    props: {
      tableTabs,
      onTabClick(tabName): void {
        action('onTabClick')(tabName);
        this.tableTabs = tableTabs.map((tab) => ({
          ...tab,
          active: tab.name === tabName,
        }));
      },
    },
  });

TableTabs.storyName = 'Table tabs';

export const ActionsGroupStartingFromSomeAction = () => ({
    template: `<cm-page-actions
                    [actions]="filterRowActions"
                    [groupStart]="groupStart"
                    (action)="onAction($event)">
                  </cm-page-actions>`,
    props: {
      filterRowActions,
      // groupStart: number('groupStart', 1),
      onAction: action('filterRowActions'),
    },
  });

ActionsGroupStartingFromSomeAction.storyName = 'Actions (group starting from some action)';

export const Pagination = () => ({
    template: `<cm-pagination-displayed
                    [page]="pagination.page"
                    [collectionSize]="pagination.size">
                  </cm-pagination-displayed>
                  <br>
                  <cm-pagination
                    class="mobile"
                    [page]="pagination.page"
                    [collectionSize]="pagination.size"
                    (pageChange)="onPageChange($event)">
                  </cm-pagination>
                  <br>
                  <cm-pagination
                    [page]="pagination.page"
                    [collectionSize]="pagination.size"
                    (pageChange)="onPageChange($event)">
                  </cm-pagination>`,
    styles: [
      `cm-pagination-displayed {
          display: block;
        }`,
    ],
    props: {
      pagination: {
        page: 2,
        size: 87,
      },
      onPageChange: action('onPageChange'),
    },
  });

export const OptionDropdown = () => ({
    template: `
      <cm-option-dropdown (optionChanged)="optionChanged($event)" [options]="options"></cm-option-dropdown>
    `,
    styles: [],
    props: {
      options: [
        {
          id: 1,
          title: 'Create',
          icon: 'edit',
        },
        {
          id: 2,
          title: 'Add to Dashboard',
          icon: 'add-to-dashboard',
        },
        {
          id: 3,
          title: 'Duplicate',
          icon: 'copy',
        },
        {
          id: 4,
          title: 'Schedule',
          icon: 'calendar',
        },
        {
          id: 5,
          title: 'Download',
          icon: 'download',
          options: [
            {
              id: 6,
              title: 'Download SCV',
            },
            {
              id: 7,
              title: 'Download PDF',
            },
          ],
        },
        {
          separate: true,
          id: 3,
          title: 'Delete',
          icon: 'trashbin',
        },
      ],
      optionChanged: (optionId) => {
        console.log('Selected Option Id:', optionId);
      },
    },
  });

export const ToggleComponent = () => ({
    template: `
      <cm-toggle [visibleElem]="visibleElemRef" [hiddenElem]="hiddenElemRef"></cm-toggle>

      <ng-template #visibleElemRef>
        <div>Text 1</div>
      </ng-template>

      <ng-template #hiddenElemRef>
        <div>Text 2</div>
      </ng-template>
    `,
    styles: [],
  });

export const Spinner = () => ({
    template: `<cm-spinner></cm-spinner>`,
  });

export const FileDownloadDropdown = () => ({
    template: `<cm-file-download-dropdown title="Click me" [fileLinks]=fileLinks></cm-file-download-dropdown>`,
    props: {
      fileLinks: [
        { name: 'Image.png', link: 'https://' },
        { name: 'File.pdf', link: 'https://' },
      ],
    },
  });

export const _TableTabs = () => ({
    template: `<cm-table-tabs [tabs]="tableTabs" (tabClick)="onTabClick($event)"></cm-table-tabs>`,
    props: {
      tableTabs,
      onTabClick(tabName) {
        action('onTabClick')(tabName);
        this.tableTabs = tableTabs.map((tab) => ({
          ...tab,
          active: tab.name === tabName,
        }));
      },
    },
  });

_TableTabs.storyName = 'Table tabs';

export const ActionTabs = () => ({
    template: `
<cm-action-tabs [tabs]="tabs" [activeTabId]="activeTabId" (tabsChange)="onTabsChange($event)"></cm-action-tabs>
<div [ngSwitch]="activeTabId">
   <div *ngSwitchCase="PAGES.FIRST">This is first page</div>
   <div *ngSwitchCase="PAGES.SECOND">This is second page</div>
   <div *ngSwitchCase="PAGES.THIRD">This is third page</div>
</div>

<button class="btn-primary btn mt-5" (click)="onTabsChange(PAGES.THIRD)">GO TO LAST PAGE</button>
`,
    props: {
      PAGES: {
        FIRST: 1,
        SECOND: 2,
        THIRD: 3,
      },
      activeTabId: 1,
      tabs: [
        {
          id: 1,
          display: 'Tab 1',
        },
        {
          id: 2,
          display: 'Tab 2',
        },
        {
          id: 3,
          display: 'Tab 3',
        },
      ],
      onTabsChange(tabId) {
        this.activeTabId = tabId;
      },
    },
  });

ActionTabs.storyName = 'Action tabs';

export const NavTabs = () => ({
    template: `<p>This tabs are appropriate for navigation.</p> `,
  });

NavTabs.storyName = 'Nav tabs';
