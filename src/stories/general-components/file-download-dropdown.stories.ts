import { moduleMetadata } from '@storybook/angular';
import { FileDownloadDropdownModule } from '@certemy/ui-components/src/lib/file-download-dropdown/file-download-dropdown.module';

export default {
    title: 'General',

    decorators: [
        moduleMetadata({
            imports: [FileDownloadDropdownModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const FileDownloadDropdown = () => ({
    template: `<cm-file-download-dropdown title="Click me" [fileLinks]=fileLinks></cm-file-download-dropdown>`,
    props: {
        fileLinks: [
            { name: 'Image.png', link: 'https://' },
            { name: 'File.pdf', link: 'https://' },
        ],
    },
});
