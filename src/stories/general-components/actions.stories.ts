import { moduleMetadata } from '@storybook/angular';
import { PageAction, PageActionsModule } from '@certemy/ui-components';
import { action } from '@storybook/addon-actions';

const filterRowActions: PageAction[] = [
    {
        name: 'Primary Action',
        type: 'primary',
    },
    {
        name: 'First grouped',
        type: 'first-grouped',
    },
    {
        name: 'Second grouped',
        type: 'second-grouped',
    },
    {
        name: 'Third grouped',
        type: 'Third-grouped',
    },
    {
        name: 'Fourth grouped',
        type: 'Fourth-grouped',
    },
];

export default {
    title: 'General',

    decorators: [
        moduleMetadata({
            imports: [PageActionsModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const ActionsGroupStartingFromSomeAction = () => ({
    template: `<cm-page-actions
                  [actions]="filterRowActions"
                  [groupStart]="groupStart"
                  (action)="onAction($event)">
                </cm-page-actions>`,
    props: {
        filterRowActions,
        groupStart: 1,
        onAction: action('filterRowActions'),
    },
});

ActionsGroupStartingFromSomeAction.storyName = 'Actions (group starting from some action)';
