import { moduleMetadata } from '@storybook/angular';
import { OptionDropdownModule } from '@certemy/ui-components';

export default {
    title: 'General',

    decorators: [
        moduleMetadata({
            imports: [OptionDropdownModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const OptionDropdown = () => ({
    template: `
    <cm-option-dropdown (optionChanged)="optionChanged($event)" [options]="options"></cm-option-dropdown>
  `,
    styles: [],
    props: {
        options: [
            {
                id: 1,
                title: 'Create',
                icon: 'edit',
            },
            {
                id: 2,
                title: 'Add to Dashboard',
                icon: 'add-to-dashboard',
            },
            {
                id: 3,
                title: 'Duplicate',
                icon: 'copy',
            },
            {
                id: 4,
                title: 'Schedule',
                icon: 'calendar',
            },
            {
                id: 5,
                title: 'Download',
                icon: 'download',
                options: [
                    {
                        id: 6,
                        title: 'Download SCV',
                    },
                    {
                        id: 7,
                        title: 'Download PDF',
                    },
                ],
            },
            {
                separate: true,
                id: 3,
                title: 'Delete',
                icon: 'trashbin',
            },
        ],
        optionChanged: (optionId) => {
            console.log('Selected Option Id:', optionId);
        },
    },
});
