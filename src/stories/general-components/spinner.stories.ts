import { moduleMetadata } from '@storybook/angular';
import { SpinnerModule } from '@certemy/ui-components';

export default {
    title: 'General',

    decorators: [
        moduleMetadata({
            imports: [SpinnerModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const Spinner = () => ({
    template: `<cm-spinner></cm-spinner>`,
});
