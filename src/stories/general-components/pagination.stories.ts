import { moduleMetadata } from '@storybook/angular';
import { PaginationModule, CardFooterPaginationModule } from '@certemy/ui-components';
import { action } from '@storybook/addon-actions';

export default {
    title: 'General',

    decorators: [
        moduleMetadata({
            imports: [PaginationModule, CardFooterPaginationModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const Pagination = () => ({
    template: `
                <div class='page-size-wrapper'>
                <cm-pagination-displayed
                  [page]="pagination.page"
                  [pageSize]="pagination.pageSize"
                  [collectionSize]="pagination.size">
                </cm-pagination-displayed>
                <cm-pagination-page-size-selector
                  [collectionSize]="pagination.size"
                  [pageSize]="pagination.pageSize"
                  (pageSizeChange)='setItemsPerPage($event)'>
                </cm-pagination-page-size-selector>
                </div>
                <br>
                <cm-pagination
                  class="mobile"
                  [page]="pagination.page"
                  [pageSize]="pagination.pageSize"
                  [collectionSize]="pagination.size"
                  (pageChange)="onPageChange($event)">
                </cm-pagination>
                <br>
                <cm-pagination
                  [page]="pagination.page"
                  [pageSize]="pagination.pageSize"
                  [collectionSize]="pagination.size"
                  (pageChange)="onPageChange($event)">
                </cm-pagination>
                <cm-card-footer-pagination
                    class="card-footer"
                    [collectionSize]="pagination.size"
                    [page]="pagination.page"
                    [pageSize]="pagination.pageSize"
                    (pageChange)="onPageChange($event)"
                    (pageSizeChange)="setItemsPerPage($event)"
                    [pageSizeSelectorActive]="true"
                    >
                </cm-card-footer-pagination>`,
    styles: [
        `cm-pagination-displayed {
        display: block;
      }
      .page-size-wrapper {
        display: flex;
        align-items: center;
        justify-content: space-around
      }
      `,
    ],
    props: {
        pagination: {
            page: 2,
            pageSize: 10,
            size: 87,
        },
        onPageChange(page) {
            action('onPageChange')(page);
            this.pagination.page = page;
        },
        setItemsPerPage(pageSize) {
            this.pagination.pageSize = pageSize;
        },
    },
});
