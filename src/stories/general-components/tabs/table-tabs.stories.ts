import { moduleMetadata } from '@storybook/angular';
import { TableTab, TableTabsModule } from '@certemy/ui-components';
import { action } from '@storybook/addon-actions';

const tableTabs: TableTab[] = [
    {
        label: 'All',
        name: 'all',
        count: 10,
        active: true,
    },
    {
        label: 'Selected',
        name: 'selected',
        count: 5,
        active: false,
    },
];

export default {
    title: 'General/Tabs',

    decorators: [
        moduleMetadata({
            imports: [TableTabsModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const TableTabs = () => ({
    template: `<cm-table-tabs [tabs]="tableTabs" (tabClick)="onTabClick($event)"></cm-table-tabs>`,
    props: {
        tableTabs,
        onTabClick(tabName) {
            action('onTabClick')(tabName);
            this.tableTabs = tableTabs.map((tab) => ({
                ...tab,
                active: tab.name === tabName,
            }));
        },
    },
});

TableTabs.storyName = 'Table tabs';
