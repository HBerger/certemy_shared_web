import { moduleMetadata } from '@storybook/angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ActionTabsModule } from '@certemy/ui-components';
import { SharedCommonModule } from '@certemy/common';

export default {
    title: 'General/Tabs',

    decorators: [
        moduleMetadata({
            imports: [SharedCommonModule, ActionTabsModule, NgbModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const ActionTabs = () => ({
    template: `
<cm-action-tabs [tabs]="tabs" [activeTabId]="activeTabId" (tabsChange)="onTabsChange($event)"></cm-action-tabs>
<div [ngSwitch]="activeTabId">
 <div *ngSwitchCase="PAGES.FIRST">This is first page</div>
 <div *ngSwitchCase="PAGES.SECOND">This is second page</div>
 <div *ngSwitchCase="PAGES.THIRD">This is third page</div>
</div>

<button class="btn-primary btn mt-5" (click)="onTabsChange(PAGES.THIRD)">GO TO LAST PAGE</button>
`,
    props: {
        PAGES: {
            FIRST: 1,
            SECOND: 2,
            THIRD: 3,
        },
        activeTabId: 1,
        tabs: [
            {
                id: 1,
                display: 'Tab 1',
            },
            {
                id: 2,
                display: 'Tab 2',
            },
            {
                id: 3,
                display: 'Tab 3',
            },
        ],
        onTabsChange(tabId) {
            this.activeTabId = tabId;
        },
    },
});

ActionTabs.storyName = 'Action tabs';
