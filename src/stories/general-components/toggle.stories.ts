import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { ToggleModule } from '@certemy/ui-components';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { importProvidersFrom } from '@angular/core';

export default {
    title: 'General',

    decorators: [
        applicationConfig({
          providers: [
            importProvidersFrom(BrowserAnimationsModule),
          ],
        }),
        moduleMetadata({
            imports: [ToggleModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const ToggleComponent = () => ({
    template: `
    <cm-toggle [visibleElem]="visibleElemRef" [hiddenElem]="hiddenElemRef"></cm-toggle>

    <ng-template #visibleElemRef>
      <div>Text 1</div>
    </ng-template>

    <ng-template #hiddenElemRef>
      <div>Text 2</div>
    </ng-template>
  `,
    styles: [],
});
