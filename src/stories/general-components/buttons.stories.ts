import { moduleMetadata } from '@storybook/angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SplitButtonAction, SplitButtonModule } from '@certemy/ui-components';
import { action } from '@storybook/addon-actions';

const splitActions: SplitButtonAction[] = [
    {
        label: 'Split Me',
        name: 'split-me',
    },
    {
        label: 'One',
        name: 'one',
    },
    {
        label: 'Two',
        name: 'two',
    },
    {
        label: 'Four!',
        name: 'four',
    },
];

export default {
    title: 'General',

    decorators: [
        moduleMetadata({
            imports: [NgbModule, SplitButtonModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const Buttons = () => ({
    template: `<div class="d-flex justify-content-between">
                <button type="button" class="btn btn-primary">Primary button</button>
                <button type="button" class="btn btn-default">Default button</button>
                <cm-split-button
                  [actions]="splitActions"
                  (actionClick)="onAction($event)">
                </cm-split-button>
                <button type="button" class="cm-link">Link button</button>
                <a href="" class="cm-link">Link</a>
              </div>`,
    props: {
        splitActions,
        onAction: action('splitAction'),
    },
});
