import { SmartFilters } from '@certemy/ui-components';
import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { SidebarModule, SidebarService, SmartFiltersModule, SmartFiltersService } from '@certemy/ui-components';
import { SharedCommonModule } from '@certemy/common';
import { importProvidersFrom, TemplateRef } from '@angular/core';
import { InjectorComponent } from './helpers/injector.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';

const filterOptionsA = [
    { id: 'firstOption', name: 'First option' },
    { id: 'secondOption', name: 'Second option' },
    { id: 'thirdOption', name: 'Third option' },
];

const filterOptionsB = [
    { id: 'option1', name: 'Option 1' },
    { id: 'option2', name: 'Option 2' },
    { id: 'option3', name: 'Option 3' },
    { id: 'option4', name: 'Option 4' },
    { id: 'option5', name: 'Option 5' },
];

const FILTER_CONFIG = [
    new SmartFilters.RangeFilter({
        name: 'range',
        label: 'Range',
        value: null,
    }),
    new SmartFilters.CheckboxGroupFilter({
        name: 'checkboxGroup1',
        label: 'Checkbox Group 1',
        value: null,
        options: [...filterOptionsA],
    }),
    new SmartFilters.CheckboxGroupFilter({
        name: 'checkboxGroup2',
        label: 'Checkbox Group 2',
        value: null,
        options: [...filterOptionsB],
    }),
    new SmartFilters.DateRangeFilter({
        name: 'dateRange',
        label: 'Date range',
        value: null,
    }),
    new SmartFilters.SelectFilter({
        name: 'select',
        label: 'Select',
        value: null,
        options: [{ id: '', name: 'Please select' }, ...filterOptionsA],
    }),
    new SmartFilters.TypeaheadFilter({
        name: 'typeahead',
        label: 'Typeahead',
        value: null,
        request$: () => of([...filterOptionsA]),
        formatter: (x) => x.name,
    }),
];

const Styles = [
    `
      .component {
        text-align: center;
        padding: 20px
      }
      .component-name{
        font-size: 20px;
        font-weight: bold;
        margin-bottom: 10px;
      }
      .sidebar-filters {
        height: 100%;
        box-shadow: 0 2px 20px 0 rgba(0, 0, 0, 0.15);
      }

      .filters-container {
        padding: 0 30px;
      }

      .header {
          padding: 30px 30px 20px 30px;
          display: flex;
          justify-content: space-between;
      }

      .title {
        font-family: $font-heading;
        font-size: 20px;
        font-weight: bold;
        letter-spacing: -0.5px;
        color: $color-text-primary;
      }

      .cm-link {
        margin-left: -6px;
      }
      .filters-value {
        text-align: left;
        padding: 50px 0 0 400px;
      }
      .active-filters-wrapper {
        margin-top: 15px;
        display: flex;
        justify-content: center;
      }
      .active-filters {
        max-width: 300px;
        margin-top: 15px;
        display: flex;
        flex-wrap: wrap;
        text-align: left;
      }
      `,
];

export default {
    title: 'Smart Filters',

    decorators: [
        applicationConfig({
          providers: [
            importProvidersFrom(SidebarModule.forRoot()),
            importProvidersFrom(BrowserAnimationsModule),
          ],
        }),
        moduleMetadata({
            imports: [SharedCommonModule, SidebarModule, SmartFiltersModule],
            declarations: [InjectorComponent],
        }),
    ],
};

export const Example = () => ({
    template: `
                <cm-injector [tokens]="tokens" (resolved)="resolved($event)"></cm-injector>
                <cm-sidebar style="height: 100vh">
                <div class="component">
                  <div class="component-name">Sidebar Filters</div>
                  <div class="component-container">
                   <cm-filter-button (click)="openSideBar(smartFilters)"></cm-filter-button>
                    <div class="active-filters-wrapper">
                    <div class="active-filters">
                       <cm-active-filters (removeLabel)="removeLabel($event)"></cm-active-filters>
                    </div>
                    </div>
                    <div class="filters-value"><strong>Filters value:</strong><br/><br/><pre>{{filtersValue | json}}</pre></div>
                  </div>
                </div>
                </cm-sidebar>
                <ng-template #smartFilters>
                  <div class="sidebar-filters">
                    <div class="header">
                      <div>
                        <h4 class="title">Filters</h4>
                        <button class="cm-link" (click)="onResetFilters()">Reset to default</button>
                      </div>
                      <button (click)="closeSideBar()" class="btn btn-primary">Close</button>
                    </div>
                    <div class="filters-container">
                      <cm-smart-filters (filtersChange)="filtersChange($event)">
                      </cm-smart-filters>
                    </div>
                  </div>
                </ng-template>
                `,
    styles: Styles,
    props: {
        tokens: {
            sidebarService: SidebarService,
            smartFiltersService: SmartFiltersService,
        },
        instances: {},
        resolved(instances) {
            this.instances = instances;
            this.instances.smartFiltersService.initialize(FILTER_CONFIG);
            this.instances.smartFiltersService.value$.subscribe((newValue) => (this.filtersValue = newValue));
        },

        openSideBar(templateRef: TemplateRef<any>) {
            this.instances.sidebarService.open(templateRef).subscribe();
        },
        closeSideBar() {
            this.instances.sidebarService.close();
        },

        filtersConfig: FILTER_CONFIG,
        filtersValue: {},

        filtersChange(event) {},

        removeLabel(event: SmartFilters.ChangeEvent<SmartFilters.FilterValue>) {
            const remove = confirm('Remove?');

            if (!remove) {
                event.preventDefault();
            }
        },
        onResetFilters() {
            const reset = confirm('Reset?');

            if (reset) {
                this.instances.smartFiltersService.resetFilters();
            }
        },
    },
});
