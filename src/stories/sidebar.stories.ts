import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { SidebarModule, SidebarService } from '@certemy/ui-components';
import { SharedCommonModule } from '@certemy/common';
import { importProvidersFrom, TemplateRef } from '@angular/core';
import { InjectorComponent } from './helpers/injector.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export default {
    title: 'Sidebar',

    decorators: [
        applicationConfig({
          providers: [
            importProvidersFrom(SidebarModule.forRoot()),
            importProvidersFrom(BrowserAnimationsModule),
          ],
        }),
        moduleMetadata({
            imports: [SharedCommonModule, SidebarModule],
            declarations: [InjectorComponent],
        }),
    ],
};

export const Example = () => ({
    template: `
                <cm-injector [tokens]="tokens" (resolved)="resolved($event)"></cm-injector>
                <cm-sidebar style="height: 500px">
                <div class="component">
                  <div class="component-name">Sidebar</div>
                  <div class="component-container">
                    <button (click)="openSideBar(sideTemplate)" class="btn btn-primary">open</button>
                  </div>
                </div>
                </cm-sidebar>
                <ng-template #sideTemplate>
                  <p>Hellow world</p>
                  <p>Hello2</p>
                  <p>Hello3</p>
                  <button (click)="closeSideBar()" class="btn btn-primary">close from inside</button>
                </ng-template>
                `,
    props: {
        tokens: { sidebarService: SidebarService },
        instances: {},
        resolved(instances) {
            this.instances = instances;
        },
        openSideBar(templateRef: TemplateRef<any>) {
            this.instances.sidebarService.open(templateRef).subscribe();
        },

        closeSideBar() {
            this.instances.sidebarService.close();
        },
    },
});
