import { moduleMetadata } from '@storybook/angular';
import { IconModule, PreloaderModule } from '@certemy/ui-components';
import { SharedCommonModule } from '@certemy/common';

export default {
  title: 'SVG icons',

  decorators: [moduleMetadata({
    imports: [SharedCommonModule, IconModule, PreloaderModule],
  })],

  parameters: {
    layout: 'centered',
  },
};

export const Icons = () => ({
    template: `<div class="wrapper">
                  <div *ngFor="let icon of icons;" class="wrapper__item">
                    <cm-icon [name]="icon"></cm-icon>
                    <span class="description">{{icon}}</span>
                  </div>
                </div>`,
    styles: [
      `.wrapper {
          display: flex;
          align-item: center;
          flex-wrap: wrap;
        }
        .wrapper__item {
          margin: 20px;
          display: flex;
          flex-direction: column;
        }
        .description {
          margin: auto;
        }
        cm-icon {
          color: #588fdf;
          height: 60px;
          width: 60px;
        }`,
    ],
    props: {
      icons: [
        'add-to-dashboard',
        'alignCenter',
        'alignLeft',
        'alignRight',
        'arrow-down',
        'arrow-in-circle',
        'attachment',
        'bread-home',
        'calendar',
        'center',
        'certification-assignment',
        'chevron-right',
        'clock-circular',
        'clock',
        'close-thin',
        'close',
        'company-logo-placeholder',
        'completion',
        'copy',
        'credentialing-icon',
        'download',
        'drag-arrow',
        'dynamic-form',
        'edit-circle',
        'edit',
        'education-step',
        'evidence-file',
        'export',
        'external-link',
        'fee-step',
        'files',
        'filter',
        'form',
        'info-circle',
        'invite-profile',
        'list',
        'lock',
        'log-step',
        'more-option',
        'note',
        'one-in-circle',
        'pie',
        'plus',
        'print',
        'reg-form-step',
        'settings-icon',
        'settings',
        'step-type',
        'test-step',
        'testing-icon',
        'trashbin',
        'two-in-circle',
        'up',
        'upload-arrow',
        'upload',
        'user-guide-icon',
        'user-icon',
        'user',
        'verification-step',
        'vouchers',
      ],
    },
  });

Icons.storyName = 'icons';

// export default {
//   title: 'SVG icons',
//
//   decorators: [moduleMetadata({
//     imports: [PreloaderModule],
//   })],
//
//   parameters: {
//     layout: 'centered',
//   },
// };

export const Preloader = () => ({
    template: `<cm-preloader></cm-preloader>`,
    props: {},
  });
