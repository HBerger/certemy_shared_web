import { moduleMetadata } from '@storybook/angular';
import { CertemyImageCropperModule, FileUploadModule, CROPPER_MODES } from '@certemy/ui-components';
import { PipesModule } from '@certemy/common';
import { IMAGE_FILE_TYPES } from '@certemy/ui-components/src/lib/file-upload/file-upload.constants';
import { ImageCropperEvent } from '@certemy/ui-components/src/lib/image-cropper';

const Styles = [
    `
    .image {
      height: 300px;
      display: flex;
      max-width: 500px;
    }
    .image-wrapper {
      display: flex;
    }
    .cropped-image {
      height: 100px;
      padding: 5px;
      width: 100px;
      object-fit: contain;
      border-radius: 50%;
    }
    .cropped-image__square-type {
      border-radius: 0;
    }
    .file-upload {
      margin-bottom: 20px;
      width: 300px;
    }
  `,
];

export default {
    title: 'Image Cropper',

    decorators: [
        moduleMetadata({
            imports: [CertemyImageCropperModule, FileUploadModule, PipesModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const ImageCropperExample = () => ({
    template: `
      <cm-file-upload
        (change)="onFilesChange($event)"
        [showFilesList]="false"
        [allowedTypes]="IMAGE_FILE_TYPES"
        class="file-upload">
      </cm-file-upload>
      <div class="image-wrapper">
        <cm-image-cropper
          class="image"
          [options]="options"
          [allowSwitchingMode]="allowSwitchingMode"
          [imageFile]="imageFile"
          (imageCropped)="imageCropped($event)">
        </cm-image-cropper>
        <img *ngIf="file" class="cropped-image" [class.cropped-image__square-type]="squareMode" [src]="file | safe: 'resourceUrl'" />
      </div>
    `,
    props: {
        options: {
            minCropBoxWidth: 80,
            minCropBoxHeight: 80,
        },
        allowSwitchingMode: true,
        IMAGE_FILE_TYPES,
        onFilesChange(files: File[]) {
            this.imageFile = files[files.length - 1];
        },
        imageCropped(imageCropperEvent: ImageCropperEvent) {
            if (!imageCropperEvent) {
                return;
            }

            const formData = new FormData();
            formData.append('croppedImage', imageCropperEvent.blob, 'image.png');
            this.file = URL.createObjectURL(imageCropperEvent.blob);
            this.squareMode = imageCropperEvent.mode === CROPPER_MODES.SQUARE;
        },
    },
    styles: Styles,
});
