import { moduleMetadata } from '@storybook/angular';
import { SharedCommonModule } from '@certemy/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UntypedFormControl } from '@angular/forms';
import { DisplayHtmlModule, HtmlEditorModule } from '@certemy/ui-components';

const ENABLE_BUTTON_TEXT = 'Enable';
const DISABLE_BUTTON_TEXT = 'Disable';

const Styles = [
    `   .example {
         min-width: 400px;
         font-size: 14px;
         display: flex;
         flex-direction: column;
      }

      .heading {
        text-align: center;
        margin: 20px auto 0;
      }
  `,
];

export default {
    title: 'Editor',

    decorators: [
        moduleMetadata({
            imports: [SharedCommonModule, HtmlEditorModule, DisplayHtmlModule, NgbModule],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const Example = () => ({
    template: `
        <div class="example">
        <cm-html-editor class="example-item" [toolbarOptions]="toolbarOptions" [formControl]="formControl" [disabled]="disabled"></cm-html-editor>
        <br>
        <h5 class="heading">Preview</h5>
        <div><cm-display-html [content]="formControl.value"></cm-display-html></div>
        <br>
        <h5 class="heading">Raw HTML</h5>
        <div>{{formControl.value}}</div>

       <button (click)="setValue()" class="btn btn-primary">Set Value</button>
       <button (click)="setDisable()" class="btn btn-primary">{{getToggleName()}}</button>
    `,
    props: {
        toolbarOptions: {
            withAlignment: true,
            withList: true,
            withFont: true,
            withSize: true,
        },
        disabled: false,
        formControl: new UntypedFormControl(''),
        setValue() {
            this.formControl.setValue('<strong>Hello</strong>');
        },
        setDisable() {
            this.disabled = !this.disabled;
        },
        getToggleName() {
            if (this.disabled) {
                return ENABLE_BUTTON_TEXT;
            }

            return DISABLE_BUTTON_TEXT;
        },
    },
    styles: Styles,
});
