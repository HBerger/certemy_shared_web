import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { SharedCommonModule } from '@certemy/common';
import { LocationModule, LocationService, SmartForm, SmartFormModule } from '@certemy/ui-components';
import { action } from '@storybook/addon-actions';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { ExampleLocationService } from './helpers/example-location.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { importProvidersFrom } from '@angular/core';

const petOptions = [
    {
        id: 'cat',
        name: 'Cat',
    },
    {
        id: 'dog',
        name: 'Dog',
    },
    {
        id: 'rabbit',
        name: 'Rabbit',
    },
];

export default {
    title: 'Smart Form',

    decorators: [
        applicationConfig({
          providers: [
            importProvidersFrom(LocationModule.forRoot()),
          ],
        }),
        moduleMetadata({
            imports: [SharedCommonModule, NgbModule,  SmartFormModule],
            providers: [{ provide: LocationService, useClass: ExampleLocationService }],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const Example = () => ({
    template: `
        <cm-smart-form #smartForm [fields]="fields"></cm-smart-form>
        <button class="btn btn-primary btn-submit" (click)="submit(smartForm)">Submit (show errors)</button>
        <br>
        <div class="col-12"><pre>{{smartForm.form.value | json}}</pre></div>
    `,
    props: {
        submit(smartForm) {
            action('FormValue')(smartForm.getFormValue());
        },
        fields: [
            new SmartForm.TextField({
                name: 'text',
                label: 'Text field',
                value: '',
                required: true,
            }),
            new SmartForm.EmailField({
                name: 'email',
                label: 'Email field',
                value: '',
                required: true,
            }),
            new SmartForm.DateField({
                name: 'date',
                label: 'Date field',
                value: '2016-12-31',
                required: true,
            }),
            new SmartForm.CheckboxField({
                name: 'checkbox',
                label: 'Single checkbox',
                value: true,
                required: true,
            }),
            new SmartForm.NumberField({
                name: 'number',
                label: 'Number field',
                value: 2016,
                required: true,
            }),
            new SmartForm.SelectField({
                name: 'select',
                label: 'Select field',
                value: '',
                required: true,
                options: [...petOptions],
            }),
            new SmartForm.LocationField({
                name: 'location',
                value: {
                    country: '4',
                    state: '1',
                    city: '',
                },
            }),
            new SmartForm.RadioGroupField({
                name: 'radioGroup',
                label: 'Radio Group',
                value: '',
                required: true,
                options: [...petOptions],
            }),
            new SmartForm.CheckboxGroupField({
                name: 'checkboxGroup',
                label: 'Checkbox Group',
                value: null,
                required: true,
                options: [...petOptions],
            }),
            new SmartForm.TypeaheadField({
                request$: (term) => {
                    return of([
                        { id: 1, name: 1111 },
                        { id: 2, name: 2222 },
                        { id: 3, name: 3333 },
                        { id: 4, name: 4444 },
                    ]).pipe(delay(300));
                },
                formatter: (item) => item.name,
                name: 'typeaheadField',
                label: 'TypeaheadField',
                value: null,
            }),
        ],
    },
});
