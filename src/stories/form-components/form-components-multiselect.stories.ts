import { moduleMetadata } from '@storybook/angular';
import { MultiSelectModule, MultiSelect } from '@certemy/ui-components';
import { SharedCommonModule, required } from '@certemy/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';

export default {
  title: 'Form Components/Multi Select',

  decorators: [
    moduleMetadata({
      imports: [SharedCommonModule, MultiSelectModule, NgbModule],
    }),
  ],

  parameters: {
    layout: 'centered',
  },
};

export const _MultiSelect = () => ({
    template: `
      <div class="wrapper">
        <div class="row">
          <div class="col-sm-4">
            <h6 class="bold">Multi Select with Object Values</h6>
            <cm-multi-select
                [formControl]="formControl"
                [options]="options"
                [valueType]="multiSelectValueTypes.Object"
                [params]="multiSelectParams2"
                class="multi-select"
                [class.is-invalid]="formControl.invalid">
            </cm-multi-select>
            <button class="btn btn-primary" (click)="toggleDisabledState()">Toggle Disabled State</button>
            <p>Valid: {{formControl.valid}}</p>
            <pre class="result-view">Value: {{formControl.value | json }}</pre>
          </div>
          <div class="col-sm-4">
            <h6 class="bold">Multi Select with Id Values</h6>
            <cm-multi-select
                [(ngModel)]="selectValue"
                [disabled]="selectDisabled"
                [options]="options"
                class="multi-select">
            </cm-multi-select>
            <button class="btn btn-primary" (click)="selectDisabled = !selectDisabled">Toggle Disabled State</button>
            <pre>Value: {{selectValue | json}}</pre>
          </div>
          <div class="col-sm-4">
            <h6 class="bold">Multi Select without Options</h6>
            <cm-multi-select [options]="noOptions" class="multi-select"></cm-multi-select>
          </div>
          <div>
            <h6 class="bold">Multi Select with Params</h6>
            <cm-multi-select
                [options]="options"
                [valueType]="multiSelectValueTypes.Object"
                [params]="multiSelectParams1"
                class="multi-select">
            </cm-multi-select>
          </div>
        </div>
      </div>
    `,
    styles: [
        `
      .wrapper {
        padding: 20px;
      }

      .multi-select {
        width: 320px;
        margin-bottom: 20px;
      }

      .result-view {
        height: 300px;
        overflow: auto;
      }
      `,
    ],
    props: {
        multiSelectParams2: {
            withSearch: true,
            noContentConfig: {
                title: 'No options',
                subtitle: 'No options were found to match your search',
            },
        },
        multiSelectParams1: {
            withChips: true,
            withSelectAll: true,
            optionDetailColumns: [
                {
                    id: 'firstName',
                    label: 'First name',
                    width: '30%',
                },
                {
                    id: 'lastName',
                    label: 'Last name',
                    width: '40%',
                },
                {
                    id: 'email',
                    label: 'Email',
                    width: '30%',
                },
            ],
        },
        options: new Array(50).fill(true).map((_, index) => ({
            id: index,
            name: `Option ${index}`,
            firstName: `Name ${index}`,
            lastName: `Last name ${index}`,
            email: `Email ${index}`,
        })),
        noOptions: [],
        formControl: new FormControl<{id: number, name: string}[]>(
            [
                { id: 2, name: 'Option 2' },
                { id: 4, name: 'Option 4' },
            ],
            [required],
        ),
        toggleDisabledState() {
            this.formControl.disabled ? this.formControl.enable() : this.formControl.disable();
        },
        selectValue: [],
        selectDisabled: false,
        multiSelectValueTypes: MultiSelect.ValueType,
    },
});

_MultiSelect.storyName = 'MultiSelect';
