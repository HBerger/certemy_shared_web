import { moduleMetadata } from '@storybook/angular';
import {
  CheckboxGroupModule,
  CheckboxModule,
  DatePickerModule,
  DatePickerMonthDateModule,
  DateRangeModule,
  LocationModule,
  LocationService,
  MultiSelectModule,
  NumberInputModule,
  RadioGroupModule,
  RadioModule,
  RangeModule,
  SelectModule,
  TextareaModule,
  TextInputModule,
  TypeaheadModule,
  FileUploadModule,
  ChipsInputModule,
  CustomDropdownModule,
  SearchInputModule,
  TimePickerModule,
  SwitchModule,
  SecureTextInputModule,
  MASK_TYPE,
  SelectSearchModule,
  DEFAULT_FILE_TYPES,
} from '@certemy/ui-components';
import { SharedCommonModule, dontBeEqual, withMessage, required } from '@certemy/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormControl } from '@angular/forms';
import { ExampleLocationService } from '../helpers/example-location.service';
import { of } from 'rxjs';
import { action } from '@storybook/addon-actions';
import { delay } from 'rxjs/operators';
import moment from 'moment-mini';

const petOptions = [
  {
    id: 'cat',
    name: 'Cat',
  },
  {
    id: 'dog',
    name: 'Dog',
  },
  {
    id: 'rabbit',
    name: 'Rabbit',
  },
];

const Styles = [
  `
    .custom-dropdown {
      width: 300px;
    }
    .field-wrapper {
      width: 600px;
    }
    .header {
       margin-top: 40px;
    }
  `,
];

export default {
  title: 'Form Components',

  decorators: [
    moduleMetadata({
      imports: [
        SharedCommonModule,
        MultiSelectModule,
        NgbModule,
        TextInputModule,
        NumberInputModule,
        ChipsInputModule,
        TextareaModule,
        CheckboxModule,
        CheckboxGroupModule,
        RadioModule,
        RadioGroupModule,
        DatePickerModule,
        DatePickerMonthDateModule,
        DateRangeModule,
        SwitchModule,
        TimePickerModule,
        SelectModule,
        RangeModule,
        LocationModule,
        TypeaheadModule,
        FileUploadModule,
        CustomDropdownModule,
        SearchInputModule,
        SecureTextInputModule,
        SelectSearchModule,
      ],
      providers: [{ provide: LocationService, useClass: ExampleLocationService }],
    }),
  ],

  parameters: {
    layout: 'centered',
  },
};

export const TextInput = () => ({
    template: `
        <div class="col">
        <cm-text-input
          [formControl]="formControl"
          placeholder="No data"
          [ngClass]="{'is-editable': isEditable}">
        </cm-text-input>
        </div>
        <br>
        <div class="col">
          Value: {{formControl.value | json}}
        </div>
    `,
    props: {
      formControl: new FormControl<string>(''),
      isEditable: true,
    },
  });

TextInput.storyName = 'Text input';

export const NumberInput = () => ({
    template: `
        <div class="col">
        <cm-number-input
          [formControl]="formControl"
          placeholder="No data"
          [ngClass]="{'is-editable': isEditable}">
        </cm-number-input>
        </div>
        <br>
        <div class="col">
          Value: {{formControl.value | json}}
        </div>
         <br>
        <div>
        <span>Disabled:</span>
          <cm-number-input
            [ngModel]="disabledInputValue"
            placeholder="No data"
            [disabled]="true">
          </cm-number-input>
        </div>
         <div>
         <span>Multiple of half:</span>
          <cm-number-input
            [ngModel]="disabledInputValue"
            placeholder="No data"
            [maskConfig]="{maskType: MASK_TYPE.MULTIPLE_OF_HALF, digitLimit: 10}">
          </cm-number-input>
        </div>
         <div>
         <span>Positive integer:</span>
          <cm-number-input
            [ngModel]="disabledInputValue"
            placeholder="No data"
            [maskConfig]="{maskType: MASK_TYPE.POSITIVE_INTEGER, digitLimit: 4}">
          </cm-number-input>
        </div>
        <div>
         <span>Percentage mask:</span>
          <cm-number-input
            [ngModel]="disabledInputValue"
            placeholder="No data"
            [maskConfig]="{maskType: MASK_TYPE.PERCENTAGE}">
          </cm-number-input>
        </div>
        <div>
         <span>Phone number:</span>
          <cm-number-input
            [ngModel]="disabledInputValue"
            placeholder="No data"
            [maskConfig]="{maskType: MASK_TYPE.PHONE_NUMBER}">
          </cm-number-input>
        </div>
        <div>
           <span>FROM_ZERO_TO_ONE:</span>
            <cm-number-input
              [ngModel]="disabledInputValue"
              placeholder="No data"
              [maskConfig]="{maskType: MASK_TYPE.FROM_ZERO_TO_ONE}">
            </cm-number-input>
         </div>
         <div>
           <span>FLOAT:</span>
            <cm-number-input
              [ngModel]="disabledInputValue"
              placeholder="No data"
              [maskConfig]="{maskType: MASK_TYPE.FLOAT}">
            </cm-number-input>
          </div>
          <div>
           <span>ONE_DECIMAL:</span>
            <cm-number-input
              [ngModel]="disabledInputValue"
              placeholder="No data"
              [maskConfig]="{maskType: MASK_TYPE.ONE_DECIMAL}">
            </cm-number-input>
          </div>`,
    props: {
      formControl: new FormControl<number>(null),
      isEditable: true,
      disabledInputValue: 999,
      MASK_TYPE: MASK_TYPE,
    },
  });

NumberInput.storyName = 'Number input';

export const ChipsInput = () => ({
    template: `
        <h6 class="bold">Common:</h6>
        <cm-chips-input [disabled]="disabled" class="field-wrapper" [validators]="validators" [formControl]="formControl1"></cm-chips-input>
        <div class="field-wrapper">Value: {{formControl1.value | json}}</div>
        <h6 class="bold header">compactMode: true</h6>
        <cm-chips-input [disabled]="disabled" (formErrorChanged)="formErrorChanged($event)" [displayError]="true" class="field-wrapper" [params]="{ compactMode: true }" [validators]="validators" [formControl]="formControl2"></cm-chips-input>
        <div class="field-wrapper">Value: {{formControl2.value | json}}</div>
        <button class="btn btn-primary header" (click)="disabled = !disabled">Toggle Disabled Mode</button>
    `,
    styles: Styles,
    props: {
      formControl1: new FormControl<string[]>(['Option 1', 'Option 2', 'Option 3']),
      formControl2: new FormControl<string[]>([]),
      disabled: false,
      formErrorChanged(value) {
        console.log('FormErro:', value);
      },
      validators: [
        withMessage(
          'You cannot add "email1@example.com" email',
          dontBeEqual({
            definedValues: ['email1@example.com'],
            errorType: 'emailError',
          }),
        ),
        withMessage('Please fill in the field', required),
      ],
    },
  });

export const Textarea = () => ({
    template: `
        <div class="col">
        <cm-textarea
          [formControl]="formControl"
          placeholder="No data"
          [ngClass]="{'is-editable': isEditable}">
        </cm-textarea>
        </div>
        <br>
        <div class="col">
          Value: {{formControl.value | json}}
        </div>

        <br>
        <div class="col">
        <label>Textarea. AutoHeight.</label>
        <cm-textarea
          [formControl]="formControlAutoHeight"
          placeholder="No data"
          cmAutoHeight
          [ngClass]="{'is-editable': isEditable}">
        </cm-textarea>
        </div>
        <br>
        <div class="col">
          Value: {{formControlAutoHeight.value | json}}
        </div>
    `,
    props: {
      formControl: new FormControl<string>(''),
      formControlAutoHeight: new FormControl<string>(''),
      isEditable: true,
    },
  });

export const Checkbox = () => ({
    template: `
        <div class="d-flex flex-column">
          <cm-checkbox [checked]="false">Unchecked</cm-checkbox>
          <cm-checkbox [checked]="true">Checked</cm-checkbox>
          <cm-checkbox [checked]="false" [disabled]="true">Unchecked disabled</cm-checkbox>
          <cm-checkbox [checked]="true" [disabled]="true">Checked disabled</cm-checkbox>
        </div>
    `,
    props: {},
  });

export const Radio = () => ({
    template: `
        <div class="d-flex flex-column">
          <cm-radio name="radio-unchecked" [checked]="false">Unchecked</cm-radio>
          <cm-radio name="radio-checked" [checked]="true">Checked</cm-radio>
          <cm-radio name="radio-unchecked-disabled" [checked]="false" [disabled]="true">Unchecked disabled</cm-radio>
          <cm-radio name="radio-checked-disabled" [checked]="true" [disabled]="true">Checked disabled</cm-radio>
        </div>
    `,
    props: {},
  });

export const CheckboxGroup = () => ({
    template: `
        <cm-checkbox-group [formControl]="formControl" [options]="petOptions"></cm-checkbox-group>
        <br>
        <div>Selected pets: <pre>{{formControl.value | json}}</pre></div>
        <br>
        <div>Disabled: <cm-checkbox-group [disabled]="true" [formControl]="formControl" [options]="petOptions"></cm-checkbox-group></div>
    `,
    props: {
      formControl: new FormControl<{ cat: boolean, dog: boolean, rabbit: boolean }>({
        cat: false,
        dog: false,
        rabbit: true,
      }),
      petOptions,
    },
  });

CheckboxGroup.storyName = 'Checkbox group';

export const RadioGroup = () => ({
    template: `
        <cm-radio-group [formControl]="formControl" [options]="petOptions" name="pets-r"></cm-radio-group>
        <br>
        <div>Selected pets: <pre>{{formControl.value | json}}</pre></div>
        <br>
        <div>Disabled: <cm-radio-group [formControl]="formControl" [disabled]="true" [options]="petOptions" name="pets-r"></cm-radio-group></div>
    `,
    props: {
      formControl: new FormControl<string>('dog'),
      petOptions,
    },
  });

RadioGroup.storyName = 'Radio group';

export const DatePicker = () => ({
    template: `
    <div class="col">
      <div>
        <cm-datepicker-month-date [(ngModel)]="model" [placeholder]="placeholder" [minDate]="datePickerMinDate" [maxDate]="datePickerMaxDate"></cm-datepicker-month-date>
      <div>Date: {{model | json}}</div>
      </div>
      <br>
      <div>
        <cm-datepicker [formControl]="formControl"></cm-datepicker>
        <div>Date: {{formControl.value | json}}</div>
      </div>
      <br>
      <p>
        Small date picker
        <cm-datepicker class="small"></cm-datepicker>
      </p>
      <p>
        Editable date picker
        <cm-datepicker [ngClass]="{'is-editable': isEditable}" placeholder="No data"></cm-datepicker>
      </p>
    </div>
    `,
    props: {
      formControl: new FormControl<string>('', { updateOn: 'blur' }),
      isEditable: true,
      get datePickerMinDate() {
        return moment([2021]).startOf('year').format('YYYY-MM-DD');
      },
      get datePickerMaxDate() {
        return moment([2021]).endOf('year').format('YYYY-MM-DD');
      },
      placeholder: 'mm-dd',
      model: '',
    },
  });

DatePicker.storyName = 'Date picker';

export const DateRange = () => ({
    template: `
        <cm-date-range [formControl]="formControl"></cm-date-range>
        <br>
        <div>Date Range: <pre>{{formControl.value | json}}</pre></div>
    `,
    props: {
      formControl: new FormControl<{ from: string, to: string }>({ from: null, to: null }),
    },
  });

export const Switcher = () => ({
    template: `
      <cm-switch [formControl]="formControl"></cm-switch>
      <div>Time: {{formControl.value | json}}</div>
    `,
    props: {
      formControl: new FormControl<boolean>(false),
    },
  });

export const TimePicker = () => ({
    template: `
        <cm-timepicker [formControl]="formControl"></cm-timepicker>
        <pre>Time: {{formControl.value | json}}</pre>
    `,
    props: {
      formControl: new FormControl<string>(''),
    },
  });

TimePicker.storyName = 'Time picker';

export const Select = () => ({
    template: `
        <cm-select
          [formControl]="formControl"
          [options]="petOptions"
          [ngClass]="{'is-editable': isEditable}"
          [opened]="true"
          [autoClose]="inside"
          placeholder="No data">
        </cm-select>
        <br>
        <div>Selected pets: <pre>{{formControl.value | json}}</pre></div>
    `,
    props: {
      formControl: new FormControl<string>('dog'),
      petOptions,
      isEditable: true,
    },
  });

export const Range = () => ({
    template: `
        <cm-range [formControl]="formControl"></cm-range>
        <br>
        <div>Range: <pre>{{formControl.value | json}}</pre></div>
    `,
    props: {
      formControl: new FormControl<{ from: number, to: number }>({ from: null, to: null }),
      petOptions,
    },
  });

export const LocationControl = () => ({
    template: `
        <cm-location [formControl]="formControl"></cm-location>
        <br>
        <pre>{{formControl.value | json}}</pre>
    `,
    props: {
      formControl: new FormControl<string>(''),
    },
  });

export const Typeahead = () => ({
    template: `
        <cm-typeahead
                      class="form-input"
                      [formControl]="formControl"
                      [request$]="request$"
                      [formatter]="formatter">
        </cm-typeahead>
        <br>
        <pre>{{formControl.value | json}}</pre>
    `,
    props: {
      formControl: new FormControl<{ name: string }[]>(null),
      request$: (term) => {
        action('Term')(term);
        return of([{ name: 'Item01' }, { name: 'Item02' }, { name: 'Item02' }, { name: 'Item01' }]).pipe(delay(200));
      },
      formatter: (res) => res.name,
    },
  });

export const FileUpload = () => ({
    template: `
                  <p>Common</p>
                  <div class="container">
                    <cm-file-upload [allowedTypes]="allowedTypes" [files]="files"></cm-file-upload>
                  </div>
                  <hr>
                  <p>[compact]="true"</p>
                  <div class="container">
                     <cm-file-upload class="compact" [allowedTypes]="allowedTypes" [files]="files" [compact]="true"></cm-file-upload>
                  </div>
                  <hr>
                  <p>filesWithUrls</p>
                  <div class="container">
                      <cm-file-upload [allowedTypes]="allowedTypes" [files]="filesWithUrls"></cm-file-upload>
                  </div>
                  <p>Single File Upload</p>
                  <div class="container">
                    <cm-file-upload [allowedTypes]="allowedTypes" [files]="files" [isMultipleFiles]="false"></cm-file-upload>
                  </div>
    `,
    props: {
      files: [],
      filesWithUrls: [
        {
          id: 1,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/9c81b79c-e142-419a-b193-184c3f6cae45.jpg',
          name: 'image.jpg',
          size: 99452,
          profileId: 378090,
        },
        {
          id: 2,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/a23c41a2-1d29-4250-813c-e9fb494e765a.xlsx',
          name: 'spreadsheet.xlsx',
          size: 4728,
          profileId: 160166,
        },
        {
          id: 3,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/8b39286f-5926-4a6e-946f-7e037d78a5d3.pptx',
          name: 'presentation.pptx',
          size: 364978,
          profileId: 160166,
        },
        {
          id: 4,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/0469bd99-02b0-4274-923a-bfcfe6bceaeb.txt',
          name: 'text.txt',
          size: 122,
          profileId: 160166,
        },
        {
          id: 5,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/1cbc71b1-c84b-4bec-8d1d-7288738bceeb.pdf',
          name: 'pdf.pdf',
          size: 9396,
          profileId: 160166,
        },
        {
          id: 6,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/8e5c3d42-849f-40af-93dd-a32c003bbdd0.rtf',
          name: 'rich text editor.rtf',
          size: 13886,
          profileId: 160166,
        },
        {
          id: 7,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/bdf323d8-593c-4e7a-b1f8-e3377e0f225d.docx',
          name: 'doc.docx',
          size: 6123,
          profileId: 160166,
        },
        {
          id: 8,
          link: 'https://certemyalpha.s3.us-west-2.amazonaws.com/97aa870b-2d3e-44b4-872c-01417d67f5a4.png?some_parameter=1&parameter=1.6',
          name: 'image.png',
          profileId: 160166,
          size: 1594447,
        },
      ],
      allowedTypes: DEFAULT_FILE_TYPES,
    },
  });

export const CustomDropdown = () => ({
    template: `
      <h6 class="bold">Common:</h6>
      <cm-custom-dropdown
        class="custom-dropdown"
        placeholder="Test data"
        [dropDownMenu]="dropdownMenu1">
      </cm-custom-dropdown>
      <ng-template #dropdownMenu1 let-dropdown>
        Dropdown text
        <button (click)="dropdown.close()">Close dropdown</button>
      </ng-template>

      <h6 class="bold header">With form control:</h6>
      <cm-custom-dropdown
        [formControl]="formControl"
        [options]="petOptions"
        class="custom-dropdown"
        placeholder="Test data"
        [dropDownMenu]="dropdownMenu2">
      </cm-custom-dropdown>
      <ng-template #dropdownMenu2 let-dropdown>
        <div *ngFor="let option of petOptions" (click)="select(option, dropdown)">{{option.name}}</div>
      </ng-template>
    `,
    props: {
      formControl: new FormControl<{ id: number, name: string }[]>(null),
      petOptions,
      select(option, dropdown) {
        dropdown.close();
        this.formControl.setValue(option.id);
      },
    },
    styles: Styles,
  });

export const SearchInput = () => ({
    template: `
      <cm-search-input (valueChange)="onSearch($event)" [minSearchLength]="2"></cm-search-input>
      <div>Search value: {{searchQuery}}</div>
    `,
    props: {
      searchQuery: '',
      onSearch(search: string) {
        this.searchQuery = search;
      },
    },
    styles: Styles,
  });

export const SecureInput = () => ({
    template: `
      <cm-secure-text-input [formControl]="formControl"></cm-secure-text-input>
      <cm-secure-text-input [formControl]="formControlWithValue"></cm-secure-text-input>

    `,
    props: {
      formControl: new FormControl<string>(''),
      formControlWithValue: new FormControl<string>('Test'),
    },
  });

export const SelectSearchControl = () => ({
    template: `
      <cm-select-search
        class="select-search-control"
        [disabled]="false"
        [config]="config"
        [options]="options"
        (search)="onSearch($event)"
        (optionSelect)="onOptionSelect($event)"
      ></cm-select-search>
      <div>Search: {{search}}</div>
    `,
    props: {
      placeholder: '',
      config: {
        noEntries: 'No data found',
      },
      search: '',
      options: [...petOptions],
      onSearch(search) {
        this.search = search;
        this.options = petOptions.filter(({ name }) => name.toLowerCase().includes(search.toLowerCase()));
      },
      onOptionSelect({ option, dropdownRef }) {
        dropdownRef.close();
        this.placeholder = option.name;
      },
    },
    styles: [
      `
      .select-search-control {
        width: 250px;
      }
    `,
    ],
  });
