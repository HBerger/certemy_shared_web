import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'cm-standalone-modal',
  templateUrl: './standalone-modal.component.html',
})
export class StandaloneModalComponent {
  @Input() data;
  constructor(private ngbActiveModal: NgbActiveModal) {}

  onCancel() {
    this.ngbActiveModal.dismiss();
  }

  onClose(data) {
    this.ngbActiveModal.close(data);
  }
}
