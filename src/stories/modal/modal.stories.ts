import { applicationConfig, moduleMetadata } from '@storybook/angular';
import { SmartModalModule, SmartModalService } from '@certemy/ui-components';
import { SharedCommonModule } from '@certemy/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InjectorComponent } from '../helpers/injector.component';
import { take } from 'rxjs/operators';
import { importProvidersFrom, TemplateRef } from '@angular/core';
import { StandaloneModalComponent } from './standalone-modal/standalone-modal.component';
import { action } from '@storybook/addon-actions';

const modalAction = action('MODAL_ACTION');

export default {
    title: 'Modals',

    decorators: [
      applicationConfig({
        providers: [
          importProvidersFrom(SmartModalModule.forRoot()),
        ],
      }),
        moduleMetadata({
            imports: [SharedCommonModule, NgbModule, SmartModalModule],
            declarations: [InjectorComponent, StandaloneModalComponent],
            entryComponents: [StandaloneModalComponent],
        }),
    ],

    parameters: {
        layout: 'centered',
    },
};

export const ModalsExamples = () => ({
    template: `
      <cm-injector [tokens]="tokens" (resolved)="resolved($event)"></cm-injector>
      <button (click)="openUnsavedChanges()" class="btn btn-default">Open unsaved changes</button><br>
      <button (click)="openConfirm()" class="btn btn-default">Open Confirm</button><br>
      <button (click)="openNotification()" class="btn btn-default">Open notification</button><br>
      <button (click)="openConfirmTemplate(listTemplate)" class="btn btn-default">Open Confirm with custom template</button><br>
      <button (click)="openStandalone()" class="btn btn-default">Open with custom component</button><br>
      <ng-template #listTemplate>
        <ul>My custom template for body
          <li>1</li>
          <li>2</li>
          <li>3</li>
        </ul>
      </ng-template>`,
    props: {
        tokens: { smartModalService: SmartModalService },
        instances: {},
        resolved(instances) {
            this.instances = instances;
        },
        openUnsavedChanges() {
            this.instances.smartModalService.openUnsavedChanges().pipe(take(1)).subscribe(modalAction);
        },

        openConfirm() {
            this.instances.smartModalService
                .openConfirm({ title: 'title', body: 'body?' })
                .pipe(take(1))
                .subscribe(modalAction);
        },

        openConfirmTemplate(listTemplate: TemplateRef<any>) {
            this.instances.smartModalService
                .openConfirm({ title: 'Confirm Template', body: listTemplate })
                .pipe(take(1))
                .subscribe(modalAction);
        },

        openStandalone() {
            this.instances.smartModalService
                .open(StandaloneModalComponent, { data: 'data from caller' })
                .pipe(take(1))
                .subscribe(modalAction);
        },

        openNotification() {
            this.instances.smartModalService
                .openNotification({ title: 'Hallo', body: 'Just close me.' })
                .pipe(take(1))
                .subscribe(modalAction);
        },
    },
});

ModalsExamples.storyName = 'Modals examples';
