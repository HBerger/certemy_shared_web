import gulp from 'gulp';
import * as path from 'path';
import { deleteAsync } from 'del';
import  fs from 'fs';
import yargs from 'yargs'
import { exec } from 'node:child_process';

const paths = {
  nodeCertemyShared: path.resolve('./node_modules/certemy-shared'),
  nodeCertemyThemeLib: path.resolve('./node_modules/certemy-shared/projects/theme'),
  themeLibWatch: path.resolve('./projects/theme'),
  themeLibFiles: path.resolve('./projects/theme/**/*'),
  nodeCertemyCommonAlias: path.resolve('./node_modules/@certemy/common'),
  nodeCertemyUiComponentsAlias: path.resolve('./node_modules/@certemy/ui-components'),
  commonLibFiles: path.resolve('./projects/common/src/**/*'),
  uiComponentsLibFiles: path.resolve('./projects/ui-components/src/**/*'),
  distTheme: path.resolve('./dist/theme'),
  dist: path.resolve('./dist/**/*'),
  sharedWebBuild: path.resolve('../certemy_shared_web-build'),
  sharedWebBuildDist: path.resolve('../certemy_shared_web-build/dist')
};

gulp.task('clean', function() {
  return deleteAsync([paths.nodeCertemyShared]);
});

gulp.task('copy-theme', gulp.series('clean', function() {
  return gulp.src(paths.themeLibFiles).pipe(gulp.dest(paths.nodeCertemyThemeLib));
}));

gulp.task('build-theme', function () {
  return gulp.src(paths.themeLibFiles).pipe(gulp.dest(paths.distTheme));
});

gulp.task('watch-theme', function () {
  gulp.watch('./projects/theme', gulp.series('copy-theme'));
});

gulp.task('copy-common', function() {
  return gulp.src(paths.commonLibFiles).pipe(gulp.dest(paths.nodeCertemyCommonAlias));
});

gulp.task('copy-ui-components', function () {
  return gulp.src(paths.uiComponentsLibFiles).pipe(gulp.dest(paths.nodeCertemyUiComponentsAlias));
});

gulp.task('copy-dependencies', gulp.series('copy-theme', 'copy-common', 'copy-ui-components'));

gulp.task('watch-dependencies', function () {
  gulp.watch('./projects', gulp.series('copy-common', 'copy-ui-components', 'copy-theme'));
});

gulp.task('dev', gulp.series('copy-theme', 'watch-theme'));

gulp.task('copy-dist', function () {
  if (!fs.existsSync(paths.sharedWebBuild)) {
    throw new Error(`certemy_shared_web-build project should be available in ${path.resolve('../')}`);
  }
  return gulp.src(paths.dist).pipe(gulp.dest(paths.sharedWebBuildDist));
});

gulp.task('push-build', function (cb) {
  const branchName = yargs.argv.branch;
  if (!branchName) {
    throw new Error(`branch name is required, use '--branch branchName'`);
  }
  const publishCommand = `git fetch && git checkout -B ${branchName} -q && git add . && git commit -m "auto-build" && git push origin HEAD`;
  const workingDir = paths.sharedWebBuild;
  exec(publishCommand, {cwd: workingDir}, function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('publish-libs', gulp.series('copy-dist', 'push-build'));
