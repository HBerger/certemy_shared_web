#!/bin/bash

set -e
repository_url=$1
region=$2
prefix=$3
workspace=$4

RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m'

DEF_PATH=`pwd`
echo "[INFO!] Default path is $DEF_PATH"

{
    # Deploy!
    export TAG=$(echo $GIT_COMMIT | cut -c1-7)
    DATE=`date +%Y-%m-%d.%H-%M-%S`
    export TAG=$TAG-$DATE
    echo STAGE=$STAGE

    # Retrieve the docker login command that you can use to authenticate your Docker client to your registry:
    # wrap this in +x so we don't log our temporary credentials
    set +x

    eval $(aws ecr get-login --no-include-email --region $region)

    #aws ecr create-repository --repository-name certemy/storybook
    echo COMMIT_TAG=$TAG

    # Build, tag and push api image to remote repository
    npm install || exit 1
    npm run storybook:build || exit 1
	
    docker build -t certemy/$DEP_ENV:${STAGE,,} -f deploy/$DEP_ENV/Dockerfile .
    docker tag certemy/$DEP_ENV:${STAGE,,} $repository_url/certemy/$DEP_ENV:$TAG
    docker push $repository_url/certemy/$DEP_ENV:$TAG
    echo -e "\n${GREEN}$DEP_ENV BUILD completed!\n"

    # Create cluster and task definition
    # ecs-cli configure & generate ecs task definition from docker compose file
    ecs-cli configure  --region=$region --cluster $cluster  --compose-service-name-prefix $prefix
    ecs-cli compose --project-name $prefix-$DEP_ENV-$STAGE-$cluster --file deploy/ecs/ecs-compose.yml create

    echo "service-name=$prefix-$DEP_ENV-$STAGE-$cluster"

    # Update the service
    deploy/ecs/update-service.sh --region $region --cluster $cluster --service-name $DEP_ENV-$STAGE-$cluster \
     --task-definition $prefix-$DEP_ENV-$STAGE-$cluster  || { exit 1; }

} || { exit 1; }

echo -e "\n${GREEN}$STAGE $DEP_ENV deploy completed!\n"
