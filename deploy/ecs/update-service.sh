#!/usr/bin/env bash
set -e

function usage() {
    set -e
    cat <<EOM
    ##### update-service #####
    Based on https://github.com/silinternational/ecs-deploy

    Required arguments:
        --region           AWS Region Name. May also be set as environment variable AWS_DEFAULT_REGION
        --cluster          Name of ECS cluster
        --service-name     Name of service to deploy
        --task-definition  Name of task definition to deploy

    Optional arguments:
        --step-back        Default is 0 - script runs latest task definition in a list. "-s 1" will run next to last in a list.
        --timeout          Default is 180s. Script monitors ECS Service for new task definition to be running.

    Requirements:
        aws:  AWS Command Line Interface
        jq:   Command-line JSON processor

    Examples:
      Simple update of a service (Using env vars for AWS settings) with last task definition:

        ./update-service.sh --cluster test-cluster --task-definition test-task-def-name --service-name test-service-name
        ./update-service.sh --region us-east-1 --cluster test-cluster --task-definition test-task-def-name --service-name test-service-name

      Simple update of a service (Using env vars for AWS settings) with task definition stepback:

        ./update-service.sh --cluster test-cluster --task-definition test-task-def-name --service-name test-service-name --step-back 1

EOM

    exit 2
}
if [ $# == 0 ]; then usage; fi

# Check requirements
function require {
    command -v $1 > /dev/null 2>&1 || {
        echo "Some of the required software is not installed:"
        echo "    please install $1" >&2;
        exit 1;
    }
}

# Check for AWS, AWS Command Line Interface
require aws
# Check for jq, Command-line JSON processor
require jq
#jq version check
JQ_VERSION="$(jq --version)"
if (( $(echo "${JQ_VERSION:3:3} < 1.5" |bc -l) )); then
    echo "jq version must be 1.5 or newer!"
    exit 1;
fi

# Setup default values for variables
CLUSTER=false
SERVICE=false
TASK_DEFINITION=false
STEP=-1
TIMEOUT=300
AWS_CLI=$(which aws)
AWS_ECS="$AWS_CLI --output json ecs"

# Loop through arguments, two at a time for key and value
while [[ $# > 0 ]]
do
    key="$1"

    case $key in
        --region)
            AWS_DEFAULT_REGION="$2"
            shift # past argument
            ;;
        --cluster)
            CLUSTER="$2"
            shift # past argument
            ;;
        --service-name)
            SERVICE="$2"
            shift # past argument
            ;;
        --task-definition)
            TASK_DEFINITION="$2"
            shift # past argument
            ;;
        --step-back)
            STEP="$((-1-$2))"
            shift # past argument
            ;;
        --timeout)
            TIMEOUT="$2"
            shift
            ;;
        *)
            usage
            exit 2
        ;;
    esac
    shift # past argument or value
done

if [ $CLUSTER == false ] || [ $SERVICE == false ] || [ $TASK_DEFINITION == false ]; then
     echo "Some of the required arguments are not set!"
     exit 1
fi

if [ -z ${AWS_DEFAULT_REGION+x} ];
  then unset AWS_DEFAULT_REGION
  else
          AWS_ECS="$AWS_ECS --region $AWS_DEFAULT_REGION"
fi

# Update the service
# Get last task def from list
TASKDEF=`$AWS_ECS list-task-definitions --family-prefix $TASK_DEFINITION | jq .taskDefinitionArns[$STEP]`
TASKDEF=$(echo $TASKDEF | sed 's/.$//' | sed 's/^.//')

$AWS_ECS update-service --cluster $CLUSTER --service $SERVICE --task-definition $TASKDEF >/dev/null

################################################################################################################
# remove this section if you don't need service update check

#echo -n 'Service updating'

# See if the service is able to come up again
#every=10
#i=0
#while [ $i -lt $TIMEOUT ]; do
#    # Scan the list of running tasks for that service, and see if one of them is the
#    # new version of the task definition
#
#    RUNNING=$($AWS_ECS list-tasks --cluster $CLUSTER  --service-name $SERVICE --desired-status RUNNING \
#    | jq -r '.taskArns[]' \
#    | xargs -I{} $AWS_ECS describe-tasks --cluster $CLUSTER --tasks {} \
#    | jq ".tasks[]| if .taskDefinitionArn == \"$TASKDEF\" then . else empty end|.lastStatus" \
#    | grep -e "RUNNING" || : >/dev/null)
#
#    echo -n '.'
#
#    if [ "$RUNNING" ]; then
#    echo "OK"
#    echo "Service $CLUSTER / $SERVICE updated"
#    echo "Service updated successfully, new task definition running.";
#    exit 0
#    fi
#
#    sleep $every
#    i=$(( $i + $every ))
#done
#
# Timeout
#echo "ERROR!"
#echo "New task definition not running within $TIMEOUT seconds"
#exit 1

################################################################################################################

echo 'Service updated'
exit 0
