# Changelog
# 1.10.0
## UI Components lib
###PageHeader
* [Add cursor pointer to Back button Branches](https://bitbucket.org/HBerger/certemy_shared_web/commits/10bf0788b66c1ddc5896d60988fc33f39a7d0357)
* [Add logic for browserBack at page header](https://bitbucket.org/HBerger/certemy_shared_web/commits/f17d92ea80ad7a1e8d611795eed5cb58d642fc2e)
* [Updated navigation method](https://bitbucket.org/HBerger/certemy_shared_web/commits/03744457056cfd10054a5caeaad84d0ad5289157)
* [Updated navigation method](https://bitbucket.org/HBerger/certemy_shared_web/commits/c2e89be75f8f1d166e21862a603725d5ef709d3e)
* [Add return to spot processing function](https://bitbucket.org/HBerger/certemy_shared_web/commits/707f5f08990172097e73c7f28fe58dbeba20d161)
###SmartTable
* [Fix for smart-check-cell](https://bitbucket.org/HBerger/certemy_shared_web/commits/f96148364c5b84ab70e73de44be4315bb308cf72)
# 1.9.0
## General
* [Remove rxjs-compat](https://bitbucket.org/HBerger/certemy_shared_web/commits/69020e72b1f2a39648f6e6283754c2f4dc41a67d)
* [Update project to Angular 7.2.8](https://bitbucket.org/HBerger/certemy_shared_web/commits/7a0a197d57a1f3b641769daf5f71d36b4936c12f)
* [Updated dependences according to Angular 7.2.8 and fixed errors](https://bitbucket.org/HBerger/certemy_shared_web/commits/c9bf4d269db646dcadc0c98db5244cffa99416b0)
## Common lib
###Constants
* [Changed dateTime format string](https://bitbucket.org/HBerger/certemy_shared_web/commits/69c0e11e29a4e25aedffbec05cfd1bcb615b2a09)
* [Update dateTime format](https://bitbucket.org/HBerger/certemy_shared_web/commits/e00eb0dc0875d5e2a9f657f6aa9b200f864e400b)
###Pipes
####DatepickerPipe
* [Added date formatter pipe](https://bitbucket.org/HBerger/certemy_shared_web/commits/17045a39b5c19dd5cda44242391eacd436aaffbe)
* [Updated date formatter name](https://bitbucket.org/HBerger/certemy_shared_web/commits/17045a39b5c19dd5cda44242391eacd436aaffbe)
###Decorators
* [Fix ShareRequest decorator for requests with multiple arguments. Add tests](https://bitbucket.org/HBerger/certemy_shared_web/commits/2c077e92f0946f9f95fc1eb17a6ed304e0933cab)
###Helpers
####ShowIfHttpError
* [Add showIfHttpError decorator](https://bitbucket.org/HBerger/certemy_shared_web/commits/17045a39b5c19dd5cda44242391eacd436aaffbe)
* [Add export of showIfHttpErrorDecorator](https://bitbucket.org/HBerger/certemy_shared_web/commits/f43618de89bfa855189140636f52409e55192e46)
## UI Components lib
### SmartTable
* [Fixed date formatting](https://bitbucket.org/HBerger/certemy_shared_web/commits/1e1d6eeeb93aa835eb8ef329e6f05ebbacbb6f5f)
### HtmlEditor
* [Add inline editor](https://bitbucket.org/HBerger/certemy_shared_web/commits/6651517bbb79fe7bf815a0d78999f2cd0a2dccff)
* [Add text editor preview and link styles](https://bitbucket.org/HBerger/certemy_shared_web/commits/60c5864d00a1e915a3ce471ca0732d14ed6cc874)
* [Replace inline editor with classic one](https://bitbucket.org/HBerger/certemy_shared_web/commits/48c7ff65a66075aef04f2382ef60ee3245ba6be2)
* [Add getData method to editor component](https://bitbucket.org/HBerger/certemy_shared_web/commits/38fed19e2e6f12185fed1728ed7a099d9b3c17a2)
* [Add formControl to edit component](https://bitbucket.org/HBerger/certemy_shared_web/commits/d013c6e72c353c856cf38a98b5cb7f8bc8d65b75)
* [Add DisplayHtml component. Add set default value method](https://bitbucket.org/HBerger/certemy_shared_web/commits/86b2798afd8231b599e8b21cf17d812d107c1e8e)
* [Update display html component styles](https://bitbucket.org/HBerger/certemy_shared_web/commits/6d3acba9afc066e3cd54c0efdfbd2577ab88a3e5)
### Icon
* [Updated icons with Arrow-down](https://bitbucket.org/HBerger/certemy_shared_web/commits/c3525bc5fe5a40b15392b4d4bdad1cce3c4bdbb2)
### SmartFilters
* [Implemented Smart filters component](https://bitbucket.org/HBerger/certemy_shared_web/commits/cc8510d4495ce34af091afc1ca238fffb168966f)
* [Refactoring with comments](https://bitbucket.org/HBerger/certemy_shared_web/commits/9b998158fd7552ce0f55937e81ff27d53326e331)
* [Fixed imports](https://bitbucket.org/HBerger/certemy_shared_web/commits/0d2dd62579767a298170e7bb2d82fe50019638de)
* [Added interfaces to all possible places](https://bitbucket.org/HBerger/certemy_shared_web/commits/c64d7b5918f0a3934c6846d5b5b4e1ebbbe999a0)
* [Added FiltersValue interface](https://bitbucket.org/HBerger/certemy_shared_web/commits/f2a0f3b345ac90faee4ece0c20b5884a6f1cf789)
* [Used Name instead uid at Smart filters](https://bitbucket.org/HBerger/certemy_shared_web/commits/d5693eac488f6f3d422475fd540effb4e9800315)
* [Update smart filters interface](https://bitbucket.org/HBerger/certemy_shared_web/commits/ad6f993d0a94a513410bb2bbb309f73b3f8d24af)
* [Split fitlers into single files. Reexport all filter classes and interfaces as one namespace](https://bitbucket.org/HBerger/certemy_shared_web/commits/3cc69aa6d52d326550edbb28ca0f2ca66058bbeb)
# 1.8.2
## Common lib
###Custom Validators
* [Change email validation regex](https://bitbucket.org/HBerger/certemy_shared_web/commits/8399548555516cce38ad2bb85dc977e4aa7d1a25)
## UI Components lib
###FormControls
####Checkbox
* [Set default value if no value to checkboxes](https://bitbucket.org/HBerger/certemy_shared_web/commits/3bddf2f70e3df6556548b85a59d4e640ed8112a3)
###PageHeader
* [Added height to page header](https://bitbucket.org/HBerger/certemy_shared_web/commits/96e539da87e36352df257825e247ade6fe01c8ae)
# 1.8.1
## UI Components lib
###Checkbox, RadioGroup
* [Added validation for checkbox-group & radio-group components](https://bitbucket.org/HBerger/certemy_shared_web/commits/cc2dc36b00f7176e3222f673a4a60660493e1583)
* [Get rid of exported object](https://bitbucket.org/HBerger/certemy_shared_web/commits/bf1f136c9929fdc97a829ae3da78e042f2ceaec8)
# 1.8.0
## Storybook
* [Update storybook build to use paths from tsconfig. Copy theme to node_modules before build](https://bitbucket.org/HBerger/certemy_shared_web/commits/80b0f2338bcfab3e4c8a8dae12b70d56f9905bff)
* [Added deployment scripts](https://bitbucket.org/HBerger/certemy_shared_web/commits/72eff2e5a9971cf52f081ba7122378a3733079c2)
# 1.7.0
## UI Components lib
###NavTabs
* [Add disable option to nav-tabs](https://bitbucket.org/HBerger/certemy_shared_web/commits/ee37aac721abb6db22027d5c45e83ee5fd6e35ee)
###PageHeader
* [Fix quotes](https://bitbucket.org/HBerger/certemy_shared_web/commits/8023fcc25548435abc869b100b377c2ee757332a)
* [Add query-params-handling input to page-header](https://bitbucket.org/HBerger/certemy_shared_web/commits/241436cab9a242085526fb92f4a782d6f7172243)
* [Add types of handling](https://bitbucket.org/HBerger/certemy_shared_web/commits/9e1835dfd2b6e646b4ada37fc4c2f3c1f5ebf728)
###SmartTable
* [Add row object to tooltip call](https://bitbucket.org/HBerger/certemy_shared_web/commits/92384def8399d24e216a90869e7a9879e281a3be)
* [Add tooltip to smart table](https://bitbucket.org/HBerger/certemy_shared_web/commits/39bad29eee433c766819e69ac99a22713efbac1a)

# 1.6.0
## General
* [Remove ui-components-guide](https://bitbucket.org/HBerger/certemy_shared_web/commits/b029aa34557ac8017e36d86d139996468a83c626)
* [Add global styles loader](https://bitbucket.org/HBerger/certemy_shared_web/commits/96fadb6a89711ec48ae1a9a67f286cf8d81406d4)
* [Add gulp task copy-dependencies(watch) to resolve path without alias](https://bitbucket.org/HBerger/certemy_shared_web/commits/7a52db4dd8bbe2c79621ba902fbe83cbd80e1d69)
* [Explicitly import index file](https://bitbucket.org/HBerger/certemy_shared_web/commits/eede08524c9def7168b9f72178658e80cc24a829)
* [Ng-packagr fix: remove barrel import to prevent error: Unexpected value 'undefined' exported by the module](https://bitbucket.org/HBerger/certemy_shared_web/commits/a16a386dc3cbe6f8bcbde2329fb1de7dd7bbe2ba)
* [Add publish-libs script](https://bitbucket.org/HBerger/certemy_shared_web/commits/0b6f5e8398ea363a65b6591b17022458f86a16f8)
* [Don't track dist folder](https://bitbucket.org/HBerger/certemy_shared_web/commits/9c522f9512a727cb846763bca8435010515d29e2)
* [Add compiled files](https://bitbucket.org/HBerger/certemy_shared_web/commits/3f27355c8076d767803dd3ef8fcb0c56777e89e4)
* [Formatting, freeze prettier version](https://bitbucket.org/HBerger/certemy_shared_web/commits/b1a4e862a2feb60ec3ca64d72e76228fc2c256fd)
* [Get back .prettier, changelog](https://bitbucket.org/HBerger/certemy_shared_web/commits/3540130812f57778300c74809c57c2f42c39aa09)
* [Build theme](https://bitbucket.org/HBerger/certemy_shared_web/commits/956ebcb4bf609ae1b23004b5da0a903d03d3c4f1)
* [Move from nx to ng-cli. Add build commands](https://bitbucket.org/HBerger/certemy_shared_web/commits/beceb911bbd216a3c707ffb632f5b10112940074)
* [Fix 'Cannot call a namespace X' for import default](https://bitbucket.org/HBerger/certemy_shared_web/commits/8e9a667689df53945552f352b3f54b38dd8ecd04)
* [Fix entry points](https://bitbucket.org/HBerger/certemy_shared_web/commits/ce49440155cf5d86355006fc91a9e9f79bcdcd5f)
## Storybook
* [Add Smart-table stories](https://bitbucket.org/HBerger/certemy_shared_web/commits/df9d6a9a974a7e74b3f98dda86d32a58c2509791)
* [Add FileUpload stories](https://bitbucket.org/HBerger/certemy_shared_web/commits/cc8e75ac8cf1df215192077aeb84f2728ff0da14)
* [Add portal stories](https://bitbucket.org/HBerger/certemy_shared_web/commits/0e60baa43a6098dffa0f624f38a57c951b93a0bf)
* [Add sidebar stories](https://bitbucket.org/HBerger/certemy_shared_web/commits/ea5e348b1f7f23f85ede13da9865e6bd883d23e0)
* [Add smart-form, form-components stories](https://bitbucket.org/HBerger/certemy_shared_web/commits/7b0b418f9ba12da41be1f68ddd1f814a9e5a08cf)
* [Add modal stories](https://bitbucket.org/HBerger/certemy_shared_web/commits/48e98ac8753adbb479f50c38d3e7f773776689dd)
* [Add injector component that allow to get instances from the Injector](https://bitbucket.org/HBerger/certemy_shared_web/commits/4c15651ba6501670389771dabbeed9513ee0900c)
* [Add stories for SVG icons](https://bitbucket.org/HBerger/certemy_shared_web/commits/95516791a94385d8cd3ff5d3af7794d2aedd1d49)
* [Add stories for General components](https://bitbucket.org/HBerger/certemy_shared_web/commits/3f0e077b22ee9b33a717a770f09b3cf305dbe164)
* [Add storybook, addons, example](https://bitbucket.org/HBerger/certemy_shared_web/commits/f541834cafce27ac04d789fdbfcdd180ee84972f)
## Common lib
###Decorators
* [Add ShareRequest decorator](https://bitbucket.org/HBerger/certemy_shared_web/commits/fefaa808f386d3adb5b982f886b6bd73eff66cdd)
###Helpers
* [Update requestCompleted helper](https://bitbucket.org/HBerger/certemy_shared_web/commits/963c102f64f5453ad822e085f5103f87285291bf)
###Directives
* [Add DirectivesModule](https://bitbucket.org/HBerger/certemy_shared_web/commits/a9239395cf45ffaa440528ba5cdca52f4012b702)
* [Move directives to its own module](https://bitbucket.org/HBerger/certemy_shared_web/commits/b80b15aa4ea8c22b4cea64389e93d125318f41dc)
## UI Components lib
###SplitButton
* [Wrap split-button in module](https://bitbucket.org/HBerger/certemy_shared_web/commits/032949f61820e2f1b9e208c8674a0383da0062c9)
###FormControls
* [Add ability to disable checkbox from FormControl](https://bitbucket.org/HBerger/certemy_shared_web/commits/6e691342ced423f1f6239c0ed0115def8cc67398)
####Datepicker
* [Add error style to datapicker](https://bitbucket.org/HBerger/certemy_shared_web/commits/8787e122d022968c61f0b3e3c859e7d7b3489fe8)
* [Remove :host prefix for datepicker dropdown z-index rule](https://bitbucket.org/HBerger/certemy_shared_web/commits/4fbffe1f930d6b5cf6aea47f9d8618ca748fe632)
* [Add invalid outline to form control and calendar button to datepicker](https://bitbucket.org/HBerger/certemy_shared_web/commits/f0df4c85adebbcd3a70acab69267756610d8d85b)
* [Fix invalid styles for datepicker control](https://bitbucket.org/HBerger/certemy_shared_web/commits/1a0569c75f1b5c877875bc932dc6e1044624eb08)
####Checkbox
* [Add ability to disable checkbox from FormControl](https://bitbucket.org/HBerger/certemy_shared_web/commits/3cd1169cc552e7af6d096666b5db8f31399bc4cf)
####MultiSelect
* [Refactor multi-select value convertation](https://bitbucket.org/HBerger/certemy_shared_web/commits/abe999cdb4a06081c5b5f89193074d45a9d2a284)
* [Add ability to work with object values at multi-select](https://bitbucket.org/HBerger/certemy_shared_web/commits/6fc792454fdf8d65835a80517524cd6ed2713d41)
* [Add ability to use multi-select as form-control](https://bitbucket.org/HBerger/certemy_shared_web/commits/b9b9fa2dd36081ae2ab4f5a14d1f06a2f121f2e3)
* [Add disabled state, when there are no options](https://bitbucket.org/HBerger/certemy_shared_web/commits/78484f34a5f2ed2a3aa45e18a8b1daacb5d8e293)
* [Add base multi-select component](https://bitbucket.org/HBerger/certemy_shared_web/commits/5b2f7746aca18af643d38f96ae3351fe9e91fbcb)
####Select
* [Prevent cm-select value transform to string](https://bitbucket.org/HBerger/certemy_shared_web/commits/3c4ae75fdc6218488085de48911c6b354079a8da)
### SmartTable
* [Add ability to disable checkbox cell](https://bitbucket.org/HBerger/certemy_shared_web/commits/42ea5999b73bc1e00307fe3142c97f89d061b82a)
* [StopPropagation on checkbox click](https://bitbucket.org/HBerger/certemy_shared_web/commits/4bb513b6df1b9b208326f0a6c2b34a78189239d1)
# 1.5.1
* [SmartTable: Add interface for event at SmartTablePopoverComponent](https://bitbucket.org/HBerger/certemy_shared_web/commits/d4e3df7ad089faae3c4d23ae4c23a71ebced1355)
* [Change rxjs imports to 5 version. Reexport SmartModal interfaces and constants](https://bitbucket.org/HBerger/certemy_shared_web/commits/b80c15db710037c10fac428a4b620dc4316e5d7d)
# 1.5.0
* [Migrate to Angular6](https://bitbucket.org/HBerger/certemy_shared_web/commits/c5657db749945ee80486df04f93f43ee97433d4a)
## Common lib
* [Add build for ui-components lib](https://bitbucket.org/HBerger/certemy_shared_web/commits/2c6939e4a9543329783a21c8e35bdba91460089a)
* [Add build for common lib](https://bitbucket.org/HBerger/certemy_shared_web/commits/030375227e729a98ab67207261ea63459df361fa)
### Directives
* [Update scroll-top directive](https://bitbucket.org/HBerger/certemy_shared_web/commits/91717ed19a6e96445c57025da49870dd8095f9ed)
* [Update scroll-top directive](https://bitbucket.org/HBerger/certemy_shared_web/commits/c47384338708972abf504b04fa08c2ccd400dfe7)
* [Add scroll-top directive](https://bitbucket.org/HBerger/certemy_shared_web/commits/54d965e45da92d9a85934d168372eecd232b7bd2)
### Pipes
* [Notify if MomentFormatterPipe got falsy value](https://bitbucket.org/HBerger/certemy_shared_web/commits/3ebd7cea33cc7fa49a7e2f004995abb1457d2785)
* [Fix moment-mini imports](https://bitbucket.org/HBerger/certemy_shared_web/commits/23119f321edd32ecda200b3081cf584b60f9bdec)
### Custom validators
* [Add isExist validator](https://bitbucket.org/HBerger/certemy_shared_web/commits/a9eeb9467730c376ebe61ee3bd4e3a47261bc6a4)
* [Update files validator](https://bitbucket.org/HBerger/certemy_shared_web/commits/5f050fc5ac8cc7bc3bfbb8d71bfab58625444e4d)
* [Update files validator](https://bitbucket.org/HBerger/certemy_shared_web/commits/04b66abe6010e0179a8edaa65d42eae1df7c3c7c)
* [Create files validator](https://bitbucket.org/HBerger/certemy_shared_web/commits/c5285509dbb88d7d1e3e8baa2132687cbabbd6e1)
### Guards
* [Rename guard filename](https://bitbucket.org/HBerger/certemy_shared_web/commits/fbb813348f8b8d3a5bf2c8d60892355c72c01455)
* [Redirect to 404](https://bitbucket.org/HBerger/certemy_shared_web/commits/c58f0d97c23150e80bb447acc166b16036e587e4)
* [Remove provide so guards are provided in app module](https://bitbucket.org/HBerger/certemy_shared_web/commits/4b892376052024c200c873bf56b8e96363b96808)
* [Create number param guard](https://bitbucket.org/HBerger/certemy_shared_web/commits/21e3c76628070a783969a64cd52086c6f859dab5)
## UI Components lib
* [Add build for ui-components-guide lib](https://bitbucket.org/HBerger/certemy_shared_web/commits/9cb69270bb1a4abc9bf68397bfbf89a8747b40c8)
### Typeahead
* [Add cursor pointer and change bg color for result items](https://bitbucket.org/HBerger/certemy_shared_web/commits/df4deea9e4e8a258cb4a26ffaedfb8a09b480331)
### Breadcrumbs
* [reexport breadcrumbs from index](https://bitbucket.org/HBerger/certemy_shared_web/commits/f4b04399c0cce696006612ef91195b7c98243b6a)
* [Add variable for breadcrumbs height](https://bitbucket.org/HBerger/certemy_shared_web/commits/7fd9c35eeee8aeb13fa3f31f828f7a0a948047ab)
* [Update breadcrumbs component module](https://bitbucket.org/HBerger/certemy_shared_web/commits/44533a2a69b53103aac192386373df551b89c439)
* [Add breadcrumbs component module](https://bitbucket.org/HBerger/certemy_shared_web/commits/22c47cffb5aed8b19ffe522368d609f37963aef4)
### MessageQuery
* [Move message-query into ui-components](https://bitbucket.org/HBerger/certemy_shared_web/commits/e8bf97082d657412bbbabc9bcea37580cef9f056)
### Loader
* [Move loader into ui-components](https://bitbucket.org/HBerger/certemy_shared_web/commits/d93d9e41eceb11b98e5bbc40d67d32a1ac60072e)
### Icon
* [Add 'info-circle' icon. Remove icrc logo svg](https://bitbucket.org/HBerger/certemy_shared_web/commits/fe05e5699d8a686dc043b4ed779fc8f0a5406572)
### FormControlErrors 
* [Display only one error](https://bitbucket.org/HBerger/certemy_shared_web/commits/8ab5a3d027420e163fc21ee75ec7e4ef1e51bd20)
* [Remove ngIf](https://bitbucket.org/HBerger/certemy_shared_web/commits/cf6d3aa641ab8bbf49ea12cc17c5e7a3a9e7a4c5)
* [Use thunk](https://bitbucket.org/HBerger/certemy_shared_web/commits/3e048d82ed8e19f75494c9cd278f0b27292d80f5)
* [Create form-control-errors component](https://bitbucket.org/HBerger/certemy_shared_web/commits/c265bb948ba952cdc5a8b092a4bafaca68be32d7)
### FileUpload
* [Add provider and extract code](https://bitbucket.org/HBerger/certemy_shared_web/commits/33ef39c0426bae09762e43c1ce3153bbc6f5de98)
* [Implement controlvalueaccesor](https://bitbucket.org/HBerger/certemy_shared_web/commits/2ff62ae02e0cda3dda813ddb852e00a35652e944)
* [Update icons. Update file input component icon](https://bitbucket.org/HBerger/certemy_shared_web/commits/0e4bf77f02edc9478c72b22aa1edae9f57013256)
### SmartTable
* [Smart-table popover - run document listeners outside of angular to not trigger CD](https://bitbucket.org/HBerger/certemy_shared_web/commits/8f495e4aef5b5b95be6ade2e2ba2132c418b98cf)
* [Add skipNextRowClick to smart-table if event was already handled](https://bitbucket.org/HBerger/certemy_shared_web/commits/450c350e6a9ce3fe21f198a43c53ff682904bb0a)
* [Remove allowSyntheticDefaultImports, revert to old default imports](https://bitbucket.org/HBerger/certemy_shared_web/commits/84040f8de9481ac2e261086b4228c5fdc3707260)
* [Fix aot build](https://bitbucket.org/HBerger/certemy_shared_web/commits/96b8db4fc09a90a86502556fdc197dfec044abbf)
### TableTabs
* [Wrap TableTabsComponent with module](https://bitbucket.org/HBerger/certemy_shared_web/commits/f9f15ee525cd215ac9f25ec4e52a99d1f903fa93)
### ActionModal
* [Add reexports for action modal](https://bitbucket.org/HBerger/certemy_shared_web/commits/16fc51a8d7c9cfccad3c21a62b8b17c6c3f78845)
### Datepicker
* [Remove caching of previous datepicker value](https://bitbucket.org/HBerger/certemy_shared_web/commits/e161d30a014cfcafa560e6532cf9565fa1dfe62a)
* [Run document listeners outside of angular to not trigger CD](https://bitbucket.org/HBerger/certemy_shared_web/commits/1de512a3b60d61779d46668ca1bb3daa5d9a33e1)
* [Display datepicker as a block](https://bitbucket.org/HBerger/certemy_shared_web/commits/1c05a9c7d387f5a9ec874e2e498711299c1d13b7)
### SmartModal
* [Add 'x-lg' size for modals](https://bitbucket.org/HBerger/certemy_shared_web/commits/bdf7249e1a0a343afaec5c47b5f05b4f3d8dcf9e)
* [Add openNotification method to smart modal service](https://bitbucket.org/HBerger/certemy_shared_web/commits/133c45e086f85b417ec94ec1c687c9db894aaa91)
* [Adjust interfaces for Confirmation and Notification modals](https://bitbucket.org/HBerger/certemy_shared_web/commits/133c45e086f85b417ec94ec1c687c9db894aaa91)
# 1.4.0
## Common lib
### Custom validators
* [Import IconModule to PopoverLayoutModule](https://bitbucket.org/HBerger/certemy_shared_web/commits/3bcea9cb63ad5d147744844ed743c059585db04a)
* [Add rangeLength validator](https://bitbucket.org/HBerger/certemy_shared_web/commits/84f032db5d11a14c64a7ea51852d6545e0fb0273)
* [Add isEmptyInputValue that allow controls to be optional. add withMessage](https://bitbucket.org/HBerger/certemy_shared_web/commits/c4ee485da6daa7e9bc9c33cbf3b0642d5d1a029f)
## UI Components lib
### Preloader
* [Add preloader, example](https://bitbucket.org/HBerger/certemy_shared_web/commits/4144a437b035eab5e3216e1ee535ed8c2a93d697)
### FileUpload
* [Change all files are valid to exist and valid](https://bitbucket.org/HBerger/certemy_shared_web/commits/972054e24100b3814186e7938a72b2581e4d6ded)
* [Add func for check if all files are valid](https://bitbucket.org/HBerger/certemy_shared_web/commits/e99149b6cbb0feecaad6be77963a2d042580a8c3)
### Icon
* [Fix IE freeze after removing svg use tag from DOM](https://bitbucket.org/HBerger/certemy_shared_web/commits/853e93c35c01041349eca9715073304ebd766ea9)
* [Add icon-remove](https://bitbucket.org/HBerger/certemy_shared_web/commits/632097e12b0dcb9a222a4cc9746c8749f8d7b5a3)
### PopoverLayout
* [Add popover layout module](https://bitbucket.org/HBerger/certemy_shared_web/commits/e563b88aaf2153a8cd7798c141b28a683fd335e3)
* [Add popover layout module](https://bitbucket.org/HBerger/certemy_shared_web/commits/e563b88aaf2153a8cd7798c141b28a683fd335e3)
### PageActions
* [Render data-id properly](https://bitbucket.org/HBerger/certemy_shared_web/commits/0517c4ab69f22b1088554e9aeb4133edab66d453)
* [Add data-id optional props](https://bitbucket.org/HBerger/certemy_shared_web/commits/95b24160651e815e94167630e897d6487ef3f237)
### SmartForm
* [Add SmartForm.TypeaheadField example](https://bitbucket.org/HBerger/certemy_shared_web/commits/2854f5186fe499e00375c636877001586bc1ab73)
### Typeahead
* [Add optional typeahead service for config](https://bitbucket.org/HBerger/certemy_shared_web/commits/a6c161572e0561c98be7c7cebed70b587ab5c959)
### SmartTable
* [Update styles for smart table. Add -item-bg-color variable](https://bitbucket.org/HBerger/certemy_shared_web/commits/8e4d81d6852df338a1892426dc7fe477599d9bba)
* [SmartCheckCell only displays it's value, but doesn't change it](https://bitbucket.org/HBerger/certemy_shared_web/commits/4dfd6b9713dcff63549f5adbb65f4cc307306f18)
### SmartModal
* [Add re-export of smart-modal module and service](https://bitbucket.org/HBerger/certemy_shared_web/commits/e90368c74aa7c6641452b9c16fb17deae0e58650)
* [Add smart modal open, confirm, unsaved change wrappers](https://bitbucket.org/HBerger/certemy_shared_web/commits/db73daae611dc6e9612c203aaa11d2c1bc4efd26)
# 1.3.2
* [SplitObservable: Share observable inside splitOservable helper.](https://bitbucket.org/HBerger/certemy_shared_web/commits/6fc467bdc541a6f0dc8db695a71196d683e11af1)
* [Typeahead: Add setDisabledState](https://bitbucket.org/HBerger/certemy_shared_web/commits/04c4d2bd321c9259a042056c4f37748c69f3b260)
* [Typeahead: Clear on blur, patch onDocumentClick, dont change _inputValueBackup](https://bitbucket.org/HBerger/certemy_shared_web/commits/3d02dea356855bdd7cf1b017846a0b674efe9edb)

# 1.3.1
* [Fix modal footer height for mobile view.](https://bitbucket.org/HBerger/certemy_shared_web/commits/3cc6b6fe0ac244f1264810cbc501d40211f7d14c)
* [Adjust required validator to use with different types](https://bitbucket.org/HBerger/certemy_shared_web/commits/81844eedfd464f34f5656b658b71882190c66569)

# 1.3.0
* [Add IE launcher for tests.](https://bitbucket.org/HBerger/certemy_shared_web/commits/e01a389908efd84541c9bea5d23e9bb8cfae1442)
### Common Lib
* [Refactor email validator to have isValid check](https://bitbucket.org/HBerger/certemy_shared_web/commits/8731a412776dd01e83dde3be621c6da2dd8b5fb7)
* [Add required validator, remove namespace.](https://bitbucket.org/HBerger/certemy_shared_web/commits/5f77b4490809fa8f66855e9419e1c715d1ec6765)
* [Fix private namespaces creation at decorators in IE11.](https://bitbucket.org/HBerger/certemy_shared_web/commits/67e7e9c7d4e3d44389eb500b68e4008b46b27cd2)
* [Add reexport for ComputedFrom decorator.](https://bitbucket.org/HBerger/certemy_shared_web/commits/efa30b02207f252cdc86d5e418f6ed839dc3a85b)
* [ComputedFrom store cache on instance, add tests](https://bitbucket.org/HBerger/certemy_shared_web/commits/33a7656cb4b76097c638967dfa923a46de67e02a)
* [Fix typings for requestCompleted helper.](https://bitbucket.org/HBerger/certemy_shared_web/commits/86e5525f136e3d825d01d8bd7e8a9f98ab8feb66)
* [Add splitObservable helper.](https://bitbucket.org/HBerger/certemy_shared_web/commits/7518c4d7141adf277b68f60493c72ce1a807ffb2)
* [Split helpers into functions.](https://bitbucket.org/HBerger/certemy_shared_web/commits/c32c35698daed66d7acb3068d073cacdf7c94e1c)
### UI Components lib
* [Allow to use loader service with non HttpService call](https://bitbucket.org/HBerger/certemy_shared_web/commits/6fea8591f2871534ac8ac6f9ae4ca007e77312dc)
* [Add ability to pass custom placeholder to typeahead.](https://bitbucket.org/HBerger/certemy_shared_web/commits/079ffed91916e42f043e2b414494cab948366801)

# 1.2.2
* [Increase datepicker z-index](https://bitbucket.org/HBerger/certemy_shared_web/commits/af0c20c24e7a59306ad08d7f9acd0f7d2afab5d5)

# 1.2.1
## Features
### Common lib
* [Add AutoSub decorator](https://bitbucket.org/HBerger/certemy_shared_web/commits/79054e6deac6134380ca1e24592995e93015a6dd)

### UI Components lib
* [Remove footer tag from card-footer](https://bitbucket.org/HBerger/certemy_shared_web/commits/4ebaf8e73df450b7f01cc824d786542e530d4818)
* [Update param default values at `cm-cart-footer-pagination`](https://bitbucket.org/HBerger/certemy_shared_web/commits/4d3f36d164b3dc52ac092119cda0817b4d886051)
* [Rename selector `cm-card-footer-pagination.component` -> `cm-card-footer-pagination`](https://bitbucket.org/HBerger/certemy_shared_web/commits/8068d45f652cf37c1c2d1efaa0b691712267fd7a)
* [Wrap pagination and displayed in card footer](https://bitbucket.org/HBerger/certemy_shared_web/commits/66e367fead881e6c858942f5136110c7a6ad31c2)
* [Add markForCheck to writeValue for all controls](https://bitbucket.org/HBerger/certemy_shared_web/commits/81e79c6ecaf22017c1e38ed586731b2d6a489320)
* [Add markForCheck to writeValue for checkbox](https://bitbucket.org/HBerger/certemy_shared_web/commits/67c46fb9b2dc3be011c79edf834bffaf999791e1)
* [Updated table and card styles](https://bitbucket.org/HBerger/certemy_shared_web/commits/19fd1ef484b12416726cf0299fe1dcdea90dc1b7)
* [Remove max-width for modals.](https://bitbucket.org/HBerger/certemy_shared_web/commits/39b430f02f8645c31fa0ca501b1435e76c6a15d4)
* [Fix button default padding. Remove min-height for modals.](https://bitbucket.org/HBerger/certemy_shared_web/commits/39000eac163539424314089c8e38e83b3e3bc85c)
* [Update style pathes.](https://bitbucket.org/HBerger/certemy_shared_web/commits/6557d2b2100c11fa8c6913ecdcb50b50d821b6c9)
* [Rename CertemyCommonModule to SharedCommonModule.](https://bitbucket.org/HBerger/certemy_shared_web/commits/868b72cb11d74a1cfa9d5fc407e4db37df33bdfc)
* [Add pipes from certemy project.](https://bitbucket.org/HBerger/certemy_shared_web/commits/3314be1afedca493091962aea3a3a92ab2989258)
* [Remove 'shared' from lib imports.](https://bitbucket.org/HBerger/certemy_shared_web/commits/1c403e1ba33569b9411b4d510221aacdcdd61b0b)
* [Remove border bottom from `cm-nav-tabs`.](https://bitbucket.org/HBerger/certemy_shared_web/commits/706a7516033b52f21db33a369bb6d149bf7e004c)
* [Split PageRowModule into PageHeaderModule and PageActionsModule](https://bitbucket.org/HBerger/certemy_shared_web/commits/6df6730e7359003a3388601482caec4b363fbdc4)

#### Datepicker
* [Add placement to datepicker](https://bitbucket.org/HBerger/certemy_shared_web/commits/93a154eb26b06bd1bc0cddd3b8df6ee88288a72d)
* [Add useDeprecatedDateOutputFormat](https://bitbucket.org/HBerger/certemy_shared_web/commits/4174ad678968857db1c96c72e02d76da5f3e9bb9)
* [Open datepicker in body, close on click outside](https://bitbucket.org/HBerger/certemy_shared_web/commits/0ecda35ad56a862ccdd2dc3f369b8113486c12aa)

#### Typeahead
* [Do not emit null after item had been selected](https://bitbucket.org/HBerger/certemy_shared_web/commits/a90e2b2d7e8d02555affcffc69fea44dc570700f)
* [Do not emit on write value](https://bitbucket.org/HBerger/certemy_shared_web/commits/436eebeada8e8c339db2b32b78171d13cbe1b07b)
* [Do not emit the same value again](https://bitbucket.org/HBerger/certemy_shared_web/commits/9f2108c623ac803141dd64ba3e6080dbab736fd5)
* [Filter undefined value emitted on focus](https://bitbucket.org/HBerger/certemy_shared_web/commits/682be33b1cc91fa06775d42089b0c440444a0297)

#### Pagination
* [Adjust styles for pagination and nav-tabs.](https://bitbucket.org/HBerger/certemy_shared_web/commits/679080df484560fc36fe95c1e75490b2de68f520)
* [Add maxSize and pageSize input properties for pagination.](https://bitbucket.org/HBerger/certemy_shared_web/commits/ff756e8da46dc9c59a3276bbd708c73425149a44)
* [Add pagination module with pagination and pagination-displayed components.](https://bitbucket.org/HBerger/certemy_shared_web/commits/fc72740075812d0cfbdeec65de23b6a2e15ec9cb)

#### Icon
* [Update cm-icon module to use external svg sprite for icons.](https://bitbucket.org/HBerger/certemy_shared_web/commits/fdc1b0f7d2c1396743201479a19f504561b874b6)
* [Remove IconConfig class.](https://bitbucket.org/HBerger/certemy_shared_web/commits/682373dfed0a5010b3334eb6a32036f80291796a)
* [Add svg icon component with module.](https://bitbucket.org/HBerger/certemy_shared_web/commits/3c40c9b49dad82f715ca64e19112ab08327dc117)

#### Smart Table

* [Clarify implementations of canOpenPopover method](https://bitbucket.org/HBerger/certemy_shared_web/commits/2f86e512161b5d8b22d48e79bb612fa95b434048)
* [Add placement for popover](https://bitbucket.org/HBerger/certemy_shared_web/commits/f4cea7d0c13ba739629af115de294ebf9e463b06)
* [Increase clickable area for popover-cell](https://bitbucket.org/HBerger/certemy_shared_web/commits/20c236214f87e60b47bc34655889e56ab3b59671)
* [Dont prevent cell click event if it has no popover](https://bitbucket.org/HBerger/certemy_shared_web/commits/f266405624fc29b99aa2700b935018fa9ee5b348)
* [Cleanup unused getters and setter for input props](https://bitbucket.org/HBerger/certemy_shared_web/commits/eebe2b0fd2c98c247a20045d2c3b0609e6e7299a)
* [Added DynamicCell interface, all cell implement it](https://bitbucket.org/HBerger/certemy_shared_web/commits/fc9f1cc2ec31a78c2f231d8b9f96e0f879e1d67e)
* [Move conditions check deeper into cm-smart-table-popover](https://bitbucket.org/HBerger/certemy_shared_web/commits/75d204ddd906ee52b4d2e6a3d4c1b3c86c28f0a6)
* [Pass popover instance to popover template context](https://bitbucket.org/HBerger/certemy_shared_web/commits/a832ada0e78ca7a582b2e7038f45d50256118866)
* [https://bitbucket.org/HBerger/certemy_shared_web/commits/483becfbe0662b109b81dd089d0e30e9d8149a9c](https://bitbucket.org/HBerger/certemy_shared_web/commits/483becfbe0662b109b81dd089d0e30e9d8149a9c)
* [Replace div wrapper to span, dont buble up event from popover, popover dont inherit cursor style](https://bitbucket.org/HBerger/certemy_shared_web/commits/648324d67a1ad538978080a53363d96105435ca0)
* [Add Smart Template Cell with example.](https://bitbucket.org/HBerger/certemy_shared_web/commits/51f543b9de5841a363de5248664b1131ea54888d)
* [Rename SmartTable order input and output props.](https://bitbucket.org/HBerger/certemy_shared_web/commits/f59b88f6df2d9098ccaa0609321632ea9a6639d1)
* [Move orderedHeader props from config to root, sort action now pure](https://bitbucket.org/HBerger/certemy_shared_web/commits/007319e50bda5509c6424d8983cd7e9fb4b00d8f)
* [Update table header name](https://bitbucket.org/HBerger/certemy_shared_web/commits/63d3ac4c3eb28f10c95b39a8d8dacf07666f05b9)
* [Updated sort arrow](https://bitbucket.org/HBerger/certemy_shared_web/commits/164817d8780459048a66ab864552bf1dddcb7f57)
* [Updated sort arrow](https://bitbucket.org/HBerger/certemy_shared_web/commits/6c3fae81cfd8fcbe14dd5882a137dccc6904503a)
* [Add `cm-smart-table-popover`.](https://bitbucket.org/HBerger/certemy_shared_web/commits/31274e57d064a6cf8e9df6d0d528c6c3d33a3f51)
* [Rename StringCell to SmartStringCell](https://bitbucket.org/HBerger/certemy_shared_web/commits/101e85c0a0f8692bad92c9028b707c26cab81e83)
* [Fix arity mismatch in `onSelectRow` handler](https://bitbucket.org/HBerger/certemy_shared_web/commits/38eb8594ed29a8828d3191fb9b8c8cee63efb61a)
* [Make `cm-smart-table` sortable. Add example](https://bitbucket.org/HBerger/certemy_shared_web/commits/b1122bcf61f967d4031eb746cb24683627a97980)
* [Add default header type for `cm-smart-table`](https://bitbucket.org/HBerger/certemy_shared_web/commits/0860838ad7ab75ec1a1a04e4aceaa43e710df01a)
* [Refactor naming of Dynamic Cell components](https://bitbucket.org/HBerger/certemy_shared_web/commits/729dec6adc100731e5934eb748d21d82a03083be)
* [Extract Dynamic Cell Components from `cm-smart-table`. Add examples](https://bitbucket.org/HBerger/certemy_shared_web/commits/24d5ceff2ea3ff186b61065f976b71b722e55639)
* [Remove ErrorService from CheckedTableProvider](https://bitbucket.org/HBerger/certemy_shared_web/commits/6493b006b82301532c1b2cedb73770118d676cec)
* [Remove unused shared libs](https://bitbucket.org/HBerger/certemy_shared_web/commits/73ae8601cb8186b980beec15cd43838702be653f)
* [Reexport smart-table-provider Selectors](https://bitbucket.org/HBerger/certemy_shared_web/commits/2f2b61c5a73cda5603aab5a5e34c91ec6ff00644)
* [Replace RowAction enum to object](https://bitbucket.org/HBerger/certemy_shared_web/commits/f4f953e8be892fda4a01ea91868381598563ba7f)
* [Added base example for `cm-smart-table`. Split Header types](https://bitbucket.org/HBerger/certemy_shared_web/commits/e1c6fe4f04e94eb4b5de5d3599cfa65bb03a07d2)
* [Sync `cm-smart-table` with ICRC state](https://bitbucket.org/HBerger/certemy_shared_web/commits/d86a5265de4a5e3d26a9ff2f58207349e9195824)

# 1.2.0

## Features

### Sandbox app
* Multiple fixes and refactorings.

### UI Components lib
* [Added `cm-card` component.](https://bitbucket.org/HBerger/certemy_shared_web/commits/354133bf59e4d509b3b8a4653d54c3c493128a52?at=master)
* [Added location control.](https://bitbucket.org/HBerger/certemy_shared_web/commits/3ffeac85be20c4e576b74dd9fc38e9f87ccc3d4f?at=master)
* [Wrap nav-tabs component with module.](https://bitbucket.org/HBerger/certemy_shared_web/commits/829935caeb3a2c6b040ce0f81a36608af6bbda77?at=master)
* [Added `cm-sidebar` component](https://bitbucket.org/HBerger/certemy_shared_web/commits/502ff84479e601ffbd45d3964323c29ff70ebd7f?at=master)
* [Group form controls into single module.](https://bitbucket.org/HBerger/certemy_shared_web/commits/1485ad278604c67f117f46d20b115b4c21baa192?at=master)
* [Added range form control.](https://bitbucket.org/HBerger/certemy_shared_web/commits/0fb127efffd15a5f7605f02c069bcb006e7285a2?at=master)
* [Added portal module.](https://bitbucket.org/HBerger/certemy_shared_web/commits/e6a944ac756daf04d587bcd25fd59e04f731a942?at=master)

## Fixes
* [Copy theme lib before app start.](https://bitbucket.org/HBerger/certemy_shared_web/commits/d55fe906256b73471d63ca04bc976f9555ae5b5e?at=master)
* [Fix height for file-input](https://bitbucket.org/HBerger/certemy_shared_web/commits/903eafa2710f5c7b4d132358c197ee16ed774664?at=master)
* Other minor changes and fixes

# 1.1.0

Revisited project structure and how project libs should be consumed. No build step at current moment. Project should use same `@angular` and `typescript` versions.

## Features

### General
* [Added prepush hook to check formatting.](https://bitbucket.org/HBerger/certemy_shared_web/commits/797b54907f6eff09b390c527505b0ebdd874796b?at=release/1.1.0)
* [Remove build artefacts. Remove packagr from project.](https://bitbucket.org/HBerger/certemy_shared_web/commits/5a5d6ed47b7c7ffd68b177d3183c78c5ceb48cea?at=release/1.1.0)

### Sandbox app
* Added minimal `sandbox` app to showcase available components and possible ways to consume this components.

### UI Components lib
* Added `UiComponentsModule`. Copied from `@certemy-icrc-web` project.
* Added new input components and adjusted existing to enable 'is-editable' mode.
* Refactored `FileUploadModule`. Added new `compact` mode for files list.
* Added `small` modificator for datepicker input.
* [[4722] Add ability to download files from files list.](https://bitbucket.org/HBerger/certemy_shared_web/commits/f8086ab1e98e5dc94da6085086aba834f676ce34?at=release/1.1.0)

### Theme lib
* Revisited the way how to use `theme` lib across the apps.
* [Added gulp tasks to update node_modules with `theme` lib assets when developing locally.](https://bitbucket.org/HBerger/certemy_shared_web/commits/d79776b2edfcc7806fcaf41e71e9186673d8c3df?at=release/1.1.0)

## Fixes

* [[4511] change datepicker disabled input to readonly](https://bitbucket.org/HBerger/certemy_shared_web/commits/54fae4d74e01d6b223f24b306fb2057d48012bd1?at=release/1.1.0)
* [Move datepicker width to host element.](https://bitbucket.org/HBerger/certemy_shared_web/commits/38c4d025a4a6fdc3b108d2e4b5d07e3841ceddff?at=release/1.1.0)

# 1.0.0

Initial proof of concept release with minimal set of features.

## Features
 
Project is build with the use of `ng-packagr`. Build artifacts are under git version control.
 
* Added `LoaderModule` lib.
* Added `MessageQueryModule` lib.
