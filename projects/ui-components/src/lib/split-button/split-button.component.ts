import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cm-split-button',
  templateUrl: './split-button.component.html',
  styleUrls: ['./split-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SplitButtonComponent {
  @Input()
  set actions([primaryAction, ...secondaryActions]: SplitButtonAction[]) {
    this.primaryAction = primaryAction;
    this.secondaryActions = secondaryActions;
  }
  @Output() actionClick = new EventEmitter<string>();
  primaryAction: SplitButtonAction;
  secondaryActions: SplitButtonAction[] = [];
}

export interface SplitButtonAction {
  label: string;
  name: string;
}
