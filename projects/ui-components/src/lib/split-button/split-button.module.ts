import { NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { SplitButtonComponent } from './split-button.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [SharedCommonModule, NgbDropdownModule],
  declarations: [SplitButtonComponent],
  exports: [SplitButtonComponent],
})
export class SplitButtonModule {}
