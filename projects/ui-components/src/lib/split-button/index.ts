import { SplitButtonAction as SplitButtonActionInterface } from './split-button.component';
export * from './split-button.component';
export * from './split-button.module';
export type SplitButtonAction = SplitButtonActionInterface;
