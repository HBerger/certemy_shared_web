import { NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { PreloaderComponent } from './preloader.component';

@NgModule({
  imports: [SharedCommonModule],
  declarations: [PreloaderComponent],
  exports: [PreloaderComponent],
})
export class PreloaderModule {}
