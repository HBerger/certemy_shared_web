import { Component } from '@angular/core';

@Component({
  selector: 'cm-preloader',
  templateUrl: './preloader.component.html',
  styleUrls: ['./preloader.component.scss'],
})
export class PreloaderComponent {}
