import { NavTab as NavTabInterface } from './nav-tabs.component';
export * from './nav-tabs.component';
export * from './nav-tabs.module';
export type NavTab = NavTabInterface;
