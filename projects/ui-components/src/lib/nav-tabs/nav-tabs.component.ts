import { Component, Input, OnInit } from '@angular/core';

export interface NavTab {
  link: string;
  display: string;
  queryParams?: {
    [k: string]: any;
  };
}

@Component({
  selector: 'cm-nav-tabs',
  templateUrl: './nav-tabs.component.html',
  styleUrls: ['./nav-tabs.component.scss'],
})
export class NavTabsComponent implements OnInit {
  @Input() tabs: NavTab[] = [];
  @Input() disabled = false;
  constructor() {}

  ngOnInit() {}
}
