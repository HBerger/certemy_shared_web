import { Attribute, Component, OnInit, TemplateRef } from '@angular/core';
import { map } from 'rxjs/operators';
import { PortalService } from './portal.service';
import { of } from 'rxjs';
import { Observable } from 'rxjs';

@Component({
  selector: 'cm-portal-outlet',
  templateUrl: './portal-outlet.component.html',
})
export class PortalOutletComponent implements OnInit {
  portalTemplate$: Observable<TemplateRef<any>> = of(null);

  constructor(private portalService: PortalService, @Attribute('name') public name) {}

  ngOnInit() {
    if (!this.name) {
      throw new Error(`Provide 'name' attribute for PortalOutletComponent.`);
    }

    this.portalTemplate$ = this.portalService.portals$.pipe(map(portals => portals[this.name]));
  }
}
