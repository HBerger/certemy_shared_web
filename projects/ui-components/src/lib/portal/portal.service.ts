import { Injectable, TemplateRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Portals {
  [key: string]: TemplateRef<any>;
}

@Injectable()
export class PortalService {
  portals$ = new BehaviorSubject<Portals>({});

  constructor() {}

  addPortal({ portalId, view }) {
    this.portals$.next({
      ...this.portals$.value,
      [portalId]: view,
    });
  }

  removePortal({ portalId }) {
    this.portals$.next({
      ...this.portals$.value,
      [portalId]: undefined,
    });
  }
}
