import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalDirective } from './portal.directive';
import { PortalService } from './portal.service';
import { PortalOutletComponent } from './portal-outlet.component';

const components = [PortalDirective, PortalOutletComponent];

@NgModule({
  imports: [CommonModule],
  exports: components,
  declarations: components,
})
export class PortalModule {
  static providers = [PortalService];

  static forRoot(): ModuleWithProviders<PortalModule> {
    return {
      ngModule: PortalModule,
      providers: [PortalService],
    };
  }
}
