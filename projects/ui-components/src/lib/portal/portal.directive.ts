import { Directive, Input, OnDestroy, TemplateRef } from '@angular/core';
import { PortalService } from './portal.service';

@Directive({ selector: '[cmPortal]' })
export class PortalDirective implements OnDestroy {
  private portalId: string;

  constructor(private templateRef: TemplateRef<null>, private portalService: PortalService) {}

  @Input()
  set cmPortal(portalId: string) {
    this.portalId = portalId;
    this.portalService.addPortal({ portalId, view: this.templateRef });
  }

  ngOnDestroy() {
    this.portalService.removePortal({ portalId: this.portalId });
  }
}
