import { NgModule } from '@angular/core';

import { PageActionsComponent } from './page-actions.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedCommonModule } from '@certemy/common';

const components = [PageActionsComponent];

@NgModule({
  imports: [NgbDropdownModule, SharedCommonModule],
  declarations: components,
  exports: components,
})
export class PageActionsModule {}
