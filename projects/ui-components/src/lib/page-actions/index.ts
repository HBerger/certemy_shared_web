import { Action as ActionInterface, PageAction as PageActionInterface } from './page-actions.component';
export * from './page-actions.module';
export * from './page-actions.component';
export type Action = ActionInterface;
export type PageAction = PageActionInterface;
