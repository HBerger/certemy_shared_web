import { Component, EventEmitter, Input, Output } from '@angular/core';

export interface PageAction {
  name: string;
  type: Action;
  dataId?: string;
  disabled?: boolean;
}

export type Action = string;

@Component({
  selector: 'cm-page-actions',
  templateUrl: './page-actions.component.html',
  styleUrls: ['./page-actions.component.scss'],
})
export class PageActionsComponent {
  @Input() actions: PageAction[] = [];
  @Input() groupStart: number = null;
  @Input() disabled: boolean = false;
  @Output() action = new EventEmitter<Action>();
  id = Math.random();

  onActionClick(type) {
    this.action.emit(type);
  }

  getPrimaryActions(actions, groupStart) {
    return groupStart === null ? actions : actions.slice(0, groupStart);
  }

  getGroupedActions(actions, groupStart) {
    return groupStart === null ? [] : actions.slice(groupStart);
  }
}
