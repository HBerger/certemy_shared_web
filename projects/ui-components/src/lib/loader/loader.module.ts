import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './loader/loader.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoaderInterceptor } from './loader.interceptor';
import { LoaderService } from './loader.service';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { LoaderProgressBarComponent } from './loader-progress-bar/loader-progress-bar.component';

@NgModule({
  imports: [CommonModule, NgbProgressbarModule],
  declarations: [LoaderComponent, LoaderProgressBarComponent],
  exports: [LoaderComponent],
})
export class LoaderModule {
  static forRoot(): ModuleWithProviders<LoaderModule> {
    return {
      ngModule: LoaderModule,
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LoaderInterceptor,
          multi: true,
        },
        LoaderService,
      ],
    };
  }
}
