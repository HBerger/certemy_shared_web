import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cm-loader-progress-bar',
  templateUrl: './loader-progress-bar.component.html',
  styleUrls: ['./loader-progress-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoaderProgressBarComponent implements OnInit {
  @Input() progress;
  constructor() {}

  ngOnInit() {}
}
