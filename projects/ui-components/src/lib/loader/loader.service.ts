import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

export interface LoaderState {
  isLoading: boolean;
}

@Injectable()
export class LoaderService {
  private isLoading = false;
  private requestCounter = 0;
  private loaderSubject = new Subject<LoaderState>();
  public loaderState: Observable<LoaderState> = this.loaderSubject.asObservable();

  show() {
    if (!this.requestCounter && !this.isLoading) {
      this.isLoading = true;
      this.loaderSubject.next(<LoaderState>{ isLoading: this.isLoading });
    }
    this.requestCounter++;
  }

  hide() {
    this.requestCounter--;
    if (!this.requestCounter && this.isLoading) {
      this.isLoading = false;
      this.loaderSubject.next(<LoaderState>{ isLoading: this.isLoading });
    }
  }

  wrapObservable<T>(obs$: Observable<T>): Observable<T> {
    return Observable.create(observer => {
      this.show();
      return obs$.pipe(finalize(() => this.hide())).subscribe(observer);
    });
  }
}
