import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LoaderService } from '../loader.service';
import { delay, map, takeWhile, tap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { timer } from 'rxjs';
import { merge } from 'rxjs';
import { Observable } from 'rxjs';

@Component({
  selector: 'cm-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements OnInit {
  progress$: Observable<number>;
  isLoading = false;
  isScreenLocked = false;

  constructor(private loaderService: LoaderService, private cd: ChangeDetectorRef) {}
  ngOnInit() {
    this.progress$ = this.loaderService.loaderState.pipe(
      switchMap(({ isLoading }) => {
        if (isLoading) {
          this.isLoading = true;
          this.isScreenLocked = true;
          this.cd.detectChanges();
          return timer(0, 100).pipe(
            map(value => (1 + value) * 10),
            takeWhile(percents => percents <= 70),
          );
        } else {
          return this.complete$();
        }
      }),
    );
  }
  complete$() {
    const progressEnd$ = of(100).pipe(tap(() => (this.isScreenLocked = false)));
    const progressStart$ = of(0).pipe(
      delay(350),
      tap(() => (this.isLoading = false)),
    );

    return merge(progressStart$, progressEnd$);
  }
}
