export interface DropdownOption {
  id: number | string;
  title: string;
  separate?: boolean;
  icon?: string;
  disabled?: boolean;
  options?: DropdownOption[];
}
