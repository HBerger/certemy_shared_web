import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptionDropdownComponent } from './option-dropdown.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { IconModule } from '../icon/icon.module';

@NgModule({
  imports: [CommonModule, NgbDropdownModule, IconModule],
  exports: [OptionDropdownComponent],
  declarations: [OptionDropdownComponent],
  providers: [],
})
export class OptionDropdownModule {}
