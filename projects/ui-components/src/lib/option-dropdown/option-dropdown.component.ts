import {
  Component,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { DropdownOption } from './option-dropdown.interface';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";

@UntilDestroy()
@Component({
  selector: 'cm-option-dropdown',
  templateUrl: './option-dropdown.component.html',
  styleUrls: ['./option-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OptionDropdownComponent implements OnInit, OnDestroy {
  @Input() options: DropdownOption[];
  @Input() disabled = false;
  @Input() buttonValue = '';
  @Input() placement = 'bottom-left';
  @Input() container = null;
  @Input() dropdownIcon = 'more-option';
  @Output() optionChanged = new EventEmitter<number | string>();
  @Output() openChange = new EventEmitter<boolean>();
  @ViewChild('dropDown', { static: true }) dropDown: NgbDropdown;
  constructor() {}

  ngOnInit() {
    this.dropDown.openChange.pipe(untilDestroyed(this)).subscribe(isOpened => this.openChange.emit(isOpened));
  }

  ngOnDestroy() {}

  toggle() {
    if (!this.disabled) {
      this.dropDown.toggle();
    }
  }

  selectOption(selectedOptionId: string | number) {
    this.dropDown.toggle();
    this.optionChanged.emit(selectedOptionId);
  }
}
