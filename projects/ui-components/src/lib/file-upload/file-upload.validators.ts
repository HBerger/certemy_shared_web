import { UntypedFormControl } from '@angular/forms';
import { filesAreValid } from './file-upload.helpers';
import { DEFAULT_FILE_MAX_SIZE, DEFAULT_FILE_TYPES } from './file-upload.constants';

export const filesAreValidValidator = (maxSize = DEFAULT_FILE_MAX_SIZE, allowedTypes = DEFAULT_FILE_TYPES) => (
  control: UntypedFormControl,
) => (filesAreValid(control.value, maxSize, allowedTypes) ? null : { filesAreValid: true });
