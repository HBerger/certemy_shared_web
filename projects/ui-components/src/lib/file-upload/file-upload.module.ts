import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedCommonModule } from '@certemy/common';
import { IconModule } from '../icon/index';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FilesListComponent } from './files-list/files-list.component';
import { FileInputComponent } from './file-input/file-input.component';
import { FileSizePipe } from './file-pipes/file-size.pipe';
import { FileTypesPipe } from './file-pipes/file-types.pipe';
import { FileErrorsPipe } from './file-pipes/file-errors.pipe';
import { SmartModalModule } from '../smart-modal/smart-modal.module';
import { FilePreviewComponent } from './file-preview/file-preview.component';
import { SmartModalService } from '../smart-modal/smart-modal.service';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import {FileUploadService} from './file-upload.service';

const components = [
  FileUploadComponent,
  FileInputComponent,
  FilesListComponent,
  FileSizePipe,
  FileTypesPipe,
  FileErrorsPipe,
  FilePreviewComponent,
];

@NgModule({
    imports: [CommonModule, SharedCommonModule, IconModule, SmartModalModule, NgxDocViewerModule],
    declarations: components,
    exports: components,
    providers: [SmartModalService, FileUploadService]
})
export class FileUploadModule {}
