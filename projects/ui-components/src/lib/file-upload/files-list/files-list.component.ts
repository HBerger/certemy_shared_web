import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output,
} from '@angular/core';
import { DEFAULT_FILE_TYPES, DEFAULT_FILE_MAX_SIZE } from '../file-upload.constants';
import { FilesListItem } from '../file-upload.interface';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';
import {FileUploadService} from '../file-upload.service';

@Component({
  selector: 'cm-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FilesListComponent),
      multi: true,
    },
  ],
})
export class FilesListComponent implements ControlValueAccessor {
  @Input() files: FilesListItem[] = [];
  @Input() maxSize = DEFAULT_FILE_MAX_SIZE;
  @Input() allowedTypes = DEFAULT_FILE_TYPES;
  @Input() showControls = false;
  @Input() compact = false;
  @Input() showLoadingData = false;
  @Output() removeFile: EventEmitter<FilesListItem> = new EventEmitter();
  private onTouched = noop;
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef, private fileUploadService: FileUploadService) {}

  writeValue(value = []) {
    this.files = value;
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  openFilePreview(file) {
    this.fileUploadService.openPreviewPopup(file);
  }
}
