import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileTypes',
})
export class FileTypesPipe implements PipeTransform {
  transform(fileTypes: string[]) {
    return fileTypes.join(', ');
  }
}
