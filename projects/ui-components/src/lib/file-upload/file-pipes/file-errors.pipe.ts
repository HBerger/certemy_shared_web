import { Pipe, PipeTransform } from '@angular/core';
import { getFileErrorString } from '../file-upload.helpers';

@Pipe({
  name: 'fileErrors',
})
export class FileErrorsPipe implements PipeTransform {
  transform(file: File, maxSize: number, allowedTypes: string[]) {
    return getFileErrorString(file, maxSize, allowedTypes);
  }
}
