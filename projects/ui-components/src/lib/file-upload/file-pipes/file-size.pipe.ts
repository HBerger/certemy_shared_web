import { Pipe, PipeTransform } from '@angular/core';
import { getFileSizeString } from '../file-upload.helpers';

@Pipe({
  name: 'fileSize',
})
export class FileSizePipe implements PipeTransform {
  transform(size: number) {
    return getFileSizeString(size);
  }
}
