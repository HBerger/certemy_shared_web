import { Injectable } from '@angular/core';
import { FilePreviewComponent } from './file-preview/file-preview.component';
import { SmartModalService } from '../smart-modal/smart-modal.service';
import { FilesListItem } from './file-upload.interface';
import { take } from 'rxjs/operators';

@Injectable()
export class FileUploadService {
  constructor(private smartModalService: SmartModalService) {}

  openPreviewPopup(file: Partial<FilesListItem>, isPdf = false) {
    this.smartModalService
      .open(
        FilePreviewComponent,
        {
          file: file,
          isPdf,
        },
        { windowClass: 'x-lg' },
      )
      .pipe(take(1))
      .subscribe();
  }
}
