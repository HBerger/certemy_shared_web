import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FileReference } from '../file-upload.interface';
import { DOC_FILE_TYPES, IMAGE_FILE_TYPES } from '@certemy/ui-components';

enum VIEWER_TYPE {
  DOC_VIEWER = 'DOC_VIEWER',
  IMAGE_VIEWER = 'IMAGE_VIEWER',
  UNSUPPORTED_VIEWER = 'UNSUPPORTED_VIEWER',
}

@Component({
  selector: 'cm-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.scss'],
})
export class FilePreviewComponent implements OnInit {
  @Input() file: FileReference;
  @Input() isPdf: boolean;

  public viewerTypes = VIEWER_TYPE;
  isSpinnerActive = false;
  viewerType: VIEWER_TYPE;
  isImageFullHeight = false;

  get fileLink(): string {
    return this.file.url || this.file.link || this.file.fileLink;
  }

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {
    this.viewerType = this.getViewerType();
    this.isSpinnerActive = this.viewerType === this.viewerTypes.DOC_VIEWER && !this.file?.preview;
  }

  private getViewerType(): VIEWER_TYPE {
    const fileWithoutQueryParameters = this.fileLink.split('?')[0];
    const filePathParts = fileWithoutQueryParameters.split('.');
    const fileExtension = `.${filePathParts[filePathParts.length - 1]}`.toLowerCase();

    if (IMAGE_FILE_TYPES.includes(fileExtension)) {
      return VIEWER_TYPE.IMAGE_VIEWER;
    }

    if (DOC_FILE_TYPES.includes(fileExtension)) {
      return VIEWER_TYPE.DOC_VIEWER;
    }

    return VIEWER_TYPE.UNSUPPORTED_VIEWER;
  }

  close(): void {
    this.activeModal.dismiss();
  }

  openNewWindow(fileLink): void {
    window.open(`${fileLink}`, '_blank');
  }

  hideSpinner(): void {
    this.isSpinnerActive = false;
  }

  changeImageSize(): void {
    this.isImageFullHeight = !this.isImageFullHeight;
  }
}
