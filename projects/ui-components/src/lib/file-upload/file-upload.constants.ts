export const KILOBYTE = 1024;
export const MEGABYTE = 1024 * 1024;
export const DEFAULT_FILE_MAX_SIZE = 5;
export const IMAGE_FILE_TYPES = ['.gif', '.jpg', '.jpeg', '.png'];
export const DOC_FILE_TYPES = ['.xls', '.xlsx', '.docx', '.doc', '.pdf', '.rtf', '.txt', '.ppt', '.pptx'];
export const DEFAULT_FILE_TYPES = [...IMAGE_FILE_TYPES, ...DOC_FILE_TYPES];
