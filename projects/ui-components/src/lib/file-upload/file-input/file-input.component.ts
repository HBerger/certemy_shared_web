import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DEFAULT_FILE_TYPES } from '../file-upload.constants';

@Component({
  selector: 'cm-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss'],
})
export class FileInputComponent {
  @Input() allowedTypes = DEFAULT_FILE_TYPES;
  @Input() disabled: boolean;
  @Input() isMultipleFiles = true;
  @Output() change: EventEmitter<File[]> = new EventEmitter<File[]>();
  isDragHover = false;
  randomId = Math.random();

  onFilesSelect(event) {
    event.stopPropagation();
    const newFiles = [].slice.call(event.target.files);

    // IE11 triggers change event when resetting input elem value manually - skip this event
    if (!newFiles.length) {
      return;
    }

    this.change.emit(newFiles);
    event.target.value = '';
  }

  onDrop(event) {
    if (this.disabled) {
      return;
    }
    event.preventDefault();
    event.stopPropagation();

    let newFiles = [].slice.call(event.dataTransfer.files);

    if (!this.isMultipleFiles) {
      newFiles = newFiles[0];
    }

    this.isDragHover = false;
    this.change.emit(newFiles);
  }

  onDragEnter(event) {
    if (this.disabled) {
      return;
    }
    event.preventDefault();
    event.stopPropagation();
    this.isDragHover = true;
  }

  onDragLeave(event) {
    if (this.disabled) {
      return;
    }
    event.preventDefault();
    event.stopPropagation();
    this.isDragHover = false;
  }

  onDragOver(event) {
    if (this.disabled) {
      return;
    }
    event.preventDefault();
    event.stopPropagation();
    event.dataTransfer.dropEffect = 'add';
  }
}
