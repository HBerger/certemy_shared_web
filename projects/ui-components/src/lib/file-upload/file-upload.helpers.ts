import { KILOBYTE, MEGABYTE, DEFAULT_FILE_MAX_SIZE, DEFAULT_FILE_TYPES } from './file-upload.constants';

export const roundSize = (size: number) => {
  return Math.round(size * 10) / 10;
};

export const isFileSizeAllowed = (fileSize: number, maxSize: number): boolean => {
  return fileSize <= maxSize * MEGABYTE;
};

export const isFileTypeAllowed = (fileName: string, allowedTypes: string[]): boolean => {
  const loweredFileName = fileName.toLowerCase();
  const fileTypeRegExp = type => new RegExp(`\\${type}$`);
  return allowedTypes.some(type => fileTypeRegExp(type).test(loweredFileName));
};

export const getValidFiles = (
  files: File[],
  maxSize = DEFAULT_FILE_MAX_SIZE,
  allowedTypes = DEFAULT_FILE_TYPES,
): File[] => {
  return files.filter(file => isFileSizeAllowed(file.size, maxSize) && isFileTypeAllowed(file.name, allowedTypes));
};

export const filesAreValid = (
  files: File[],
  maxSize = DEFAULT_FILE_MAX_SIZE,
  allowedTypes = DEFAULT_FILE_TYPES,
): boolean => files.length === getValidFiles(files, maxSize, allowedTypes).length;

export const filesExistAndValid = (
  files: File[],
  maxSize = DEFAULT_FILE_MAX_SIZE,
  allowedTypes = DEFAULT_FILE_TYPES,
): boolean => files.length && filesAreValid(files, maxSize, allowedTypes);

export const getFileSizeString = (size: number): string => {
  let sizeString: string;
  // if file size bigger then 1000 KB  show in MB
  if (size > 1000 * KILOBYTE) {
    sizeString = roundSize(size / MEGABYTE) + ' MB';
  } else if (size < 1000) {
    sizeString = roundSize(size) + ' bytes';
  } else {
    sizeString = roundSize(size / KILOBYTE) + ' KB';
  }

  return sizeString;
};

export const getFileErrorString = (file: File, maxSize = DEFAULT_FILE_MAX_SIZE, allowedTypes = DEFAULT_FILE_TYPES) => {
  if (!isFileSizeAllowed(file.size, maxSize)) {
    return 'File is too large for upload';
  }

  if (!isFileTypeAllowed(file.name, allowedTypes)) {
    return 'Invalid file type for upload';
  }

  return '';
};
