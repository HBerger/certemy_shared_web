export interface FileReference {
  name: string;
  size: number;
  id: number;
  url?: string;
  link?: string;
  fileLink?: string;
  preview?: boolean;
}

export type FilesListItem = File | FileReference;
