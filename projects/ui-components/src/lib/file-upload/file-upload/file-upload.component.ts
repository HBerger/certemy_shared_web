import { ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { DEFAULT_FILE_TYPES } from '../file-upload.constants';
import { getValidFiles } from '../file-upload.helpers';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';

@Component({
  selector: 'cm-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => FileUploadComponent),
      multi: true,
    },
  ],
})
export class FileUploadComponent implements ControlValueAccessor {
  @Input() maxSize = 5; // max file size in MB
  @Input() files: Array<File> = [];
  @Input() showFilesList = true;
  @Input() allowedTypes: string[] = DEFAULT_FILE_TYPES;
  @Input() compact = false;
  @Output() change: EventEmitter<File[]> = new EventEmitter<File[]>();
  @Input() isMultipleFiles = true;

  onTouched = noop;

  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  onFilesSelect(newFiles: File[] | File) {
    const files = Array.isArray(newFiles) ? newFiles : [newFiles];

    this.files = this.updateFilesList(this.files, files);

    this.onChange();
  }

  onFileRemove(fileToRemove) {
    this.files = this.files.filter(file => file.name !== fileToRemove.name);

    this.onChange();
  }

  onChange() {
    this.change.emit(this.files);

    this.propagateChange(this.files);
    this.onTouched();
  }
  getValidFiles(): File[] {
    return getValidFiles(this.files, this.maxSize, this.allowedTypes);
  }

  private updateFilesList(currentFiles: File[], newFiles: File[]): File[] {
    if (currentFiles.length) {
      // Filter out file if there is a new one with the same name
      currentFiles = currentFiles.filter(file => !newFiles.find(newFile => newFile.name === file.name));
    }
    return [...currentFiles, ...newFiles];
  }

  writeValue(files: File[] = []) {
    this.files = files;
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }
}
