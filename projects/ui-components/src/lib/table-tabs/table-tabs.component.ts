import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cm-table-tabs',
  templateUrl: './table-tabs.component.html',
  styleUrls: ['./table-tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableTabsComponent {
  @Input() tabs: TableTab[] = [];
  @Output() tabClick = new EventEmitter<string>();

  trackByFn(_, tab) {
    return tab.name;
  }
}

export interface TableTab {
  label: string;
  name: string;
  count: number;
  active: boolean;
}
