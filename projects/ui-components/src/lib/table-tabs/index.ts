import { TableTab as TableTabInterface } from './table-tabs.component';
export * from './table-tabs.component';
export * from './table-tabs.module';
export type TableTab = TableTabInterface;
