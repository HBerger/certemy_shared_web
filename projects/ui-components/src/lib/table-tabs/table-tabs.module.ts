import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableTabsComponent } from './table-tabs.component';

@NgModule({
  imports: [CommonModule],
  exports: [TableTabsComponent],
  declarations: [TableTabsComponent],
})
export class TableTabsModule {}
