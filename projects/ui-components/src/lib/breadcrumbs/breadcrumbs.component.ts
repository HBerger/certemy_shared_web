import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import _get from 'lodash-es/get';
import { distinctUntilChanged, filter, map, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';

export const BREADCRUMB_HOME_LINK = 'HOME_LINK';

export enum BreadcrumbType {
  Home,
  Current,
  Simple,
}

export interface BreadCrumb {
  label: string;
  url: string;
}

@Component({
  selector: 'cm-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent {
  readonly BreadcrumbType = BreadcrumbType;
  breadcrumbs$: Observable<BreadCrumb[]> = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    distinctUntilChanged(),
    map(() => this.getAllBreadCrumbs(this.activatedRoute.root)),
    startWith(this.getAllBreadCrumbs(this.activatedRoute.root)),
  );

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  getAllBreadCrumbs(route: ActivatedRoute, url = '/', breadcrumbs: BreadCrumb[] = []): BreadCrumb[] {
    const isRootPath = !route.routeConfig;

    if (isRootPath && route.firstChild) {
      return this.getAllBreadCrumbs(route.firstChild, url, breadcrumbs);
    }

    const nextUrl = this.getNextUrl(route, url);
    const breadcrumb = this.getBreadcrumb(route, nextUrl);

    if (breadcrumb) {
      breadcrumbs = [...breadcrumbs, breadcrumb];
    }

    if (route.firstChild) {
      return this.getAllBreadCrumbs(route.firstChild, nextUrl, breadcrumbs);
    }

    return breadcrumbs;
  }

  getBreadcrumbType(breadcrumb: BreadCrumb, isLast: boolean): BreadcrumbType {
    if (breadcrumb.label === BREADCRUMB_HOME_LINK) {
      return BreadcrumbType.Home;
    } else if (isLast) {
      return BreadcrumbType.Current;
    } else {
      return BreadcrumbType.Simple;
    }
  }

  private getNextUrl(route: ActivatedRoute, url: string): string {
    const path = this.getPath(route);
    return `${url}${path ? path + '/' : ''}`;
  }

  private getBreadcrumb(route: ActivatedRoute, nextUrl: string): BreadCrumb | null {
    const label = _get(route, 'routeConfig.data.breadcrumb') as string;

    return label ? { label, url: nextUrl } : null;
  }

  private getPath(route: ActivatedRoute): string {
    const path = route.routeConfig ? route.routeConfig.path : '';

    if (path[0] === ':') {
      const paramName = path.slice(1);
      return route.params['value'][paramName];
    } else {
      return path;
    }
  }
}
