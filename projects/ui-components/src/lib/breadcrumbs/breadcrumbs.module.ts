import { NgModule } from '@angular/core';
import { BreadcrumbsComponent } from './breadcrumbs.component';
import { SharedCommonModule } from '@certemy/common';
import { IconModule } from '../icon/index';

@NgModule({
  imports: [SharedCommonModule, IconModule],
  declarations: [BreadcrumbsComponent],
  exports: [BreadcrumbsComponent],
})
export class BreadcrumbsModule {}
