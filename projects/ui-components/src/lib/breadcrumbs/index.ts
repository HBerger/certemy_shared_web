import { BreadCrumb as BreadCrumbInterface } from './breadcrumbs.component';
export * from './breadcrumbs.component';
export * from './breadcrumbs.module';
export type BreadCrumb = BreadCrumbInterface;
