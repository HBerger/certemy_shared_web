import { Component, Input } from '@angular/core';
import { SmartTable } from '../smart-table.constants';

@Component({
  selector: 'cm-smart-template-cell',
  template: `<ng-container *ngTemplateOutlet="data.header.value; context: data;"></ng-container>`,
})
export class SmartTemplateCellComponent implements SmartTable.DynamicCell {
  static type = SmartTable.ColumnTypeId.TEMPLATE;

  @Input() data: SmartTable.DynamicCellData;
}
