import {
  Component,
  ComponentFactoryResolver,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewContainerRef,
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SmartTable } from '../smart-table.constants';

@Component({
  selector: 'cm-dynamic-cell',
  template: ``,
})
export class DynamicCellComponent implements OnInit, OnDestroy {
  @Input()
  set component(value) {
    if (this.data) {
      this.updateComponentInstance(value, this.data);
    }
    this._component = value;
  }

  get component() {
    return this._component;
  }

  @Input()
  set data(value) {
    if (this.componentRef) {
      this.updateDataBinding(value);
    }
    this._data = value;
  }

  get data() {
    return this._data;
  }

  @Output() action = new EventEmitter();

  private _data;
  private _component;

  private readonly onDestroy$ = new Subject<void>();
  private componentRef;
  private componentSubscription;

  constructor(private viewContainerRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {}

  ngOnInit() {
    if (this.component && this.data) {
      this.updateComponentInstance(this.component, this.data);
    }
  }

  private updateDataBinding(newData) {
    (<SmartTable.DynamicCell>this.componentRef.instance).data = newData;
  }

  private updateComponentInstance(newComponent, data) {
    this.viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(newComponent);
    this.componentRef = this.viewContainerRef.createComponent(componentFactory);

    if (this.componentSubscription) {
      this.componentSubscription.unsubscribe();
    }

    const componentInstance = <SmartTable.DynamicCell>this.componentRef.instance;

    if (componentInstance.action) {
      this.componentSubscription = componentInstance.action
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(payload => this.action.emit(payload));
    }

    componentInstance.data = data;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
