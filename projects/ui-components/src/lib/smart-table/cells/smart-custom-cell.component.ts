import { Component, Input } from '@angular/core';
import { defaultIfEmpty, SmartTable } from '../smart-table.constants';

@Component({
  selector: 'cm-smart-custom-cell',
  template: `{{getValue | thunk : data}} `,
})
export class SmartCustomCellComponent implements SmartTable.DynamicCell {
  static type = SmartTable.ColumnTypeId.CUSTOM;

  @Input() data: SmartTable.DynamicCellData;

  getValue = ({ header, row }: SmartTable.DynamicCellData) => {
    return defaultIfEmpty((<any>header.value)(row));
  };
}
