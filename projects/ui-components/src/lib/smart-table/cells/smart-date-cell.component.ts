import { Component, Input } from '@angular/core';
import _get from 'lodash-es/get';
import { defaultIfEmpty, SmartTable } from '../smart-table.constants';
import { DateFormatPipe } from '@certemy/common';

@Component({
  selector: 'cm-smart-date-cell',
  template: `{{getValue | thunk : data}} `,
  providers: [DateFormatPipe],
})
export class SmartDateCellComponent implements SmartTable.DynamicCell {
  static type = SmartTable.ColumnTypeId.DATE;

  @Input() data: SmartTable.DynamicCellData;

  constructor(private dateFormatPipe: DateFormatPipe) {}

  getValue = ({ header, row }: SmartTable.DynamicCellData) => {
    const unformatedDate = _get(row, header.value, '');

    return unformatedDate ? this.dateFormatPipe.transform(unformatedDate) : defaultIfEmpty(unformatedDate);
  };
}
