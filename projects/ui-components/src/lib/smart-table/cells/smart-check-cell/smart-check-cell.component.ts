import { Component, EventEmitter, Input, Output } from '@angular/core';
import _get from 'lodash-es/get';
import { defaultIfEmpty, SmartTable } from '../../smart-table.constants';
import { ComputedFrom } from '@certemy/common';

@Component({
  selector: 'cm-smart-check-cell',
  templateUrl: './smart-check-cell.component.html',
  styleUrls: ['./smart-check-cell.component.scss'],
})
export class SmartCheckCellComponent implements SmartTable.DynamicCell {
  static type = SmartTable.ColumnTypeId.CHECK;
  id = Math.random();
  @Input() data: SmartTable.DynamicCellData;
  @Output() action = new EventEmitter();

  @ComputedFrom('data')
  get value() {
    const { header, row } = this.data;
    return defaultIfEmpty(_get(row, header.value, false));
  }

  @ComputedFrom('data')
  get isDisabled() {
    const { header, row } = this.data;

    if (!('disable' in header)) {
      return false;
    }

    switch (typeof header.disable) {
      case 'function':
        return header.disable(row);
      case 'boolean':
        return header.disable;
      default:
        throw new Error(`Unknown type ${typeof header.disable} of 'disable' parameter.`);
    }
  }

  onSelectRow(event) {
    event.stopPropagation();
    event.preventDefault();
    const checked = event.target.checked;

    // Reset back, but show change event
    // Change should be made by table consumer, not by the table it self
    event.target.checked = !checked;
    this.action.emit({ checked });
  }

  stopPropagation(event) {
    event.stopPropagation();
  }
}
