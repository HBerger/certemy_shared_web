import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SmartTable } from '../smart-table.constants';

@Component({
  selector: 'cm-smart-action-cell',
  template: `
    <ng-container *ngFor="let actionType of data.header.value">
      <button *ngIf="(getRowAction | thunk : actionType: data.row) as action"
              type="button"
              class="cm-link"
              (click)="actionClick($event, action)">
        {{action.name}}
      </button>
    </ng-container>`,
})
export class SmartActionCellComponent implements SmartTable.DynamicCell {
  static type = SmartTable.ColumnTypeId.ACTIONS;

  @Input() data: SmartTable.DynamicCellData;
  @Output() action = new EventEmitter();

  getRowAction(action: SmartTable.RowActionType, row: SmartTable.Row): SmartTable.RowAction {
    return typeof action === 'function' ? action(row) : action;
  }

  actionClick(event, action: SmartTable.RowAction) {
    event.stopPropagation();
    this.action.emit({ action });
  }
}
