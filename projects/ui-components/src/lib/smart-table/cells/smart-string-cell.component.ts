import { Component, Input } from '@angular/core';
import _get from 'lodash-es/get';
import { defaultIfEmpty, SmartTable } from '../smart-table.constants';

@Component({
  selector: 'cm-smart-string-cell',
  template: `{{getValue | thunk : data}} `,
})
export class SmartStringCellComponent implements SmartTable.DynamicCell {
  static type = SmartTable.ColumnTypeId.STRING;

  @Input() data: SmartTable.DynamicCellData;

  getValue = ({ header, row }: SmartTable.DynamicCellData) => {
    return defaultIfEmpty(_get(row, header.value));
  };
}
