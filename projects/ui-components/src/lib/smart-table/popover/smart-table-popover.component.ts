import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  TemplateRef,
  Output,
  OnInit,
  OnDestroy,
  NgZone,
} from '@angular/core';
import { SmartTable } from '../smart-table.constants';
import _get from 'lodash-es/get';
import { Subject } from 'rxjs';
import { fromEvent } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

export interface Content {
  row: SmartTable.Row;
  header: SmartTable.Header;
}

@Component({
  selector: 'cm-smart-table-popover',
  templateUrl: './smart-table-popover.component.html',
  styleUrls: ['./smart-table-popover.component.scss'],
})
export class SmartTablePopoverComponent implements OnInit, OnDestroy {
  @Input() template: TemplateRef<any>;
  @Input() content: Content;
  @Output() skipNextRowClick = new EventEmitter();

  private closePopover;
  private readonly onDestroy$ = new Subject<void>();

  constructor(private elemRef: ElementRef, private ngZone: NgZone) {}

  ngOnInit() {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(window.document, 'click')
        .pipe(
          filter((event: MouseEvent) => this.closePopover && !this.elemRef.nativeElement.contains(event.target)),
          takeUntil(this.onDestroy$),
        )
        .subscribe(() => {
          this.ngZone.run(() => {
            this.closePopover();
            this.closePopover = null;
          });
        });
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  @HostListener('click')
  clickInsidePopover() {
    if (this.canOpenPopover(this.content)) {
      this.skipNextRowClick.emit(null);
    }
  }

  onCellClick(popover) {
    if (this.canOpenPopover(this.content)) {
      popover.open({ ...this.content, popover });
      this.closePopover = () => popover.close();
      this.skipNextRowClick.emit(null);
    }
  }

  getPopoverPlacement(header: SmartTable.Header): string {
    return _get(header as { popover: { placement: string } }, 'popover.placement', 'top');
  }

  private canOpenPopover({ header, row }: Content): boolean {
    if (header.popover) {
      const shouldOpen = header.popover.shouldOpen || (() => true);
      return shouldOpen(row);
    } else {
      return false;
    }
  }
}
