import { Injectable } from '@angular/core';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { SmartTable } from '../smart-table.constants';
import { TableTab } from '../../table-tabs';

export const TAB_NAMES = {
  ALL_TAB: 'ALL_TAB',
  SELECTED_TAB: 'SELECTED_TAB',
};

export interface State {
  tableTabs: TableTab[];
  selected: Set<number>;
  activeTabName: string;
  tableRows: SmartTable.Row[];
  collectionSize: number;
  filters: any;
}

export interface DataServiceResponse {
  rows: SmartTable.Row[];
  count: number;
}

export interface DataServiceConfig {
  service: (filters) => Observable<DataServiceResponse>;
}

export namespace Selectors {
  export const getTableTabs = (state: State) =>
    state.tableTabs.map(tab => {
      switch (tab.name) {
        case TAB_NAMES.SELECTED_TAB:
          return { ...tab, count: state.selected.size, active: tab.name === state.activeTabName };
        case TAB_NAMES.ALL_TAB:
          return { ...tab, count: state.collectionSize, active: tab.name === state.activeTabName };
        default:
          return tab;
      }
    });

  export const getTableRows = (state: State) =>
    state.tableRows.map(row => ({
      ...row,
      checked: state.selected.has(row.id),
    }));

  export const getCollectionSize = (state: State) =>
    state.activeTabName === TAB_NAMES.ALL_TAB ? state.collectionSize : state.selected.size;

  export const getCurrectFilters = (state: State) =>
    state.activeTabName === TAB_NAMES.ALL_TAB ? state.filters : { where: { id: { inq: Array.from(state.selected) } } };

  export const areFiltersHidden = (state: State) => state.activeTabName === TAB_NAMES.SELECTED_TAB;

  export const getSelectedIDs = (state: State) => state.selected;
}

@Injectable()
export class CheckedTableProvider {
  private state: State = {
    tableTabs: [
      {
        label: 'All',
        name: TAB_NAMES.ALL_TAB,
        count: 0,
        active: true,
      },
      {
        label: 'Selected',
        name: TAB_NAMES.SELECTED_TAB,
        count: 0,
        active: false,
      },
    ],
    selected: new Set(),
    activeTabName: TAB_NAMES.ALL_TAB,
    tableRows: [],
    collectionSize: 0,
    filters: {
      limit: 10,
      skip: 0,
    },
  };
  private state$ = new BehaviorSubject(this.state);
  private dataServiceConfig: DataServiceConfig;
  private updateTable$;

  constructor() {
    this.updateTable$ = new Subject();
    this.updateTable$
      .pipe(switchMap(() => this.dataServiceConfig.service(Selectors.getCurrectFilters(this.state))))
      .subscribe(({ rows, count }) => {
        this.state.tableRows = rows;
        if (this.state.activeTabName === TAB_NAMES.ALL_TAB) {
          this.state.collectionSize = count;
        }
        this.wasUpdated();
      });
  }

  select(selector: (state: State) => any): Observable<any> {
    return this.state$.pipe(
      map(selector),
      distinctUntilChanged(),
    );
  }

  onTabSelect(tabName: string) {
    if (tabName === this.state.activeTabName) {
      return;
    }
    this.state = {
      ...this.state,
      activeTabName: tabName,
    };

    this.updateTable();
  }

  onRowCheck(action) {
    const updateSelected = (selected, { payload, checked }): Set<number> => {
      const selectedCopy: Set<number> = new Set(selected);
      if (checked) {
        selectedCopy.add(payload.id);
        return selectedCopy;
      } else {
        selectedCopy.delete(payload.id);
        return selectedCopy;
      }
    };
    this.state = {
      ...this.state,
      selected: updateSelected(this.state.selected, action),
    };

    this.wasUpdated();
  }

  initTable({ data, dataServiceConfig }: { data: DataServiceResponse; dataServiceConfig: DataServiceConfig }) {
    this.dataServiceConfig = dataServiceConfig;
    this.state = {
      ...this.state,
      tableRows: data.rows,
      collectionSize: data.count,
    };

    this.wasUpdated();
  }

  updateTable() {
    this.updateTable$.next();
  }

  onFilterChange(filters) {
    this.state = {
      ...this.state,
      filters,
    };
    this.updateTable();
  }

  onSelectedSend() {
    this.state = {
      ...this.state,
      selected: new Set(),
    };
    this.state = {
      ...this.state,
      activeTabName: TAB_NAMES.ALL_TAB,
    };
    this.updateTable();
  }

  private wasUpdated() {
    this.state$.next(this.state);
  }
}
