import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import _flatten from 'lodash-es/flatten';
import { DYNAMIC_CELL_PROVIDERS, SmartTable } from './smart-table.constants';
import ColumnTypeId = SmartTable.ColumnTypeId;
import Order = SmartTable.Order;

const EXPAND_PROPERTY_NAME = '_expand';

@Component({
  selector: 'cm-smart-table',
  templateUrl: './smart-table.component.html',
  styleUrls: ['./smart-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SmartTableComponent implements OnChanges {
  /** @deprecated - use onCellAction */
  @Output() onRowCheck = new EventEmitter<{ payload: SmartTable.Row; checked: boolean }>();

  /** @deprecated - use onCellAction */
  @Output() onActionClick = new EventEmitter<{ action: SmartTable.RowAction; payload: SmartTable.Row }>();

  @Output() onRowClick = new EventEmitter();

  @Output() onCellAction = new EventEmitter<{ cellType: SmartTable.ColumnTypeId; row: SmartTable.Row; payload: any }>();

  @Output() onSelectAllRows = new EventEmitter<{ header: SmartTable.Header; payload: any }>();

  @Output() orderChange = new EventEmitter<SmartTable.OrderedHeader>();

  @Input() order: SmartTable.OrderedHeader = { header: null, order: null };

  @Input() headers: SmartTable.Header[];

  @Input() rows: SmartTable.Row[];

  @Input() rowDetailsRef?: TemplateRef<any>;

  @Input() allowSelectAllCheckboxes = false;

  @Input() allRowsChecked = false;
  @Input() childrenParam = 'children';

  @ViewChild('expandableRow', { static: true })
  expandableRow;

  readonly SERVICE_COLUMNS = [EXPAND_PROPERTY_NAME];

  ColumnTypeId = ColumnTypeId;
  private skipNextRowClick = false;

  private defaultConfig: SmartTable.Config = {
    rowClickable: true,
    noEntries: 'No record found',
    rowHasError: () => false,
  };
  private _config: SmartTable.Config = this.defaultConfig;
  @Input()
  set config(config: SmartTable.Config) {
    this._config = { ...this._config, ...config };
  }

  get config(): SmartTable.Config {
    return this._config;
  }

  private dynamicCellsProviders: any[];

  constructor(@Inject(DYNAMIC_CELL_PROVIDERS) dynamicCellsProviders: any[], private element: ElementRef) {
    this.dynamicCellsProviders = _flatten(dynamicCellsProviders);
  }

  ngOnChanges(changes) {
    if ((this.headers && changes.rows) || (this.rows && changes.headers)) {
      const hasChildren = this.rows.find(row => row[this.childrenParam]);
      const hasCollapsedHeader = this.headers.find(row => row.propertyName === EXPAND_PROPERTY_NAME);
      const firstNotCheckHeaderIndex = this.headers.findIndex(row => row.type !== SmartTable.ColumnTypeId.CHECK);
      if (hasChildren && !hasCollapsedHeader) {
        this.headers = [
          ...this.headers.slice(0, firstNotCheckHeaderIndex),
          {
            name: '',
            value: this.expandableRow,
            type: SmartTable.ColumnTypeId.TEMPLATE,
            propertyName: EXPAND_PROPERTY_NAME,
          },
          ...this.headers.slice(firstNotCheckHeaderIndex),
        ];
      }
    }
  }

  getCellComponent = (headerType: ColumnTypeId = ColumnTypeId.STRING) => {
    const cellComponent = this.dynamicCellsProviders.find(cell => cell.type === headerType);
    if (!cellComponent) {
      throw new Error(`Unsupported dynamicCell type: ${headerType}`);
    } else {
      return cellComponent;
    }
  };

  trackByFn = (index, row: SmartTable.Row) => {
    return row.id || index;
  };

  getHeaderName = (header: SmartTable.Header): string => {
    return header.name || '';
  };

  getHeaderCssClass = (header: SmartTable.Header): string => {
    const sortableClass = {
      [Order.SORTABLE]: 'fa-sort',
      [Order.ASC]: 'fa-sort-asc',
      [Order.DESC]: 'fa-sort-desc',
    };
    return sortableClass[this.getHeaderOrder(header)] || '';
  };

  onDynamicCellAction(header: SmartTable.Header, row: SmartTable.Row, payload) {
    this.onCellAction.emit({ cellType: header.type, row, payload });
    this.backwardCompatibleAdapter(header, row, payload);
  }

  onHeaderClick(header: SmartTable.Header) {
    if (header.sortable) {
      const newOrder = this.getNextHeaderOrder(this.getHeaderOrder(header));
      const newOrderedHeader = { header, order: newOrder };
      this.orderChange.emit(newOrderedHeader);
    }
  }

  // If event was handled inside child component but we still want trigger eventListenter outside (as document:click)
  onSkipNextRowClick() {
    this.skipNextRowClick = true;
  }

  serviceColumnClick(event, header: SmartTable.Header, row) {
    event.stopPropagation();
    switch (header.propertyName) {
      case EXPAND_PROPERTY_NAME:
        row._showChildren = !row._showChildren;
        break;
    }
  }

  onRowClicked(row) {
    if (!this.skipNextRowClick) {
      this.onRowClick.emit(row);
    }
    this.skipNextRowClick = false;
  }

  selectAllRows(header: SmartTable.Header) {
    this.onSelectAllRows.emit({ header, payload: { checked: this.allRowsChecked } });
    this.rows.forEach(row => {
      this.backwardCompatibleAdapter(header, row, { checked: this.allRowsChecked });
    });
  }

  private backwardCompatibleAdapter(header: SmartTable.Header, row: SmartTable.Row, payload) {
    switch (header.type) {
      case SmartTable.ColumnTypeId.CHECK:
        this.onRowCheck.emit({ payload: row, ...payload });
        break;
      case SmartTable.ColumnTypeId.ACTIONS:
        this.onActionClick.emit({ payload: row, ...payload });
        break;
    }
  }

  private getHeaderOrder(header: SmartTable.Header): Order {
    if (this.order.header === header) {
      return this.order.order;
    } else {
      return header.sortable ? Order.SORTABLE : null;
    }
  }

  private getNextHeaderOrder(order: Order): Order {
    if (!order) {
      return null;
    }
    const orderFlow = {
      [Order.SORTABLE]: Order.DESC,
      [Order.DESC]: Order.ASC,
      [Order.ASC]: Order.DESC,
    };
    return orderFlow[order];
  }

  getRowStyle(row: SmartTable.Row) {
    const borderColor = this.config.getBorderLeftColor && this.config.getBorderLeftColor(row);

    return { 'border-left': borderColor ? `5px solid ${borderColor}` : 'none' };
  }

  getHeaderStyle(header: SmartTable.Header) {
    const style: { [key: string]: string } = {};

    const { width, maxWidth } = header;

    if (width) {
      style['width'] = `${width}px`;
    }

    if (maxWidth) {
      style['max-width'] = `${maxWidth}px`;
    }

    return style;
  }
}
