import { NgModule, Provider } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SmartTableComponent } from './smart-table.component';
import { CheckboxModule } from '../form-controls/checkbox/checkbox.module';
import { SharedCommonModule } from '@certemy/common';
import { NgbPopoverModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { SmartStringCellComponent } from './cells/smart-string-cell.component';
import { DynamicCellComponent } from './cells/dynamic-cell.component';
import { SmartActionCellComponent } from './cells/smart-action-cell.component';
import { SmartDateCellComponent } from './cells/smart-date-cell.component';
import { SmartCustomCellComponent } from './cells/smart-custom-cell.component';
import { SmartCheckCellComponent } from './cells/smart-check-cell/smart-check-cell.component';
import { DYNAMIC_CELL_PROVIDERS } from './smart-table.constants';
import { SmartTablePopoverComponent } from './popover/smart-table-popover.component';
import { SmartTemplateCellComponent } from './cells/smart-template-cell.component';

const dynamicCells = [
  SmartStringCellComponent,
  SmartActionCellComponent,
  SmartDateCellComponent,
  SmartCustomCellComponent,
  SmartCheckCellComponent,
  SmartTemplateCellComponent,
];

const dynamicCellsProvider: Provider = {
  provide: DYNAMIC_CELL_PROVIDERS,
  useValue: dynamicCells,
  multi: true,
};

@NgModule({
    imports: [CommonModule, SharedCommonModule, CheckboxModule, NgbTooltipModule, NgbPopoverModule],
    declarations: [SmartTableComponent, DynamicCellComponent, SmartTablePopoverComponent, ...dynamicCells],
    exports: [SmartTableComponent],
    providers: [DatePipe, dynamicCellsProvider]
})
export class SmartTableModule {}
