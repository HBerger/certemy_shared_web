import { EventEmitter, InjectionToken, TemplateRef } from '@angular/core';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';

const NA = '-';

export const defaultIfEmpty = value => {
  if (value === null || value === undefined || (typeof value === 'string' && !value.length)) {
    return NA;
  } else {
    return value;
  }
};

export namespace SmartTable {
  export enum ColumnTypeId {
    STRING = 1,
    ACTIONS,
    DATE,
    CUSTOM,
    CHECK,
    TEMPLATE,
  }

  export type Header =
    | HeaderString
    | HeaderAction
    | HeaderDate
    | HeaderCustom
    | HeaderCheck
    | HeaderTemplate
    | HeaderTypeBase;

  export enum Order {
    SORTABLE = 'SORTABLE',
    ASC = 'ASC',
    DESC = 'DESC',
  }

  export interface PopoverTemplateContext {
    row: Row;
    header: Header;
    popover?: NgbPopover;
  }

  export interface PopoverConfig {
    template: TemplateRef<PopoverTemplateContext>;
    shouldOpen?: (row: Row) => boolean;
    placement?: string;
  }

  export interface HeaderBase {
    name: string;
    value: any;
    propertyName?: string;
    sortable?: boolean;
    popover?: PopoverConfig;
    tooltip?: (row: Row) => string;
    maxWidth?: number;
    width?: number;
    helpTooltip?: string | HelpTooltipConfig;
    additionalNames?: string;
  }

  export interface HelpTooltipConfig {
    text: string;
    class: string;
    container: string;
  }

  export interface HeaderTypeBase extends HeaderBase {
    type: any;
  }

  export interface HeaderDefaultTypeBase extends HeaderBase {
    type?: any;
  }

  export interface HeaderString extends HeaderDefaultTypeBase {
    type?: ColumnTypeId.STRING;
    value: string;
  }

  export interface RowAction {
    type: any;
    name: string;
  }

  export type ComputedRowActionsId = (row: Row) => RowAction;
  export type RowActionType = RowAction | ComputedRowActionsId;

  export interface HeaderAction extends HeaderTypeBase {
    type: ColumnTypeId.ACTIONS;
    value: RowActionType[];
  }

  export interface HeaderDate extends HeaderTypeBase {
    type: ColumnTypeId.DATE;
    value: string;
  }

  export type CustomHeaderValue = (row: Row) => string;

  export interface HeaderCustom extends HeaderTypeBase {
    type: ColumnTypeId.CUSTOM;
    value: CustomHeaderValue;
  }

  export interface HeaderCheck extends HeaderTypeBase {
    type: ColumnTypeId.CHECK;
    value: string;
    disable?: (row: Row) => boolean | boolean;
  }

  export interface HeaderTemplate extends HeaderTypeBase {
    type: ColumnTypeId.TEMPLATE;
    value: TemplateRef<DynamicCellData>;
  }

  export interface OrderedHeader {
    header: Header;
    order: Order;
  }

  export interface Config {
    rowClickable?: boolean;
    noEntries?: string;
    rowHasError?: (row: Row) => boolean;
    getBorderLeftColor?: (row: Row) => string | null;
  }

  export interface Row {
    [key: string]: any;
  }

  export interface DynamicCellData {
    header: Header;
    row: Row;
  }

  export interface DynamicCell {
    // Can't be well expressed with TS
    // static type = SmartTable.ColumnTypeId;
    data: DynamicCellData;
    action?: EventEmitter<any>;
  }
}

export const DYNAMIC_CELL_PROVIDERS = new InjectionToken<any[]>('DYNAMIC_CELL_PROVIDERS');
