import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFormatPipe, SharedCommonModule } from '@certemy/common';
import { SmartFiltersComponent } from './filters-config/smart-filters.component';
import { CheckboxGroupModule } from '../form-controls/checkbox-group';
import { CheckboxModule } from '../form-controls/checkbox/checkbox.module';
import { RangeModule } from '../form-controls/range/range.module';
import { DateRangeModule } from '../form-controls/date-range';
import { SelectModule } from '../form-controls/select';
import { TypeaheadModule } from '../form-controls/typeahead';
import { ActiveFiltersComponent } from './active-filters/active-filters.component';
import { SmartFiltersService } from './smart-filters.service';
import { FilterButtonComponent } from './filter-button/filter-button.component';
import { CustomControlModule } from '../form-controls/custom-control';
import { MultiSelectModule } from '../form-controls/multi-select';

@NgModule({
  imports: [
    CommonModule,
    SharedCommonModule,
    CheckboxModule,
    CheckboxGroupModule,
    RangeModule,
    DateRangeModule,
    SelectModule,
    TypeaheadModule,
    CustomControlModule,
    MultiSelectModule,
  ],
  providers: [SmartFiltersService, DateFormatPipe],
  declarations: [SmartFiltersComponent, ActiveFiltersComponent, FilterButtonComponent],
  exports: [SmartFiltersComponent, ActiveFiltersComponent, FilterButtonComponent],
})
export class SmartFiltersModule {
  static providers = [SmartFiltersService];
  static forRoot(): ModuleWithProviders<SmartFiltersModule> {
    return {
      ngModule: SmartFiltersModule,
      providers: [SmartFiltersService],
    };
  }
}
