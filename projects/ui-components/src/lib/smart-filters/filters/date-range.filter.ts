import { BaseFilter } from './base.filter';
import { FilterTypes, FilterValue } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';
import { DateFormatPipe } from '@certemy/common';
import { defaultIfEmpty } from '@certemy/ui-components';

export interface DateRangeFilterValue {
  from: number | null;
  to: number | null;
}

export class DateRangeFilter extends BaseFilter<DateRangeFilterValue> {
  readonly type = FilterTypes.dateRange;
  defaultValue: DateRangeFilterValue = {
    from: null,
    to: null,
  };
  minDate: Date | string;
  maxDate: Date | string;

  constructor(options) {
    super(options);
    this.defaultValue = {
      ...this.defaultValue,
      ...options.defaultValue,
    };
    this.minDate = options.minDate;
    this.maxDate = options.maxDate;
  }

  getLabel(value) {
    const { from, to } = value;
    let label = '';

    const dateFormatPipe = new DateFormatPipe();

    const formattedFromValue = from ? dateFormatPipe.transform(from, 'dateWithMonthName') : defaultIfEmpty(from);
    const formattedToValue = to ? dateFormatPipe.transform(to, 'dateWithMonthName') : defaultIfEmpty(to);

    if (from && to) {
      label = `${formattedFromValue} - ${formattedToValue}`;
    } else if (from && !to) {
      label = `from: ${formattedFromValue}`;
    } else if (!from && to) {
      label = `to: ${formattedToValue}`;
    }

    return label;
  }

  valueToLabel(filter: SmartFilters.FilterPayload<FilterValue>): SmartFilters.FilterLabel[] {
    const name = filter.name;
    const value = filter.value as DateRangeFilterValue;

    if (value.from || value.to) {
      return [
        {
          filterName: name,
          type: FilterTypes.dateRange,
          label: this.getLabel(value),
          title: this.label || '',
        },
      ];
    } else {
      return [];
    }
  }

  labelToValue(_labelData: SmartFilters.FilterLabel) {
    return { ...this.getDefaultValue() };
  }
}
