import { BaseFilterWithOptions, FilterOption } from './base.filter';
import { FilterTypes } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';
import { MultiSelect, MultiSelectParams } from '../../form-controls/multi-select/multi-select.component';
import { Observable } from 'rxjs';
import { SmartForm } from '@certemy/ui-components';
import { take } from 'rxjs/operators';
import { SmartFiltersService } from '../smart-filters.service';
import _cloneDeep from 'lodash-es/cloneDeep';

export type MultiselectFilterValue = number[] | FilterOption[];
const defaultParams: MultiSelectParams = {
  withSearch: false,
  noContentConfig: null,
  withChips: false,
  withSelectAll: false,
  optionDetailColumns: [],
  template: null,
  debounceTime: 0,
  onlyUnselectAllBtn: false,
};

export class MultiselectFilter extends BaseFilterWithOptions<MultiselectFilterValue> {
  readonly type = FilterTypes.multiselect;
  params: MultiSelectParams;
  request$: () => Observable<SmartForm.FieldOption[]>;
  defaultValue = [];
  valueType: MultiSelect.ValueType = MultiSelect.ValueType.Id;
  defaultActiveFiltersValue = null;

  constructor(options, smartFiltersService?: SmartFiltersService) {
    super(options);
    this.defaultValue = [...this.defaultValue, ...(options.defaultValue || [])];
    this.params = {
      ...defaultParams,
      ...options.params,
    };
    this.defaultActiveFiltersValue = options.defaultActiveFiltersValue || this.defaultActiveFiltersValue;
    this.valueType = options.valueType || this.valueType;

    if (
      smartFiltersService &&
      options.request$ &&
      options.defaultValue &&
      options.defaultValue.length &&
      !this.options.length
    ) {
      options
        .request$(options.options)
        .pipe(take(1))
        .subscribe(optionsArr => {
          const filters = smartFiltersService.config$.getValue();
          const index = filters.findIndex(({ name }) => name === this.name);

          this.options = optionsArr;
          smartFiltersService.config$.next([...filters.slice(0, index), _cloneDeep(this), ...filters.slice(index + 1)]);
        });
      return;
    }
  }

  valueToLabel(filter: SmartFilters.FilterPayload<MultiselectFilterValue>): SmartFilters.FilterLabel[] {
    const { name, value } = filter;

    const baseActiveFilter = {
      filterName: name,
      type: FilterTypes.multiselect,
      title: this.label || '',
    };

    if (value && value.length) {
      if (this.valueType === MultiSelect.ValueType.Object) {
        return (value as FilterOption[]).map((option: FilterOption) => {
          return {
            ...baseActiveFilter,
            optionId: option.id.toString(),
            label: option.name,
          };
        });
      }
      return (value as number[]).map(optionId => {
        return {
          ...baseActiveFilter,
          optionId: optionId.toString(),
          label: `${this.optionsNamesMap[optionId]}`,
        };
      });
    } else if (this.defaultActiveFiltersValue) {
      return [
        {
          ...baseActiveFilter,
          label: this.defaultActiveFiltersValue,
        },
      ];
    }
    return [];
  }

  labelToValue(labelData: SmartFilters.FilterLabel, currentValue: MultiselectFilterValue): MultiselectFilterValue {
    if (this.valueType === MultiSelect.ValueType.Object) {
      return (currentValue as FilterOption[]).filter(option => option.id.toString() !== labelData.optionId);
    }
    return (currentValue as number[]).filter(value => value.toString() !== labelData.optionId);
  }
}
