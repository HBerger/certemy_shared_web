import { BaseFilter } from './base.filter';
import { FilterTypes } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';
import { TemplateRef } from '@angular/core';
import _isArray from 'lodash-es/isArray';

interface CustomFilterSingleValue {
  id: number | string;
  name: string;
}

export type CustomFilterValue = CustomFilterSingleValue | CustomFilterSingleValue[];

export class CustomFilter extends BaseFilter<CustomFilterValue> {
  readonly type = FilterTypes.custom;
  template: TemplateRef<any>;
  defaultActiveFiltersValue = null;

  constructor(options) {
    super(options);
    this.template = options.template;
    this.defaultActiveFiltersValue = options.defaultActiveFiltersValue || this.defaultActiveFiltersValue;
  }

  valueToLabel(filter: SmartFilters.FilterPayload<CustomFilterValue>): SmartFilters.FilterLabel[] {
    let { name, value } = filter;

    const baseActiveFilter = {
      filterName: name,
      type: FilterTypes.custom,
      title: this.label || '',
    };

    const valueIsArray = _isArray(value);

    if (value && !valueIsArray) {
      value = value as CustomFilterSingleValue;
      return [
        {
          ...baseActiveFilter,
          label: value.name,
        },
      ];
    } else if (valueIsArray && (value as CustomFilterSingleValue[]).length) {
      value = value as CustomFilterSingleValue[];
      return value.map(option => {
        return {
          ...baseActiveFilter,
          optionId: option.id.toString(),
          label: option.name,
        };
      });
    } else if (this.defaultActiveFiltersValue) {
      return [
        {
          ...baseActiveFilter,
          label: this.defaultActiveFiltersValue,
        },
      ];
    }

    return [];
  }

  labelToValue(labelData: SmartFilters.FilterLabel, currentValue: CustomFilterValue): CustomFilterValue {
    return this.getDefaultValue();
  }
}
