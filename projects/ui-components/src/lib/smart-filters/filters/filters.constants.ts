import { CheckboxGroupFilter, CheckboxGroupFilterValue } from './checkbox-group.filter';
import { RangeFilter, RangeFilterValue } from './range.filter';
import { DateRangeFilter, DateRangeFilterValue } from './date-range.filter';
import { SelectFilter } from './select.filter';
import { TypeaheadFilter, TypeaheadFilterValue } from './typeahead.filter';
import { CustomFilter, CustomFilterValue } from './custom.filter';
import { MultiselectFilter, MultiselectFilterValue } from './multiselect.filter';

export enum FilterTypes {
  checkboxGroup,
  range,
  dateRange,
  select,
  typeahead,
  custom,
  multiselect,
}

export type Filter =
  | CheckboxGroupFilter
  | RangeFilter
  | DateRangeFilter
  | SelectFilter
  | TypeaheadFilter
  | CustomFilter
  | MultiselectFilter;
export type FilterValue =
  | CheckboxGroupFilterValue
  | RangeFilterValue
  | DateRangeFilterValue
  | string
  | TypeaheadFilterValue
  | CustomFilterValue
  | MultiselectFilterValue;
