import { SmartFilters } from '@certemy/ui-components';

export function getSelectedFilterOptionsIds(options: SmartFilters.CheckboxGroupFilterValue): number[] {
  return Object.keys(options)
    .filter(id => options[id])
    .map(id => +id);
}
