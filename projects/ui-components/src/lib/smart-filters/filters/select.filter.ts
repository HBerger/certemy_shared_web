import { BaseFilterWithOptions } from './base.filter';
import { FilterTypes } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';

export class SelectFilter extends BaseFilterWithOptions<string> {
  readonly type = FilterTypes.select;
  defaultValue = '';
  placeholder = 'Please select';
  defaultActiveFiltersValue = null;

  constructor(options) {
    super(options);
    this.defaultValue = this.defaultValue || options.defaultValue;
    this.placeholder = options.placeholder || this.placeholder;
    this.defaultActiveFiltersValue = options.defaultActiveFiltersValue || this.defaultActiveFiltersValue;
  }

  valueToLabel(filter: SmartFilters.FilterPayload<string>): SmartFilters.FilterLabel[] {
    const { name, value } = filter;

    const baseActiveFilter = {
      filterName: name,
      type: FilterTypes.select,
      title: this.label || '',
    };

    if (!value && this.defaultActiveFiltersValue) {
      return [
        {
          ...baseActiveFilter,
          label: this.defaultActiveFiltersValue,
        },
      ];
    }

    if (value) {
      return [
        {
          ...baseActiveFilter,
          label: this.optionsNamesMap[value],
        },
      ];
    } else {
      return [];
    }
  }

  labelToValue(labelData: SmartFilters.FilterLabel, currentValue: string): string {
    return this.defaultValue;
  }
}
