import { BaseFilter } from './base.filter';
import { FilterTypes, FilterValue } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';

export interface RangeFilterValue {
  from: number | null;
  to: number | null;
}

export class RangeFilter extends BaseFilter<RangeFilterValue> {
  readonly type = FilterTypes.range;
  defaultValue: RangeFilterValue = {
    from: null,
    to: null,
  };

  constructor(options) {
    super(options);
    this.defaultValue = {
      ...this.defaultValue,
      ...options.defaultValue,
    };
  }

  getLabel(value) {
    const { from, to } = value;
    let label = '';

    if (from && to) {
      label = `${from} - ${to}`;
    } else if (from && !to) {
      label = `from ${from}`;
    } else if (!from && to) {
      label = `till ${to}`;
    }

    return label;
  }

  valueToLabel(filter: SmartFilters.FilterPayload<FilterValue>): SmartFilters.FilterLabel[] {
    const name = filter.name;
    const value = filter.value as RangeFilterValue;

    if (value.from || value.to) {
      return [
        {
          filterName: name,
          type: FilterTypes.range,
          label: this.getLabel(value),
          title: this.label || '',
        },
      ];
    } else {
      return [];
    }
  }

  labelToValue(labelData: SmartFilters.FilterLabel) {
    return { ...this.getDefaultValue() };
  }
}
