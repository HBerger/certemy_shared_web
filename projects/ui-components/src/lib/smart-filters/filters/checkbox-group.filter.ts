import { BaseFilterWithOptions } from './base.filter';
import { FilterTypes, FilterValue } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';

export interface CheckboxGroupFilterValue {
  [key: string]: boolean;
}

export class CheckboxGroupFilter extends BaseFilterWithOptions<CheckboxGroupFilterValue> {
  readonly type = FilterTypes.checkboxGroup;
  defaultValue = this.options.reduce((fieldsMap, field) => {
    fieldsMap[field.id] = false;
    return fieldsMap;
  }, {});

  constructor(options) {
    super(options);
    this.defaultValue = {
      ...this.defaultValue,
      ...options.defaultValue,
    };
  }

  valueToLabel(filter: SmartFilters.FilterPayload<FilterValue>): SmartFilters.FilterLabel[] {
    const { name, value } = filter;

    if (value) {
      return Object.keys(value)
        .filter(optionId => value[optionId])
        .map(optionId => {
          return {
            filterName: name,
            type: FilterTypes.checkboxGroup,
            optionId: optionId,
            label: `${this.optionsNamesMap[optionId]}`,
            title: this.label || '',
          };
        });
    } else {
      return [];
    }
  }

  labelToValue(labelData: SmartFilters.FilterLabel, currentValue: CheckboxGroupFilterValue): CheckboxGroupFilterValue {
    return {
      ...currentValue,
      [labelData.optionId]: false,
    };
  }
}
