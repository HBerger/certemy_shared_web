import { FilterValue } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';

export abstract class BaseFilter<T> {
  public readonly type;
  public name: string;
  public label: string;
  public hidden: boolean;
  public disabled: boolean;
  public readonly defaultValue: T;

  protected constructor(options) {
    Object.assign(
      this,
      {
        hidden: false,
        disabled: false,
      },
      options,
    );
  }

  getDefaultValue() {
    return this.defaultValue;
  }

  abstract valueToLabel(filter: SmartFilters.FilterPayload<FilterValue>): SmartFilters.FilterLabel[];

  abstract labelToValue(labelData: SmartFilters.FilterLabel, currentValue: T): T;
}

export interface FilterOption {
  name: string;
  id: string | number;
}

export abstract class BaseFilterWithOptions<T> extends BaseFilter<T> {
  public options: FilterOption[] = [];

  get optionsNamesMap() {
    return this.options.reduce((optionsMap, option) => {
      optionsMap[option.id] = option.name;
      return optionsMap;
    }, {});
  }

  constructor(options) {
    super(options);
    this.options = options.options || this.options;
  }
}
