import { Observable } from 'rxjs';
import { BaseFilter } from './base.filter';
import { FilterTypes, FilterValue } from './filters.constants';
import * as SmartFilters from '../smart-filters.interfaces';

export type TypeaheadFilterValue = object | null;

export class TypeaheadFilter extends BaseFilter<TypeaheadFilterValue> {
  readonly type = FilterTypes.typeahead;
  public request$: (term: string) => Observable<any>;
  public formatter: (result: any) => string;
  public placeholder = 'Search';
  public tooltip: any;
  public defaultActiveFiltersValue = null;
  defaultValue = null;

  constructor(options) {
    super(options);
    this.request$ = options.request$;
    this.formatter = options.formatter;
    this.placeholder = options.placeholder || this.placeholder;
    this.defaultValue = this.defaultValue || options.defaultValue;
    this.tooltip = {
      enable: options.tooltipEnable,
      placement: options.tooltipPlacement,
    };
    this.defaultActiveFiltersValue = options.defaultActiveFiltersValue;
  }

  valueToLabel(filter: SmartFilters.FilterPayload<FilterValue>): SmartFilters.FilterLabel[] {
    const { name, value } = filter;

    const baseActiveFilter = {
      filterName: name,
      type: FilterTypes.typeahead,
      title: this.label || '',
    };

    if (!value && this.defaultActiveFiltersValue) {
      return [
        {
          ...baseActiveFilter,
          label: this.defaultActiveFiltersValue,
        },
      ];
    }

    return value
      ? [
          {
            ...baseActiveFilter,
            label: this.formatter(value),
          },
        ]
      : [];
  }

  labelToValue(labelData: SmartFilters.FilterLabel, currentValue: TypeaheadFilterValue): TypeaheadFilterValue {
    return this.defaultValue;
  }
}
