import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import * as SmartFilters from '../smart-filters.interfaces';
import * as Filters from '../filters';
import { SmartFiltersService } from '../smart-filters.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'cm-smart-filters',
  templateUrl: './smart-filters.component.html',
  styleUrls: ['./smart-filters.component.scss'],
})
export class SmartFiltersComponent implements OnInit, OnDestroy {
  filtersValue$: Observable<SmartFilters.FiltersValue> = this.smartFiltersService.value$;
  filtersConfig$: Observable<SmartFilters.FiltersConfig> = this.smartFiltersService.config$;
  @Output() filtersChange = new EventEmitter<SmartFilters.ChangeEvent<Filters.FilterValue>>();
  filtersValue: SmartFilters.FiltersValue;
  filtersConfig: SmartFilters.FiltersConfig;

  public readonly FILTER_TYPES = Filters.FilterTypes;
  private readonly onDestroy$ = new Subject<void>();

  constructor(private smartFiltersService: SmartFiltersService) {}

  ngOnInit() {
    this.filtersValue$.pipe(takeUntil(this.onDestroy$)).subscribe(newValue => (this.filtersValue = newValue));
    this.filtersConfig$.pipe(takeUntil(this.onDestroy$)).subscribe(newConfig => (this.filtersConfig = newConfig));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  onFilterChange(name: string, value: Filters.FilterValue) {
    const newValue = { name, value };
    let prevented = false;

    const event: SmartFilters.ChangeEvent<Filters.FilterValue> = {
      value,
      preventDefault() {
        prevented = true;
      },
    };

    this.filtersChange.emit(event);

    if (!prevented) {
      this.smartFiltersService.filtersChange(newValue);
    }
  }
}
