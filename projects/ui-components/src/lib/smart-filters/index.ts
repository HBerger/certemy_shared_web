import {
  FilterValue as _FilterValue,
  RangeFilterValue as _RangeFilterValue,
  DateRangeFilterValue as _DateRangeFilterValue,
  CheckboxGroupFilterValue as _CheckboxGroupFilterValue,
  TypeaheadFilterValue as _TypeaheadFilterValue,
  MultiselectFilterValue as _MultiselectFilterValue,
  CustomFilterValue as _CustomFilterValue,
  FilterTypes as _FilterTypes,
  Filter as _Filter,
  RangeFilter as _RangeFilter,
  DateRangeFilter as _DateRangeFilter,
  CheckboxGroupFilter as _CheckboxGroupFilter,
  SelectFilter as _SelectFilter,
  TypeaheadFilter as _TypeaheadFilter,
  MultiselectFilter as _MultiselectFilter,
  CustomFilter as _CustomFilter,
  FilterOption as _FilterOption,
  BaseFilter as _BaseFilter,
  BaseFilterWithOptions as _BaseFilterWithOptions,
  getSelectedFilterOptionsIds as _getSelectedFilterOptionsIds,
} from './filters';

import {
  FiltersConfig as _FiltersConfig,
  FilterLabel as _FilterLabel,
  FiltersValue as _FiltersValue,
  ChangeEvent as _ChangeEvent,
  FilterPayload as _FilterPayload,
  RemoveLabelEvent as _RemoveLabelEvent,
} from './smart-filters.interfaces';

export namespace SmartFilters {
  export type FilterValue = _FilterValue;
  export type RangeFilterValue = _RangeFilterValue;
  export type DateRangeFilterValue = _DateRangeFilterValue;
  export type CheckboxGroupFilterValue = _CheckboxGroupFilterValue;
  export type TypeaheadFilterValue = _TypeaheadFilterValue;
  export type MultiselectFilterValue = _MultiselectFilterValue;
  export type Filter = _Filter;
  export type FilterOption = _FilterOption;
  export type CustomFilterValue = _CustomFilterValue;
  export const RangeFilter = _RangeFilter;
  export const DateRangeFilter = _DateRangeFilter;
  export const CheckboxGroupFilter = _CheckboxGroupFilter;
  export const SelectFilter = _SelectFilter;
  export const TypeaheadFilter = _TypeaheadFilter;
  export const MultiselectFilter = _MultiselectFilter;
  export const CustomFilter = _CustomFilter;
  export const FilterTypes = _FilterTypes;
  export const BaseFilter = _BaseFilter;
  export const BaseFilterWithOptions = _BaseFilterWithOptions;
  export const getSelectedFilterOptionsIds = _getSelectedFilterOptionsIds;
}

export namespace SmartFilters {
  export type FiltersConfig = _FiltersConfig;
  export type FilterLabel = _FilterLabel;
  export type FiltersValue = _FiltersValue;
  export type ChangeEvent<T> = _ChangeEvent<T>;
  export type FilterPayload<T> = _FilterPayload<T>;
  export type RemoveLabelEvent = _RemoveLabelEvent;
}
export { SmartFiltersModule } from './smart-filters.module';
export { SmartFiltersService } from './smart-filters.service';
export { SmartFiltersComponent } from './filters-config/smart-filters.component';
export { FilterButtonComponent } from './filter-button/filter-button.component';
export { ActiveFiltersComponent } from './active-filters/active-filters.component';
