import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import * as SmartFilters from '../smart-filters.interfaces';
import * as Filters from '../filters';
import { SmartFiltersService } from '../smart-filters.service';
import { takeUntil } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { ComputedFrom } from '@certemy/common';

@Component({
  selector: 'cm-active-filters',
  templateUrl: './active-filters.component.html',
  styleUrls: ['./active-filters.component.scss'],
})
export class ActiveFiltersComponent implements OnInit {
  filtersValue$: Observable<SmartFilters.FiltersValue> = this.smartFiltersService.value$;
  filtersConfig$: Observable<SmartFilters.FiltersConfig> = this.smartFiltersService.config$;
  filtersValue: SmartFilters.FiltersValue | string;
  filtersConfig: SmartFilters.FiltersConfig;
  @Input() displayFilterName = false;
  @Output() removeLabel = new EventEmitter<SmartFilters.RemoveLabelEvent>();

  private readonly onDestroy$ = new Subject<void>();

  @ComputedFrom('filtersConfig')
  get filtersConfigMap(): { [filterName: string]: Filters.Filter } {
    return this.filtersConfig.reduce((filtersConfigMap, filter) => {
      filtersConfigMap[filter.name] = filter;
      return filtersConfigMap;
    }, {});
  }

  @ComputedFrom('filtersValue', 'filtersConfigMap')
  get activeFilters(): SmartFilters.FilterLabel[] {
    return Object.keys(this.filtersValue)
      .map(
        (filterName): SmartFilters.FilterLabel[] => {
          return this.filtersConfigMap[filterName].valueToLabel({
            name: filterName,
            value: this.filtersValue[filterName],
          });
        },
      )
      .reduce((result, labels) => [...result, ...labels], []);
  }

  constructor(private smartFiltersService: SmartFiltersService) {}

  ngOnInit() {
    this.filtersValue$.pipe(takeUntil(this.onDestroy$)).subscribe(newValue => (this.filtersValue = newValue));
    this.filtersConfig$.pipe(takeUntil(this.onDestroy$)).subscribe(newConfig => (this.filtersConfig = newConfig));
  }

  trackByFn(_, item) {
    return item.label;
  }

  onRemoveLabel(labelObj: SmartFilters.FilterLabel) {
    let prevented = false;

    const event = {
      labelObj,
      preventDefault() {
        prevented = true;
      },
    };

    this.removeLabel.emit(event);

    if (!prevented) {
      this.smartFiltersService.removeLabel(labelObj);
    }
  }
}
