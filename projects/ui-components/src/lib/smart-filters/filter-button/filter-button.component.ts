import { Component } from '@angular/core';

@Component({
  selector: 'cm-filter-button',
  template: `
    <button class="btn btn-primary">
      <i class="fa fa-filter"></i> Filter
    </button>
  `,
  styles: [
    `
      .btn-primary {
        width: 94px;
        border-radius: 20px;
        margin-left: 6px;
        padding: 0;
      }

      .fa-filter {
        margin-right: 8px;
      }
    `,
  ],
})
export class FilterButtonComponent {}
