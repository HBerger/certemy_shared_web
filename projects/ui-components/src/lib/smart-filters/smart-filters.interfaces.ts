import * as Filters from './filters/index';

export interface FilterPayload<T> {
  name: string;
  value: T;
}

export interface FilterLabel {
  filterName: string;
  type: Filters.FilterTypes;
  label: string;
  title?: string;
  optionId?: string;
}

export interface ChangeEvent<T> {
  value: T;
  preventDefault(): void;
}

export interface RemoveLabelEvent {
  labelObj: FilterLabel;
  preventDefault(): void;
}

export type FiltersConfig = Filters.Filter[];

export interface FiltersValue {
  [filterName: string]: Filters.FilterValue;
}
