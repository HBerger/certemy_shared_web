import { Injectable, OnDestroy } from '@angular/core';
import * as SmartFilters from './smart-filters.interfaces';
import * as Filters from './filters';
import { BehaviorSubject, merge, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Injectable()
export class SmartFiltersService implements OnDestroy {
  config$ = new BehaviorSubject<SmartFilters.FiltersConfig>([]);
  value$ = new BehaviorSubject<SmartFilters.FiltersValue>({});
  updateValue$ = new Subject();

  private readonly onDestroy$ = new Subject<void>();

  get configMap() {
    return this.config$.value.reduce((configMap, filter) => {
      configMap[filter.name] = filter;
      return configMap;
    }, {});
  }

  initialize(filtersConfig: SmartFilters.FiltersConfig) {
    this.config$.next(filtersConfig);
  }

  constructor() {
    merge(this.config$.pipe(map(config => this.getDefaultValues(config))), this.updateValue$)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(this.value$);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  getDefaultValues(config: SmartFilters.FiltersConfig): { [filterName: string]: Filters.FilterValue } {
    return config.reduce((filtersValue, filter) => {
      filtersValue[filter.name] = filter.getDefaultValue();
      return filtersValue;
    }, {});
  }

  filtersChange({ name, value }: SmartFilters.FilterPayload<Filters.FilterValue>) {
    this.updateValue$.next({
      ...this.value$.getValue(),
      [name]: value,
    });
  }

  removeLabel(labelObj: SmartFilters.FilterLabel) {
    const { filterName } = labelObj;
    const filtersValue = this.value$.getValue();
    const value = this.configMap[filterName].labelToValue(labelObj, filtersValue[filterName]);

    this.filtersChange({
      name: filterName,
      value,
    });
  }

  resetFilters() {
    this.updateValue$.next(this.getDefaultValues(this.config$.getValue()));
  }
}
