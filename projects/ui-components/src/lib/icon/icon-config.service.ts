import { Injectable } from '@angular/core';

@Injectable()
export class IconConfigService {
  path = '/assets/images/icons';
}
