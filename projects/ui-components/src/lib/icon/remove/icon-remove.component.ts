import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'cm-icon-remove',
  templateUrl: './icon-remove.component.html',
  styleUrls: ['./icon-remove.component.scss'],
})
export class IconRemove {}
