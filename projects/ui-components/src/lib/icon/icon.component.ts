import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IconConfigService } from './icon-config.service';

@Component({
  selector: 'cm-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  @Input() name: string;
  path: string;

  constructor(private iconService: IconConfigService) {
    this.path = iconService.path;
  }
}
