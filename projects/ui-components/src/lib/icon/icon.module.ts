import { ModuleWithProviders, NgModule } from '@angular/core';

import { IconComponent } from './icon.component';
import { IconConfigService } from './icon-config.service';
import { CommonModule } from '@angular/common';
import { IconRemove } from './remove/icon-remove.component';

@NgModule({
  imports: [CommonModule],
  declarations: [IconComponent, IconRemove],
  providers: [IconConfigService],
  exports: [IconComponent, IconRemove],
})
export class IconModule {
  static forRoot(iconConfig: IconConfigService): ModuleWithProviders<IconModule> {
    return {
      ngModule: IconModule,
      providers: [{ provide: IconConfigService, useValue: iconConfig }],
    };
  }
}
