import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation,
  OnInit,
  EventEmitter,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';
import { QuillEditorComponent } from "ngx-quill";

const FONTS = ['arial', 'comic', 'courier', 'georgia', 'helvetica', 'lucida', 'timesnewroman', 'trebuchet'];

@Component({
  selector: 'cm-html-editor',
  templateUrl: './html-editor.component.html',
  styleUrls: ['./html-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HtmlEditorComponent),
      multi: true,
    },
  ],
  encapsulation: ViewEncapsulation.None,
})
export class HtmlEditorComponent implements OnInit, ControlValueAccessor {
  onTouched = noop;
  value = '';
  options = { modules: { toolbar: [] } };
  customOptions = [
    {
      import: 'formats/font',
      whitelist: FONTS,
    },
  ];

  @Input() disabled: boolean;
  @Input()
  toolbarOptions = {
    withAlignment: false,
    withList: true,
    withFont: false,
    withSize: false,
  };
  @Input() boundContainer: ElementRef | string;
  @Output() editorCreated = new EventEmitter<void>();
  @ViewChild('editor') editorContainer: QuillEditorComponent;
  private editor;
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    const normalSizeValue = false;

    this.options.modules.toolbar = [
      this.toolbarOptions.withFont && [{ font: FONTS }],
      this.toolbarOptions.withSize && [{ size: ['small', normalSizeValue, 'large', 'huge'] }],
      ['bold', 'italic'],
      this.toolbarOptions.withList && [{ list: 'ordered' }, { list: 'bullet' }],
      ['link'],
      this.toolbarOptions.withAlignment && [{ align: '' }, { align: 'center' }, { align: 'right' }],
    ].filter((value) => value);
  }

  handleEditorCreated() {
    this.editor = this.editorContainer.quillEditor;
    this.writeValue(this.value);
    this.editorCreated.emit();
    if (this.value) {
      setTimeout(() => {
        this.editor.clipboard.dangerouslyPasteHTML(this.value);
      });
    }
  }

  onValueChange(quillEditor) {
    this.propagateChange(quillEditor.html || '');
  }

  writeValue(value = '') {
    this.value = value;

    if (this.editor) {
      this.editor.clipboard.dangerouslyPasteHTML(this.value);
      this.editor.blur();
    }
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }
}
