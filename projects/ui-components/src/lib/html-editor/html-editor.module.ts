import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HtmlEditorComponent } from './html-editor.component';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [CommonModule, QuillModule],
  declarations: [HtmlEditorComponent],
  exports: [HtmlEditorComponent],
})
export class HtmlEditorModule {}
