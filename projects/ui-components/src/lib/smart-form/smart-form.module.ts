import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbDatepickerModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedCommonModule } from '@certemy/common';
import { SmartFormComponent } from './smart-form.component';
import { TypeaheadModule } from '../form-controls/typeahead/typeahead.module';
import { SelectModule } from '../form-controls/select/select.module';
import { RadioGroupModule } from '../form-controls/radio-group/index';
import { CheckboxModule } from '../form-controls/checkbox/checkbox.module';
import { CheckboxGroupModule } from '../form-controls/checkbox-group/index';
import { DatePickerModule } from '../form-controls/datepicker/index';
import { DatePickerMonthDateModule } from '../form-controls/datepicker/datepicker-month-date/index';
import { LocationModule } from '../form-controls/location/index';

//  Public static property 'providers' of exported class has or is using name 'LocationService' from external module <...> but cannot be named
import { LocationService } from '../form-controls/location/index';

const components = [SmartFormComponent];

const modules = [
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  NgbDatepickerModule,
  NgbTypeaheadModule,
  SharedCommonModule,
  SelectModule,
  RadioGroupModule,
  CheckboxModule,
  CheckboxGroupModule,
  DatePickerModule,
  DatePickerMonthDateModule,
  TypeaheadModule,
  LocationModule,
];

@NgModule({
  imports: modules,
  declarations: components,
  exports: components,
})
export class SmartFormModule {
  static providers = [...LocationModule.providers];

  static forRoot(): ModuleWithProviders<SmartFormModule> {
    return {
      ngModule: SmartFormModule,
      providers: [...LocationModule.providers],
    };
  }
}
