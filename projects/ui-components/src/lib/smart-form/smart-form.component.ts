import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { required, email, phone } from '@certemy/common';
import { Observable } from 'rxjs';
import { Location } from '../form-controls/location/location.component';

@Component({
  selector: 'cm-smart-form',
  templateUrl: './smart-form.component.html',
  styleUrls: ['./smart-form.component.scss'],
})
export class SmartFormComponent implements OnInit {
  @Input()
  public set fields(fields: SmartForm.Field[]) {
    this._fields = fields;
    this.generateForm();
    this.showErrors = false;
  }
  public get fields() {
    return this._fields;
  }
  @Input()
  set showErrors(showErrors) {
    this._showErrors = showErrors;
  }
  get showErrors() {
    return this._showErrors;
  }
  @Output() register = new EventEmitter();
  public form: UntypedFormGroup;
  public readonly FIELD_TYPES = SmartForm.FieldTypes;
  public readonly formErrors = SmartForm.FormErrors;
  private _fields: SmartForm.Field[];
  private _showErrors = false;

  constructor(private fb: UntypedFormBuilder) {
    this.showError = this.showError.bind(this);
  }

  ngOnInit() {
    this.register.emit(this);
  }

  public generateFieldId(field: SmartForm.Field) {
    return `form_${field.name}`;
  }

  public showError(errors, showErrors: boolean): boolean {
    return errors && showErrors;
  }

  public getFormValue({ withDisabled } = { withDisabled: false }) {
    this.showErrors = false;

    if (this.form.invalid) {
      this.showErrors = true;
      return null;
    } else {
      return withDisabled ? this.form.getRawValue() : this.form.value;
    }
  }

  private generateForm() {
    const formFields = this.fields.reduce((form, field: SmartForm.Field) => {
      form[field.name] = this.createField(field);

      return form;
    }, {});
    this.form = this.fb.group(formFields);
  }

  private createField(field: SmartForm.Field) {
    const value = field.value || '';
    let validators = [];

    if (field.validators && field.validators.length) {
      validators = field.validators;
    }

    return [
      {
        value,
        disabled: field.disabled,
      },
      Validators.compose(validators),
    ];
  }
}

export namespace SmartForm {
  export enum FieldTypes {
    text,
    textarea,
    number,
    email,
    date,
    select,
    radioGroup,
    checkboxGroup,
    checkbox,
    typeahead,
    location,
  }

  export enum FormErrors {
    required = 'Please provide valid value.',
    email = 'Please provide valid email.',
    positiveNumber = 'Please provide positive number.',
    phone = 'Please provide at least 7 symbols.',
  }

  export type Field =
    | TextField
    | TextareaField
    | DateField
    | NumberField
    | EmailField
    | TypeaheadField
    | SelectField
    | RadioGroupField
    | CheckboxGroupField
    | CheckboxField
    | PhoneField
    | LocationField;

  export interface FieldOption {
    name: string;
    id: string | number;
    tooltip?: string;
    disabled?: boolean;
    afterTpl?: TemplateRef<any> | string;
    customClass?: string;
    useHtml?: boolean;
    htmlMarkup?: string;
  }

  export class BaseFormField<T> {
    public type = FieldTypes.text;
    public name: string;
    public label: string;
    public value: T;
    public required: boolean;
    public disabled: boolean;
    public validators: ValidatorFn[];

    constructor(options) {
      this.name = options.name;
      this.label = options.label;
      this.value = options.value || '';
      this.required = options.required || false;
      this.disabled = options.disabled || false;
      this.validators = options.validators || [];

      const requiredValidator = this.required ? [required] : [];
      this.validators = [...this.validators, ...requiredValidator];
    }
  }

  export interface IBaseFormFieldOptions<T> {
    name: string;
    label: string;
    value: T;
    required?: boolean;
    disabled?: boolean;
    validators?: ValidatorFn[];
  }

  export class BaseOptionsFormField<T> extends BaseFormField<T> {
    public type = FieldTypes.select;
    public options: FieldOption[] = [];

    constructor(options: IBaseOptionsFormFieldOptions<T>) {
      super(options);
      this.options = options.options;
    }
  }

  export interface IBaseOptionsFormFieldOptions<T> extends IBaseFormFieldOptions<T> {
    options: FieldOption[];
  }

  export class TextField extends BaseFormField<string> {
    constructor(options: IBaseFormFieldOptions<string>) {
      super(options);
    }
  }

  export class TextareaField extends BaseFormField<string> {
    public type = FieldTypes.textarea;

    constructor(options: IBaseFormFieldOptions<string>) {
      super(options);
    }
  }

  export class DateField extends BaseFormField<string | Date> {
    public type = FieldTypes.date;

    constructor(options: IBaseFormFieldOptions<string | Date>) {
      super(options);
    }
  }

  export class NumberField extends BaseFormField<number> {
    public type = FieldTypes.number;

    constructor(options: IBaseFormFieldOptions<number>) {
      super(options);
    }
  }

  export class EmailField extends BaseFormField<string> {
    public type = FieldTypes.email;

    constructor(options: IBaseFormFieldOptions<string>) {
      super(options);
      this.validators = [...this.validators, email];
    }
  }

  export class TypeaheadField extends BaseFormField<any> {
    public type = FieldTypes.typeahead;
    public request$: (term: string) => Observable<any>;
    public formatter: (result: any) => string;

    constructor(options: ITypeaheadFieldOptions) {
      super(options);
      this.request$ = options.request$;
      this.formatter = options.formatter;
    }
  }

  export interface ITypeaheadFieldOptions extends IBaseFormFieldOptions<any> {
    request$: (term: string) => Observable<any>;
    formatter: (result: any) => string;
  }

  export interface ILocationFieldOptions {
    name?: string;
    value?: Location.Value;
    disabled?: boolean;
    validators?: ValidatorFn[];
  }

  export class SelectField extends BaseOptionsFormField<string | number> {}

  export class RadioGroupField extends BaseOptionsFormField<string | number> {
    public type = FieldTypes.radioGroup;
  }

  export class CheckboxGroupField extends BaseOptionsFormField<{ [key: string]: boolean }> {
    public type = FieldTypes.checkboxGroup;
  }

  export class CheckboxField extends BaseFormField<boolean> {
    public type = FieldTypes.checkbox;
  }

  export class PhoneField extends BaseFormField<string> {
    public type = FieldTypes.text;

    constructor(options: IBaseFormFieldOptions<string>) {
      super(options);
      this.validators = [...this.validators, phone];
    }
  }

  export class LocationField {
    public name: string;
    public type = FieldTypes.location;
    public value: Location.Value;
    public disabled: boolean;
    public validators: ValidatorFn[];

    constructor(options: ILocationFieldOptions) {
      this.name = options.name || 'location';
      this.value = options.value || { country: '', state: '', city: '' };
      this.disabled = options.disabled || false;
      this.validators = options.validators || [];
    }
  }
}
