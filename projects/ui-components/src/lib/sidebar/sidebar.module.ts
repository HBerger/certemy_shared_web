import { ModuleWithProviders, NgModule } from '@angular/core';
import { SidebarService } from './sidebar.service';
import { SidebarComponent } from './sidebar.component';
import { SharedCommonModule } from '@certemy/common';

@NgModule({
  imports: [SharedCommonModule],
  declarations: [SidebarComponent],
  exports: [SidebarComponent],
})
export class SidebarModule {
  static providers = [SidebarService];
  static forRoot(): ModuleWithProviders<SidebarModule> {
    return {
      ngModule: SidebarModule,
      providers: [SidebarService],
    };
  }
}
