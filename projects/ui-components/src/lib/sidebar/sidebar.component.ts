import { Component, OnInit, TemplateRef } from '@angular/core';
import { SidebarService, SidebarToggleData } from './sidebar.service';
import _isNumber from 'lodash-es/isNumber';

if (!Element.prototype.matches) {
  Element.prototype.matches = (Element.prototype as any).msMatchesSelector;
}

@Component({
  selector: 'cm-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  contentTemplateRef: TemplateRef<any>;
  timeoutId;
  showSidebar = false;
  styles: any;

  private readonly defaultStyles = {
    wrapper: {},
    aside: {},
  };

  constructor(private sidebarService: SidebarService) {}

  ngOnInit() {
    this.styles = { ...this.defaultStyles };
    this.sidebarService.toggle$.subscribe((data: SidebarToggleData) => {
      this.styles = {
        ...this.defaultStyles,
      };
      if (data && data.templateRef) {
        const { templateRef, config } = data;
        if (config && config.width) {
          const width = _isNumber(config.width) ? `${config.width}px` : config.width;
          Object.assign(this.styles, {
            wrapper: {
              transform: `translate(-${width})`,
            },
            aside: {
              right: `-${width}`,
              width: `${width}`,
            },
          });
        }
        this.showSidebar = true;
        this.contentTemplateRef = templateRef;
      } else {
        this.showSidebar = false;
        if (this.timeoutId) {
          clearTimeout(this.timeoutId);
        }

        this.timeoutId = setTimeout(() => {
          this.contentTemplateRef = null;
        }, 400);
      }
    });
  }

  onOverlayClick() {
    this.sidebarService.close();
  }
}
