import { Injectable, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable()
export class SidebarService {
  toggle$ = new Subject<SidebarToggleData>();
  private onClose$ = new Subject<void>();

  open(content: TemplateRef<any>, config?: any) {
    return new Observable<void>(observer => {
      this.toggle$.next({ templateRef: content, config });
      const onCloseSub = this.onClose$.subscribe(() => observer.complete());
      return () => {
        this.toggle$.next(null);
        onCloseSub.unsubscribe();
      };
    });
  }

  close() {
    this.onClose$.next();
  }
}

export interface SidebarToggleData {
  templateRef: TemplateRef<any>;
  config?: any;
}
