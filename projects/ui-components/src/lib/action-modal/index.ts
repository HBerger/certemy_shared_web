import {
  ActionModalConfig as ActionModalConfigInterface,
  BtnConfig as BtnConfigInterface,
  BtnAction as BtnActionInterface,
} from './action-modal.component';
export * from './action-modal.component';
export * from './action-modal.module';
export type ActionModalConfig = ActionModalConfigInterface;
export type BtnConfig = BtnConfigInterface;
export type BtnAction = BtnActionInterface;
