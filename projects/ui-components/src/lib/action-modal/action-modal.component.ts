import { Component, TemplateRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

export interface BtnConfig {
  text: string;
  action: BtnAction;
}

export interface ActionModalConfig {
  header: string;
  body: string;
  primaryBtn?: BtnConfig;
  secondaryBtn?: BtnConfig;
}

export type BtnAction = any;

@Component({
  selector: 'cm-action-modal',
  templateUrl: './action-modal.component.html',
  styleUrls: ['./action-modal.component.scss'],
})
export class ActionModalComponent {
  config: ActionModalConfig;
  @ViewChild('content') content: TemplateRef<any>;
  modalRef: NgbModalRef;
  modalAction$: Subject<BtnAction>;

  constructor(private modalService: NgbModal) {}

  open(config: ActionModalConfig): Observable<BtnAction> {
    this.config = config;
    this.modalRef = this.modalService.open(this.content);
    this.modalAction$ = new Subject();
    return this.modalAction$.asObservable();
  }

  onAction(action: BtnAction): void {
    this.modalRef.close();
    this.modalAction$.next(action);
    this.modalAction$.complete();
  }
}
