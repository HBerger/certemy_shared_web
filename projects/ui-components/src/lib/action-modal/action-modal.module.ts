import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedCommonModule } from '@certemy/common';
import { ActionModalComponent } from './action-modal.component';

@NgModule({
  imports: [CommonModule, SharedCommonModule],
  declarations: [ActionModalComponent],
  exports: [ActionModalComponent],
})
export class ActionModalModule {}
