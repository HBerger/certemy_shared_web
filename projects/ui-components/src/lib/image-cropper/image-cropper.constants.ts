export enum CROPPER_MODES {
  CIRCLE = 1,
  SQUARE = 2,
  RECTANGLE = 3,
}

export const ASPECT_RATIO = {
  RECTANGLE: 20 / 11, // Ratio is based on header parameters
  DEFAULT: 1,
};
