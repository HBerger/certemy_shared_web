export interface CanvasData {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface CropBoxData {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface ImageCropperDefaultConfiguration {
  originalFile?: string;
  configuration?: CanvasData;
  mode?: number;
  cropBoxConfiguration?: CropBoxData;
}

export interface ImageCropperEvent {
  blob: Blob;
  configuration: CanvasData;
  cropBoxConfiguration?: CropBoxData;
  mode?: number;
}
