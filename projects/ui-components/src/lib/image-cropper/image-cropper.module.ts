import { NgModule } from '@angular/core';
import { ImageCropperComponent } from './image-cropper.component';
import { AngularCropperjsModule } from 'angular-cropperjs';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [AngularCropperjsModule, CommonModule],
  exports: [ImageCropperComponent],
  declarations: [ImageCropperComponent],
})
export class CertemyImageCropperModule {}
