import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { CropperComponent } from 'angular-cropperjs';
import { CropBoxData, ImageCropperDefaultConfiguration, ImageCropperEvent } from './image-cropper.interface';
import { ComputedFrom } from '@certemy/common';
import { ASPECT_RATIO, CROPPER_MODES } from './image-cropper.constants';
import _get from 'lodash-es/get';

@Component({
  selector: 'cm-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageCropperComponent {
  @Input() options?: any = {};
  @Input() allowSwitchingMode = false;
  @Input() availableModes: CROPPER_MODES[] = [CROPPER_MODES.SQUARE, CROPPER_MODES.CIRCLE];

  @Input()
  set defaultConfiguration(config: ImageCropperDefaultConfiguration) {
    if (!config) {
      return;
    }
    this.imageUrl = config.originalFile;
    this.defaultCanvasData = config.configuration;
    this.defaultCropBoxData = config.cropBoxConfiguration;
    this.mode = config.mode;
  }

  @Output() imageCropped = new EventEmitter<ImageCropperEvent>();
  @Output() imageLoaded = new EventEmitter<undefined>();
  zoomStep = 0.1;
  defaultCanvasData;
  MODES = CROPPER_MODES;
  imageLoading = true;
  defaultCropBoxData: CropBoxData;
  private mode = CROPPER_MODES.CIRCLE;

  @Input()
  set imageFile(file: File) {
    if (!file) {
      return;
    }

    this.imageUrl = null;
    this.imageCropped.emit(null);

    this._getBase64(file).then((data: string) => {
      this.imageUrl = data;
      this.changeDetector.markForCheck();
    });
  }

  @ViewChild('angularCropper') angularCropper: CropperComponent;
  imageUrl: string = '';

  cropperOptions = {
    viewMode: 0,
    dragMode: 'move',
    zoomable: true,
    movable: true,
    guides: false,
    aspectRatio: ASPECT_RATIO.DEFAULT,
    autoCropArea: 0.65,
    restore: false,
    center: false,
    highlight: false,
    cropBoxMovable: false,
    cropBoxResizable: false,
    checkOrientation: false,
    toggleDragModeOnDblclick: false,
    crop: this.onImageCropped.bind(this),
    ready: this.onImageLoaded.bind(this),
    ...this.options,
  };

  @ComputedFrom('mode')
  get isSquareMode(): boolean {
    return [CROPPER_MODES.SQUARE, CROPPER_MODES.RECTANGLE].includes(this.mode as number);
  }

  // Firefox adds cropper property to image object
  // if checkCrossOrigin option is true (angular-cropperjs.js: line 67),
  // and as a result constructor new Cropper works incorrectly
  // cropper.js: line 3189 (_createClass method)
  get isFirefox(): boolean {
    return navigator.userAgent.includes('Firefox');
  }

  constructor(private changeDetector: ChangeDetectorRef) {}

  private _getBase64(file: File) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  switchMode(mode: CROPPER_MODES) {
    this.mode = mode;
    this.setAspectRatio(this.mode);
    this.onImageCropped();
  }

  zoomChange(value) {
    this.angularCropper.cropper.zoom(value);
  }

  onImageCropped() {
    if (!this.angularCropper.cropper || !this.angularCropper.cropper.getCroppedCanvas()) {
      return;
    }

    // While cropping it is necessary to set image type in order
    // to avoid image size enlargement: https://github.com/fengyuanchen/cropperjs#known-issues
    // https://github.com/fengyuanchen/cropperjs/issues/562

    this.angularCropper.cropper
      .getCroppedCanvas({
        width: 500,
        height: 500,
      })
      .toBlob((blob: Blob) => {
        const event: ImageCropperEvent = {
          blob,
          configuration: this.angularCropper.cropper.getCanvasData(),
          cropBoxConfiguration: this.angularCropper.cropper.getCropBoxData(),
          mode: this.mode,
        };

        this.imageCropped.emit(event);
      }, this.getImageType(this.imageUrl));
  }

  onImageLoaded() {
    this.imageLoaded.emit();
    this.imageLoading = false;

    if (this.defaultCanvasData) {
      this.setAspectRatio(this.mode);
      this.angularCropper.cropper.setCanvasData(this.defaultCanvasData);
      this.angularCropper.cropper.setCropBoxData(this.defaultCropBoxData);
      this.defaultCropBoxData = null;
      this.defaultCanvasData = null;
    }
  }

  isModeAvailable(mode: CROPPER_MODES): boolean {
    return this.availableModes.includes(mode);
  }

  isModeActive(mode: CROPPER_MODES): boolean {
    return this.mode === mode;
  }

  private setAspectRatio(mode: CROPPER_MODES) {
    if (!_get(this.angularCropper, 'cropper')) {
      return;
    }

    if (mode === CROPPER_MODES.RECTANGLE) {
      this.options = {
        ...this.options,
        aspectRatio: ASPECT_RATIO.RECTANGLE,
      };
      this.angularCropper.cropper.setAspectRatio(ASPECT_RATIO.RECTANGLE);
    } else {
      this.options = {
        ...this.options,
        aspectRatio: ASPECT_RATIO.DEFAULT,
      };
      this.angularCropper.cropper.setAspectRatio(1);
    }
  }

  private getImageType(imageUrl: string) {
    const getImageTypeFromBase64Url = imageUrl.slice(imageUrl.indexOf(':') + 1, imageUrl.indexOf(';'));
    const getImageTypeFromUrl = imageUrl.slice(imageUrl.lastIndexOf('.') + 1, imageUrl.length);
    const isBase64Url = imageUrl.includes('base64') && imageUrl.includes('data:');
    return isBase64Url ? getImageTypeFromBase64Url : `image/${getImageTypeFromUrl}`;
  }
}
