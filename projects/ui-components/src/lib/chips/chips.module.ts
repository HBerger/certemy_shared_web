import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChipsComponent } from './chips.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, NgbTooltipModule],
  declarations: [ChipsComponent],
  exports: [ChipsComponent],
})
export class ChipsModule {}
