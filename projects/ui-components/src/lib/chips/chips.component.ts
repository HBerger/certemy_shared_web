import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'cm-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipsComponent {
  @Input() disabled: boolean;
  @Input() options: any[];
  @Output() removeLabel = new EventEmitter();

  trackByFn(value, option) {
    return option.name;
  }

  remove(option) {
    this.removeLabel.emit(option);
  }
}
