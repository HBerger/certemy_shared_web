import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'cm-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ColorPickerComponent {
  @Input() color: string;
  @Input() presetColors?: string[];
  @Input() placement = 'bottom left right top';
  @Input() disableAlpha = true;
  @Output() colorChanged = new EventEmitter<string>();
  @Output() colorPickerClosed = new EventEmitter<string>();
  @Output() colorPickerOpened = new EventEmitter<boolean>();

  changeComplete(event) {
    const hex = event.color.hex;

    this.colorChanged.emit(hex);
  }

  onColorPickerClosed() {
    this.colorPickerClosed.emit(this.color);
    this.colorPickerOpened.emit(false);
  }

  onColorPickerOpened() {
    this.colorPickerOpened.emit(true);
  }
}
