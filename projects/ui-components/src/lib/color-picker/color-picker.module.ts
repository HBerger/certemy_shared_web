import { NgModule } from '@angular/core';
import { ColorPickerComponent } from './color-picker.component';
import { ColorSketchModule } from 'ngx-color/sketch';
import { CommonModule } from '@angular/common';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, ColorSketchModule, NgbTooltipModule],
  exports: [ColorPickerComponent],
  declarations: [ColorPickerComponent],
})
export class ColorPickerModule {}
