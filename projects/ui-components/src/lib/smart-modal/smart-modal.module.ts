import { ModuleWithProviders, NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SmartModalService } from './smart-modal.service';
import { SmartModalComponent } from './smart-modal/smart-modal.component';

@NgModule({
    imports: [SharedCommonModule, NgbModalModule],
    declarations: [SmartModalComponent]
})
export class SmartModalModule {
  // TODO: Should be tested more
  static forRoot(): ModuleWithProviders<SmartModalModule> {
    return {
      ngModule: SmartModalModule,
      providers: [SmartModalService],
    };
  }
}
