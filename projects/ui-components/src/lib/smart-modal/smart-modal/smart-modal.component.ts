import { Component, Input, ViewEncapsulation, TemplateRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

export interface SmartModalConfig {
  title?: string;
  body?: string | TemplateRef<null>;
  rejectButtonText?: string;
  successButtonText?: string;
}

export interface ConfirmationModalConfig extends SmartModalConfig {
  title: string;
  body: string | TemplateRef<null>;
}

export interface NotificationModalConfig {
  title: string;
  body: string | TemplateRef<null>;
  closeButtonText?: string;
}

@Component({
  selector: 'cm-smart-modal',
  templateUrl: './smart-modal.component.html',
  styleUrls: ['./smart-modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SmartModalComponent {
  @Input() config: SmartModalConfig;

  constructor(private activeModal: NgbActiveModal) {}

  onSuccessClick() {
    this.activeModal.close();
  }

  onRejectClick() {
    this.activeModal.dismiss();
  }

  isTemplateRef(something): boolean {
    return something instanceof TemplateRef;
  }
}
