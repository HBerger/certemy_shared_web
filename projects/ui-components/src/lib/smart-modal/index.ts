import {
  SmartModalConfig as SmartModalConfigInterface,
  NotificationModalConfig as NotificationModalConfigInterface,
  ConfirmationModalConfig as ConfirmationModalConfigInterface,
} from './smart-modal/smart-modal.component';
export { SmartModalModule } from './smart-modal.module';
export {
  SmartModalService,
  CloseType as SmartModalCloseType,
  skipClose as skipModalClose,
} from './smart-modal.service';
export { SmartModalComponent } from './smart-modal/smart-modal.component';
export type SmartModalConfig = SmartModalConfigInterface;
export type NotificationModalConfig = NotificationModalConfigInterface;
export type ConfirmationModalConfig = ConfirmationModalConfigInterface;
