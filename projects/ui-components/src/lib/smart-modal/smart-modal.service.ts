import { Injectable } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { filter, map, mapTo } from 'rxjs/operators';
import {
  SmartModalComponent,
  SmartModalConfig,
  NotificationModalConfig,
  ConfirmationModalConfig,
} from './smart-modal/smart-modal.component';

export enum CloseType {
  SUCCESS,
  REJECT,
}

export function skipClose() {
  return filter((action: { type: CloseType; payload: any }) => action.type === CloseType.SUCCESS);
}

@Injectable()
export class SmartModalService {
  readonly CloseType = CloseType;

  constructor(private modalService: NgbModal) {}

  open(component: any, inputs: Object, options: NgbModalOptions = {}): Observable<{ type: CloseType; payload: any }> {
    return Observable.create(observer => {
      const defaultOptions: NgbModalOptions = { backdrop: 'static' };
      const modalRef = this.modalService.open(component, { ...defaultOptions, ...options });
      Object.assign(modalRef.componentInstance, inputs);

      modalRef.result.then(
        payload => observer.next({ type: CloseType.SUCCESS, payload }),
        payload => observer.next({ type: CloseType.REJECT, payload }),
      );
      return () => modalRef.close();
    });
  }

  openConfirm(config: ConfirmationModalConfig, options: NgbModalOptions = {}): Observable<boolean> {
    const defaultConfig: SmartModalConfig = {
      rejectButtonText: 'Cancel',
      successButtonText: 'Proceed',
    };
    return this.open(SmartModalComponent, { config: { ...defaultConfig, ...config } }, options).pipe(
      map(({ type }) => type === CloseType.SUCCESS),
    );
  }

  openUnsavedChanges() {
    const config = {
      title: 'Unsaved changes',
      body: 'Do you really want to proceed without saving?',
    };
    return this.openConfirm(config);
  }

  openNotification(config: NotificationModalConfig, options: NgbModalOptions = {}) {
    const smartModalConfig: SmartModalConfig = {
      title: config.title,
      body: config.body,
      successButtonText: config.closeButtonText || 'Close',
    };
    return this.open(SmartModalComponent, { config: smartModalConfig }, options).pipe(mapTo(null));
  }
}
