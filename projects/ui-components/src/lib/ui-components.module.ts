import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedCommonModule } from '@certemy/common';

import { PageActionsModule } from './page-actions/page-actions.module';
import { PageHeaderModule } from './page-header/page-header.module';
import { SmartTableModule } from './smart-table/smart-table.module';
import { NavTabsModule } from './nav-tabs/nav-tabs.module';
import { ActionModalModule } from './action-modal/action-modal.module';
import { FileUploadModule } from './file-upload/file-upload.module';
import { SmartFormModule } from './smart-form/smart-form.module';
import { TableTabsModule } from './table-tabs/table-tabs.module';
import { SelectModule } from './form-controls/select/select.module';
import { CheckboxModule } from './form-controls/checkbox/checkbox.module';
import { RadioGroupModule } from './form-controls/radio-group/radio-group.module';
import { RadioModule } from './form-controls/radio/radio.module';
import { CheckboxGroupModule } from './form-controls/checkbox-group/checkbox-group.module';
import { DatePickerModule } from './form-controls/datepicker/datepicker.module';
import { DatePickerMonthDateModule } from './form-controls/datepicker/datepicker-month-date/datepicker-month-date.module';
import { TypeaheadModule } from './form-controls/typeahead/typeahead.module';
import { TextInputModule } from './form-controls/text-input/text-input.module';
import { NumberInputModule } from './form-controls/number-input/number-input.module';
import { TextareaModule } from './form-controls/textarea/textarea.module';
import { CardModule } from './card/card.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { SmartFiltersModule } from './smart-filters/smart-filters.module';
import { LocationModule } from './form-controls/location/location.module';
import { RangeModule } from './form-controls/range/range.module';
import { PortalModule } from './portal/portal.module';
import { IconModule } from './icon/icon.module';
import { PaginationModule } from './pagination/pagination.module';
import { PopoverLayoutModule } from './popover-layout/popover-layout.module';
import { PreloaderModule } from './preloader/preloader.module';
import { BreadcrumbsModule } from './breadcrumbs/breadcrumbs.module';
import { SplitButtonModule } from './split-button';
import { CertemyImageCropperModule } from './image-cropper';
import { ColorPickerModule } from './color-picker';
import { SpinnerModule } from './spinner';
import { ChipsModule } from './chips';
import { ChipsInputModule } from './form-controls/chips-input';
import { ActionTabsModule } from './action-tabs';
import { OptionDropdownModule } from './option-dropdown';
import { ToggleModule } from './toggle';
import { SearchInputModule } from './form-controls/search-input';
import { TimePickerModule } from './form-controls/timepicker';
import { SwitchModule } from './form-controls/switch';
import { FileDownloadDropdownModule } from './file-download-dropdown';
import { CustomControlModule } from './form-controls/custom-control';
import { NgxDnDModule } from './ngx-dnd/ngx-dnd.module';

const modules = [
  SmartTableModule,
  PageActionsModule,
  PageHeaderModule,
  ActionModalModule,
  FileUploadModule,
  SmartFormModule,
  SelectModule,
  CheckboxModule,
  CheckboxGroupModule,
  RadioModule,
  RadioGroupModule,
  DatePickerModule,
  DatePickerMonthDateModule,
  TypeaheadModule,
  TextInputModule,
  NumberInputModule,
  TextareaModule,
  CardModule,
  LocationModule,
  SmartFormModule,
  NavTabsModule,
  SidebarModule,
  SmartFiltersModule,
  RangeModule,
  PortalModule,
  IconModule,
  PaginationModule,
  PopoverLayoutModule,
  PreloaderModule,
  TableTabsModule,
  BreadcrumbsModule,
  SplitButtonModule,
  CertemyImageCropperModule,
  ColorPickerModule,
  SpinnerModule,
  ChipsModule,
  ChipsInputModule,
  ActionTabsModule,
  OptionDropdownModule,
  ToggleModule,
  SearchInputModule,
  TimePickerModule,
  SwitchModule,
  FileDownloadDropdownModule,
  CustomControlModule,
  NgxDnDModule,
];

@NgModule({
  imports: [...modules, CommonModule, NgbModule, SharedCommonModule, RouterModule],
  declarations: [],
  exports: [...modules],
})
export class UiComponentsModule {
  static forRoot(): ModuleWithProviders<UiComponentsModule> {
    return {
      ngModule: UiComponentsModule,
      providers: [...LocationModule.providers, ...PortalModule.providers, ...SidebarModule.providers],
    };
  }
}
