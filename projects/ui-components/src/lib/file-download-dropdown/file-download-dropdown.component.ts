import { Component, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { FileLink } from './file-download-dropdown.interface';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'cm-file-download-dropdown',
  templateUrl: './file-download-dropdown.component.html',
  styleUrls: ['./file-download-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileDownloadDropdownComponent {
  @Input() fileLinks: FileLink[];
  @Input() title: string;
  @ViewChild('dropdown') dropdown: NgbDropdown;

  toggle() {
    this.dropdown.toggle();
  }
}
