export interface FileLink {
  name: string;
  link: string;
}
