import { NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { FileDownloadDropdownComponent } from './file-download-dropdown.component';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [SharedCommonModule, NgbDropdownModule],
  declarations: [FileDownloadDropdownComponent],
  exports: [FileDownloadDropdownComponent],
})
export class FileDownloadDropdownModule {}
