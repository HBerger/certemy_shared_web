import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cm-card-footer-pagination',
  templateUrl: './card-footer-pagination.component.html',
  styleUrls: ['./card-footer-pagination.component.scss'],
})
export class CardFooterPaginationComponent {
  @Input() page = 1;
  @Input() collectionSize = 0;
  @Input() maxSize = 4;
  @Input() pageSize = 10;
  @Input() pageSizeSelectorActive = false;
  @Output() pageChange = new EventEmitter<number>();
  @Output() pageSizeChange = new EventEmitter<number>();
}
