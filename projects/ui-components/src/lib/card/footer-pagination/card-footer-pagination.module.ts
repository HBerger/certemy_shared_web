import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardFooterPaginationComponent } from './card-footer-pagination.component';
import { PaginationModule } from '../../pagination/pagination.module';

@NgModule({
  imports: [CommonModule, PaginationModule],
  declarations: [CardFooterPaginationComponent],
  providers: [],
  exports: [CardFooterPaginationComponent],
})
export class CardFooterPaginationModule {}
