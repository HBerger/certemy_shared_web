import { NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { CustomDropdownComponent } from './custom-dropdown.component';

@NgModule({
  imports: [SharedCommonModule, NgbDropdownModule],
  exports: [CustomDropdownComponent],
  declarations: [CustomDropdownComponent],
})
export class CustomDropdownModule {}
