import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';
import { SmartForm } from '@certemy/ui-components';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'cm-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['./custom-dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomDropdownComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CustomDropdownComponent implements ControlValueAccessor {
  @ViewChild('dropDown') dropDown: NgbDropdown;
  @Input() placeholder: string;
  @Input() options: SmartForm.FieldOption[] = [];
  @Input() dropDownMenu: ElementRef;
  @Input() autoClose?: true | false | 'inside' | 'outside' = true;
  @Input() disabled?: boolean;
  selectedValue: string;
  onTouched = noop;
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  toggle() {
    if (!this.disabled) {
      this.dropDown.toggle();
    }
  }

  writeValue(value = '') {
    const selectedOptions = this.options.find(option => option.id === value);
    this.selectedValue = selectedOptions ? selectedOptions.name : this.placeholder;
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.disabled = isDisabled;
    this.changeDetector.markForCheck();
  }
}
