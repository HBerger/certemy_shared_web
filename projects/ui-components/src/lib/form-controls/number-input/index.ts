import { DEFAULT_MASK_CONFIG as DEFAULT_MASK_CONFIG_CONST } from './number-input.constants';
export { MASK_TYPE, NumberMaskConfig } from './number-input.interface';
export * from './number-input.module';
export * from './number-input.component';
export { applyPhoneMask } from './number-input.helpers';
export const DEFAULT_MASK_CONFIG = DEFAULT_MASK_CONFIG_CONST;
