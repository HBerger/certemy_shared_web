import { MASK_TYPE, NumberMaskConfig } from './number-input.interface';
import { DEFAULT_MASK_CONFIG, DIGIT_LIMIT, NUMBER_INPUT_REGEXP } from './number-input.constants';
import _isString from 'lodash-es/isString';
import { maskitoEventHandler, maskitoPrefixPostprocessorGenerator, maskitoWithPlaceholder } from '@maskito/kit';
import { MaskitoOptions, maskitoUpdateElement } from '@maskito/core';

export function getNumberMask(config: NumberMaskConfig = DEFAULT_MASK_CONFIG): any {
  const MASK_MAP = {
    [MASK_TYPE.DEFAULT]: {
      mask: defaultMask.bind({ digitLimit: config.digitLimit }),
    },
    [MASK_TYPE.POSITIVE_INTEGER]: {
      mask: Array.from({ length: config.digitLimit }, () => NUMBER_INPUT_REGEXP.digit),
    },
    [MASK_TYPE.MULTIPLE_OF_HALF]: {
      mask: multipleOfHalfMask.bind({ digitLimit: config.digitLimit }),
    },
    [MASK_TYPE.PERCENTAGE]: {
      mask: percentageMask,
    },
    [MASK_TYPE.FLOAT]: {
      mask: floatNumberMask,
    },
    [MASK_TYPE.PHONE_NUMBER]: phoneNumberMask(config.prefix),
    [MASK_TYPE.ONE_DECIMAL]: {
      mask: oneDecimalMask.bind({ digitLimit: config.digitLimit }),
    },
    [MASK_TYPE.FROM_ZERO_TO_ONE]: {
      mask: fromZeroToOneMask.bind({ digitLimit: config.digitLimit }),
    },
    [MASK_TYPE.LEADING_ZERO]: {
      mask: addLeadingZero.bind({ digitLimit: config.digitLimit }),
    }
  };

  return MASK_MAP[config.maskType];
}

const isZeroOnThirdPlaceAllowed = (value: string) => `${value.slice(0, 2)}` === '10';
const isDotAllowed = (index: number, char: string, dotIndex: number) =>
  index !== 0 && char.match(NUMBER_INPUT_REGEXP.dot) && !dotIndex;

export function multipleOfHalfMask({ value }: { value: string }): RegExp[] {
  const allowedDecimal = 1;
  const allowedInteger = this.digitLimit || DIGIT_LIMIT;

  let dotIndex = null;

  return value.split('').map((char, index) => {
    if (isDotAllowed(index, char, dotIndex)) {
      dotIndex = index;
      return NUMBER_INPUT_REGEXP.dot;
    }

    if (areLimitsExceeded(value, index, allowedInteger, allowedDecimal, dotIndex)) {
      return NUMBER_INPUT_REGEXP.nothing;
    }

    if (!dotIndex || (dotIndex && (parseInt(char, 10) === 5 || parseInt(char, 10) === 0))) {
      return NUMBER_INPUT_REGEXP.digit;
    }

    return NUMBER_INPUT_REGEXP.nothing;
  });
}

export function oneDecimalMask({ value }: { value: string }): RegExp[] {
  const allowedDecimal = 1;
  const allowedInteger = this.digitLimit || DIGIT_LIMIT;

  let dotIndex = null;

  return value.split('').map((char, index) => {
    if (isDotAllowed(index, char, dotIndex)) {
      dotIndex = index;
      return NUMBER_INPUT_REGEXP.dot;
    }

    if (areLimitsExceeded(value, index, allowedInteger, allowedDecimal, dotIndex)) {
      return NUMBER_INPUT_REGEXP.nothing;
    }

    return NUMBER_INPUT_REGEXP.digit;
  });
}

export function fromZeroToOneMask({ value }: { value: string }): RegExp[] {
  const MAX_VALUE = '1';
  const allowedDecimal = this.digitLimit || DIGIT_LIMIT;
  const allowedInteger = 1;

  let dotIndex = null;

  return value.split('').map((char, index) => {
    if (isDotAllowed(index, char, dotIndex) && value < MAX_VALUE) {
      dotIndex = index;
      return NUMBER_INPUT_REGEXP.dot;
    }

    if (areLimitsExceeded(value, index, allowedInteger, allowedDecimal, dotIndex)) {
      return NUMBER_INPUT_REGEXP.nothing;
    }

    if (value > MAX_VALUE && index === 0) {
      return NUMBER_INPUT_REGEXP.one;
    }

    return NUMBER_INPUT_REGEXP.digit;
  });
}

export function defaultMask({ value }: { value: string }): RegExp[] {
  const allowedDecimal = 2;
  const allowedInteger = this.digitLimit || DIGIT_LIMIT;

  let dotIndex = null;

  return value.split('').map((char, index) => {
    if (isDotAllowed(index, char, dotIndex)) {
      dotIndex = index;
      return NUMBER_INPUT_REGEXP.dot;
    }

    if (areLimitsExceeded(value, index, allowedInteger, allowedDecimal, dotIndex)) {
      return NUMBER_INPUT_REGEXP.nothing;
    }

    return NUMBER_INPUT_REGEXP.digit;
  });
}

export function percentageMask({ value }: { value: string }): RegExp[] {
  return value.split('').map((char, index) => {
    if (areLimitsExceeded(value, index) || (index === 2 && !isZeroOnThirdPlaceAllowed(value))) {
      return NUMBER_INPUT_REGEXP.nothing;
    }

    if (index === 2 && isZeroOnThirdPlaceAllowed(value)) {
      return NUMBER_INPUT_REGEXP.zero;
    }

    return NUMBER_INPUT_REGEXP.digit;
  });
}

function getDecimalParameters(
  dotIndex: number,
  value: string,
  allowedDecimal: number,
): { decimalLimitExceeded: boolean; lastAllowedDecimalIndex: number } {
  return {
    decimalLimitExceeded: dotIndex ? value.slice(dotIndex + 1).length > allowedDecimal : false,
    lastAllowedDecimalIndex: dotIndex + allowedDecimal,
  };
}

function areLimitsExceeded(
  value: string,
  index: number,
  allowedInteger = 3,
  allowedDecimal = 0,
  dotIndex = null,
): boolean {
  const { decimalLimitExceeded, lastAllowedDecimalIndex } = getDecimalParameters(dotIndex, value, allowedDecimal);
  const integerLimitExceeded = !dotIndex && index + 1 > allowedInteger;
  return allowedDecimal
    ? integerLimitExceeded || (dotIndex && decimalLimitExceeded && index > lastAllowedDecimalIndex)
    : integerLimitExceeded;
}

function floatNumberMask({ value }: { value: string }): RegExp[] {
  const dotRegexp = /\./;
  const digitRegexp = /\d/;
  let alreadyHaveDot = false;
  return value.split('').map((char, index) => {
    if (char.match(dotRegexp) && index !== 0 && !alreadyHaveDot) {
      alreadyHaveDot = true;
      return dotRegexp;
    } else {
      return digitRegexp;
    }
  });
}

function phoneNumberMask(prefix: string = '') {
  const prefixArr = prefix.split('');
  const prefixPlaceholder = (' ').repeat(prefixArr.length);
  const PLACEHOLDER = `${prefixPlaceholder} (___) ___-____`;
  const {
    removePlaceholder,
    plugins,
    ...placeholderOptions
  } = maskitoWithPlaceholder(PLACEHOLDER);

  // Mask for US & CANADA numbers
  // TODO: Change when new cases will appear
  return ({
    preprocessors: placeholderOptions.preprocessors,
    postprocessors: [
      maskitoPrefixPostprocessorGenerator(prefix),
      ...placeholderOptions.postprocessors,
    ],
    mask: [
      ...prefixArr,
      ' ',
      '(',
      /\d/,
      /\d/,
      /\d/,
      ')',
      ' ',
      /\d/,
      /\d/,
      /\d/,
      '-',
      /\d/,
      /\d/,
      /\d/,
      /\d/,
    ],
    plugins: [
      ...plugins,
      maskitoEventHandler('focus', element => {
        const initialValue = element.value || `${prefix} (`;

        maskitoUpdateElement(
          element,
          initialValue + PLACEHOLDER.slice(initialValue.length),
        );
      }),
      maskitoEventHandler('blur', element => {
        const cleanValue = removePlaceholder(element.value);

        maskitoUpdateElement(element, cleanValue === prefix ? '' : cleanValue);
      }),
    ],
  } as MaskitoOptions);
}

export function applyPhoneMask(value) {
  if (value && _isString(value)) {
    return value.replace(/^(\d{3})?(\d{3})?(\d{4})?(.*)/, ' ($1) $2-$3$4');
  }
  return value;
}

export function addLeadingZero({ value }: { value: string }): RegExp[] {
  const maxLength = this.digitLimit || DIGIT_LIMIT;
  const dotRegexp = /\./;
  const digitRegexp = /\d/;
  let alreadyHaveDot = false;

  return value.split('').map((char, index) => {
    let indexWithDot = index;
    if (alreadyHaveDot && index !== 0) {
      indexWithDot = index - 1;
    }
    if (areLimitsExceeded(value, indexWithDot, maxLength )) {
      return NUMBER_INPUT_REGEXP.nothing;
    }
    if (char.match(dotRegexp) && index !== 0 && !alreadyHaveDot && !(value.startsWith('0') && index > 1)) {
      alreadyHaveDot = true;
      return dotRegexp;
    } else {
      return digitRegexp;
    }
  })
}
