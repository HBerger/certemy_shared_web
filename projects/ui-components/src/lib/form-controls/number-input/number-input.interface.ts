export enum MASK_TYPE {
  DEFAULT,
  POSITIVE_INTEGER,
  MULTIPLE_OF_HALF,
  PERCENTAGE,
  FLOAT,
  PHONE_NUMBER,
  ONE_DECIMAL,
  FROM_ZERO_TO_ONE,
  LEADING_ZERO
}

export interface NumberMaskConfig {
  maskType?: MASK_TYPE;
  digitLimit?: number;
  prefix?: string;
}

export interface Mask {
  mask: RegExp[] | ((value: string) => RegExp[]);
  guide: boolean;
}
