import { MASK_TYPE, NumberMaskConfig } from './number-input.interface';

export const NUMBER_INPUT_REGEXP = {
  dot: /\./,
  digit: /\d/,
  nothing: /a^/,
  zero: /[0]/,
  one: /[1]/,
  space: ' ',
  dash: '-',
  leftParenthesis: '(',
  rightParenthesis: ')',
};

export const DIGIT_LIMIT = 6;
export const DEFAULT_PREFIX = '';

export const DEFAULT_MASK_CONFIG: NumberMaskConfig = {
  maskType: MASK_TYPE.DEFAULT,
  digitLimit: DIGIT_LIMIT,
};
