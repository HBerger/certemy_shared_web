import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumberInputComponent } from './number-input.component';
import { FormsModule } from '@angular/forms';
import { MaskitoDirective } from '@maskito/angular';

const components = [NumberInputComponent];

@NgModule({
  imports: [CommonModule, FormsModule, MaskitoDirective],
  declarations: components,
  exports: components,
  providers: [],
})
export class NumberInputModule {}
