import { ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SmartForm } from '../../smart-form/smart-form.component';
import { noop } from '@certemy/common';
import { MASK_TYPE, NumberMaskConfig } from './number-input.interface';
import { DEFAULT_MASK_CONFIG, DIGIT_LIMIT, DEFAULT_PREFIX } from './number-input.constants';
import { getNumberMask } from './number-input.helpers';
import _cloneDeep from 'lodash-es/cloneDeep';
import { MaskitoOptions } from '@maskito/core';

// Requirements: Only positive numbers with 2 or less decimal symbols allowed

@Component({
  selector: 'cm-number-input',
  templateUrl: './number-input.component.html',
  styleUrls: ['./number-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => NumberInputComponent),
      multi: true,
    },
  ],
})
export class NumberInputComponent implements ControlValueAccessor {
  @Input() id = Math.random();
  @Input() options: SmartForm.FieldOption[] = [];
  @Input() placeholder = '';
  @Input()
  set maskConfig(config: NumberMaskConfig) {
    this.maskitoOptions = getNumberMask({
      maskType: config.maskType || MASK_TYPE.DEFAULT,
      digitLimit: config.digitLimit || DIGIT_LIMIT,
      prefix: config.prefix || DEFAULT_PREFIX,
    }) as MaskitoOptions;
    this.config = config;
  }
  @Input()
  set disabled(value: boolean) {
    this.setDisabledState(value);
  }

  maskitoOptions = getNumberMask(_cloneDeep(DEFAULT_MASK_CONFIG)) as MaskitoOptions;
  value = '';
  isDisabled = false;
  onTouched = noop;

  private propagateChange = noop;
  private config: NumberMaskConfig = DEFAULT_MASK_CONFIG;

  constructor(private changeDetector: ChangeDetectorRef) {}

  writeValue(value = '') {
    this.value = value;
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  onValueChange() {
    // Phone number mask has non-digit symbols
    const treatAsString =
      [MASK_TYPE.PHONE_NUMBER, MASK_TYPE.LEADING_ZERO].includes(this.config.maskType);
    const value = this.value ? (treatAsString ? this.value : +this.value) : '';
    this.propagateChange(value);
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.isDisabled = isDisabled;
  }
}
