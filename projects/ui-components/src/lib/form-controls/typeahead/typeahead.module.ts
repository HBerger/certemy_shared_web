import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypeaheadComponent } from './typeahead.component';
import { NgbTypeaheadModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

const components = [TypeaheadComponent];

@NgModule({
  imports: [CommonModule, NgbTypeaheadModule, ReactiveFormsModule, NgbTooltipModule],
  declarations: components,
  exports: components,
  providers: [],
})
export class TypeaheadModule {}
