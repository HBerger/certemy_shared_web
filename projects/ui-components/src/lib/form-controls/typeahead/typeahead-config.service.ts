import { Injectable } from '@angular/core';

@Injectable()
export class TypeaheadConfigService {
  escapeSearch = true;
}
