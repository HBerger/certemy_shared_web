import { Component, forwardRef, Input, OnDestroy, OnInit, ViewChild, ChangeDetectorRef, Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, catchError, switchMap, takeUntil, map, filter } from 'rxjs/operators';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { of, merge } from 'rxjs';
import { Subject } from 'rxjs';
import { NgbTypeahead, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { noop } from '@certemy/common';
import _isEqual from 'lodash-es/isEqual';
import { TypeaheadConfigService } from './typeahead-config.service';
import { identity } from 'rxjs';

@Component({
  selector: 'cm-typeahead',
  templateUrl: './typeahead.component.html',
  styleUrls: ['./typeahead.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TypeaheadComponent),
      multi: true,
    },
  ],
})
export class TypeaheadComponent implements OnInit, OnDestroy, ControlValueAccessor {
  @Input() id = Math.random();
  @Input() request$: (term: string) => Observable<any>;
  @Input() formatter: (result: any) => string;
  @Input() inputFormatter: (result: any) => string;
  @Input() placeholder = 'Search';
  @Input() tooltip: any;
  @Input() editable: boolean = false;
  @Input()
  set disabled(isDisabled: boolean) {
    this.setDisabledState(isDisabled);
  }
  @ViewChild('typeahead', { static: true })
  typeahead: NgbTypeahead;
  @ViewChild('typeaheadTooltip', { static: true })
  typeaheadTooltip: NgbTooltip;

  control = new UntypedFormControl();
  focus$ = new Subject<string>();
  onTouched = noop;

  private config: TypeaheadConfigService;
  private lastSettedValue;
  private onDestroy$ = new Subject<void>();
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef, @Optional() typeaheadConfigService: TypeaheadConfigService) {
    this.search = this.search.bind(this);
    const newPartialConfig: Partial<TypeaheadConfigService> = typeaheadConfigService || {};
    const defaultConfig = new TypeaheadConfigService();
    this.config = { ...defaultConfig, ...newPartialConfig };
  }

  ngOnInit() {
    this.control.valueChanges
      .pipe(
        // on focus control emit undefined
        filter(value => value !== undefined),
        filter(value => !_isEqual(value, this.lastSettedValue)),
        takeUntil(this.onDestroy$),
      )
      .subscribe(value => this.updateValue(value));

    // TODO: Replace this patch after fix at ng-bootstrap
    // Issue: https://github.com/ng-bootstrap/ng-bootstrap/issues/1276
    // Patch "handleBlur" method to clear input if value is not valid
    const typeaheadHandleBlur = this.typeahead.handleBlur;
    this.typeaheadTooltip.placement = (this.tooltip && this.tooltip.placement) || 'auto';
    this.typeahead.handleBlur = () => {
      typeaheadHandleBlur.call(this.typeahead);

      // If not editable - bootstrap emits undefined on blur, but doesn't clear input value in DOM
      // Clear typeahead input and set "null" instead of "undefined" to model
      if (!this.typeahead.editable && !this.control.value) {
        this.clearTypeahead();
      }
    };
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  search(text$: Observable<string>) {
    return merge(
      text$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
      ),
      this.focus$,
    ).pipe(
      map(this.config.escapeSearch ? encodeURIComponent : identity),
      switchMap((term: string) => this.request$(term)),
      catchError(() => of([])),
    );
  }

  writeValue(value = '') {
    this.lastSettedValue = value;
    this.control.setValue(value, { emitEvent: false });
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  onBlur() {
    this.onTouched();
  }

  setDisabledState(isDisabled: boolean) {
    isDisabled ? this.control.disable({ emitEvent: false }) : this.control.enable({ emitEvent: false });
  }

  private updateValue(value) {
    this.lastSettedValue = value;
    this.propagateChange(value);
  }

  private clearTypeahead() {
    this.typeahead['_onChange'](null);
    this.typeahead.writeValue(null);
  }
}
