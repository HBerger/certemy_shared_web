import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, UntypedFormControl } from '@angular/forms';
import { SmartForm } from '../../smart-form/smart-form.component';
import { noop } from '@certemy/common';

@Component({
  selector: 'cm-radio-group',
  templateUrl: './radio-group.component.html',
  styleUrls: ['./radio-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => RadioGroupComponent),
      multi: true,
    },
  ],
})
export class RadioGroupComponent implements ControlValueAccessor, Validator, OnDestroy {
  @Input() options: SmartForm.FieldOption[] = [];
  @Input() required = false;
  @Input()
  set disabled(value: boolean) {
    this.setDisabledState(value);
  }

  name = Math.random();
  value;
  isDisabled = false;
  onTouched = noop;
  private get hasValue() {
    return !!this.value || this.value === 0;
  }
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  writeValue(value) {
    this.value = value;
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  onRadioClick(id) {
    if (this.isDisabled || this.value === id) {
      return;
    }

    this.value = id;
    this.propagateChange(this.value);
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  validate(c: UntypedFormControl) {
    return this.required && !this.hasValue ? { required: true } : null;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  ngOnDestroy() {
    this.validate = () => null;
  }
}
