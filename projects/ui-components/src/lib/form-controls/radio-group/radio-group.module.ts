import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { RadioGroupComponent } from './radio-group.component';
import { RadioModule } from '../radio/radio.module';
import { CommonModule } from '@angular/common';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, RadioModule, NgbTooltipModule],
  exports: [RadioGroupComponent],
  declarations: [RadioGroupComponent],
})
export class RadioGroupModule {}
