import { NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { MultiSelectComponent } from './multi-select.component';
import { CheckboxModule } from '../checkbox/checkbox.module';
import { ChipsModule } from '../../chips/chips.module';
import { SearchInputModule } from '../search-input';

@NgModule({
  imports: [SharedCommonModule, NgbDropdownModule, CheckboxModule, ChipsModule, SearchInputModule],
  exports: [MultiSelectComponent],
  declarations: [MultiSelectComponent],
})
export class MultiSelectModule {}
