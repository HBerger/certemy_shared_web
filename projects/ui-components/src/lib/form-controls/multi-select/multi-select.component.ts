import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  TemplateRef,
  ViewChild,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { SmartForm } from '@certemy/ui-components';
import { ComputedFrom, noop } from '@certemy/common';
import _keyBy from 'lodash-es/keyBy';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject, merge, BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, skip, takeUntil, take } from 'rxjs/operators';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";

export namespace MultiSelect {
  export enum ValueType {
    Object = 'Object',
    Id = 'Id',
  }

  export type OptionId = string | number;
  export type ValueEntry = SmartForm.FieldOption | OptionId;
}

export interface MultiSelectParams {
  withSearch: boolean;
  noContentConfig: {
    title: string;
    subtitle: string;
  };
  withChips: boolean;
  withSelectAll: boolean;
  onlyUnselectAllBtn: boolean;
  optionDetailColumns: any[];
  template: TemplateRef<any>;
  debounceTime: number;
}

const defaultMultiSelectParams: MultiSelectParams = {
  withSearch: false,
  noContentConfig: null,
  withChips: false,
  withSelectAll: false,
  onlyUnselectAllBtn: false,
  optionDetailColumns: [],
  template: null,
  debounceTime: 0,
};

@Component({
  selector: 'cm-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiSelectComponent),
      multi: true,
    },
  ],
})
export class MultiSelectComponent implements OnDestroy {
  _params: MultiSelectParams = defaultMultiSelectParams;
  @ViewChild('dropDown') dropDown: NgbDropdown;
  @Input()
  set options(value: SmartForm.FieldOption[]) {
    this.search = '';
    this.searchedOptions = [];
    this._options = value;
  }
  @Input()
  set disabled(isDisabled) {
    this.isDisabled = isDisabled;
    this.changeDetector.markForCheck();
  }
  @Input() valueType: MultiSelect.ValueType = MultiSelect.ValueType.Id;
  @Input() placeholder: string;
  @Input()
  set params(value: Partial<MultiSelectParams>) {
    this._params = {
      ...defaultMultiSelectParams,
      ...value,
    };
  }
  @Input() showValues = false;
  @Input() request$: (options: SmartForm.FieldOption[]) => Observable<SmartForm.FieldOption[]>;

  valueMap = new Map<MultiSelect.OptionId, SmartForm.FieldOption>();
  isOpen = false;
  onTouched = noop;
  searchedOptions: SmartForm.FieldOption[] = [];
  search = '';
  _options: SmartForm.FieldOption[] = [];
  private onDestroy$ = new Subject<void>();

  private isDisabled = false;

  private changeValue$ = new BehaviorSubject(null);
  private mouseLeave$ = new Subject<void>();

  readonly VALUE_TYPE = MultiSelect.ValueType;

  @HostListener('mouseleave')
  onMouseLeaveEvent() {
    this.mouseLeave$.next();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  @ComputedFrom('_options')
  get optionsMap(): { [id: string]: SmartForm.FieldOption } {
    return _keyBy(this._options, 'id') as any;
  }

  @ComputedFrom('_options', 'isOpen', 'checkedCount')
  get selectPlaceholder(): string | MultiSelect.ValueEntry[] {
    if (!this._options.length) {
      return this.placeholder || 'No Options Available';
    } else if (this.checkedCount === 0 && this.isOpen) {
      return this.placeholder || '0 Selected';
    } else if (this.checkedCount === 0 && !this.isOpen) {
      return this.placeholder || 'Please select';
    } else if (this.isDisabled && this.showValues) {
      return this.mapToArray(this.valueMap);
    } else if (this.checkedCount === this._options.length) {
      return 'All Selected';
    } else {
      return `${this.valueMap.size} Selected`;
    }
  }

  @ComputedFrom('_options', 'isOpen', 'checkedCount')
  get showPlaceholderValues(): boolean {
    return this._options.length && this.checkedCount !== 0 && this.isDisabled && this.showValues;
  }

  get checkedCount() {
    return this.valueMap.size;
  }

  get disabled() {
    return this.isDisabled || (!this._options.length && !this.request$);
  }

  constructor(private changeDetector: ChangeDetectorRef) {}

  onSearch(search = '') {
    this.search = search.trim();
    this.searchedOptions = this._options.filter(option => option.name.toLowerCase().includes(search.toLowerCase()));
  }

  writeValue(value) {
    this.valueMap = value ? this.arrayToMap(value) : new Map();
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    merge(this.changeValue$.pipe(debounceTime(this._params.debounceTime)), this.mouseLeave$)
      .pipe(
        takeUntil(this.onDestroy$),
        map(() => this.changeValue$.getValue()),
        distinctUntilChanged(),
        skip(1),
      )
      .subscribe(fn);
  }

  private propagateChange(values) {
    this.changeValue$.next(values);
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled = false) {
    this.disabled = isDisabled;
  }

  toggleOption(option: SmartForm.FieldOption) {
    if (this.valueMap.has(option.id)) {
      this.valueMap.delete(option.id);
    } else {
      if (!option.disabled) {
        this.valueMap.set(option.id, option);
      }
    }

    this.propagateChange(this.mapToArray(this.valueMap));
  }

  onOpenChange(isOpen: boolean) {
    if (this.request$ && isOpen) {
      this.getOptionsFromRequest();
    } else {
      this.isOpen = isOpen;
    }
  }

  private getOptionsFromRequest() {
    this.request$(this._options)
      .pipe(
        take(1),
        catchError(() => of([])),
      )
      .subscribe(options => {
        this.options = options;
        this.isOpen = true;
        this.changeDetector.markForCheck();
      });
  }

  toggle(event) {
    event.stopPropagation();

    if (!this.disabled) {
      this.dropDown.toggle();
    }
  }

  selectAll() {
    let options;
    if (this.search) {
      options = this.searchedOptions.filter(option => !option.disabled);
    } else {
      options = this._options.filter(option => !option.disabled);
    }
    options.forEach(option => this.valueMap.set(option.id, option));

    this.propagateChange(this.mapToArray(this.valueMap));
  }

  unselectAll() {
    this.valueMap.clear();

    this.propagateChange(this.mapToArray(this.valueMap));
  }

  mapToArray(
    valueMap: Map<MultiSelect.OptionId, SmartForm.FieldOption>,
    valueType: MultiSelect.ValueType = this.valueType,
  ): MultiSelect.ValueEntry[] {
    let resultArray = [];

    valueMap.forEach(option => {
      resultArray = [...resultArray, this.transformToOutputFormat(option, valueType)];
    });

    return resultArray;
  }

  private arrayToMap(value: MultiSelect.ValueEntry[]): Map<MultiSelect.OptionId, SmartForm.FieldOption> {
    const iterableValue = value.map(
      (transformedValue: MultiSelect.ValueEntry): [MultiSelect.OptionId, SmartForm.FieldOption] => {
        const optionId = this.getOptionId(transformedValue);

        return [optionId, this.optionsMap[optionId]];
      },
    );
    return new Map(iterableValue);
  }

  private transformToOutputFormat(option, valueType: MultiSelect.ValueType) {
    return valueType === MultiSelect.ValueType.Object ? option : option.id;
  }

  private getOptionId(transformedValue: MultiSelect.ValueEntry): MultiSelect.OptionId {
    return this.isFieldOption(transformedValue) ? transformedValue.id : transformedValue;
  }

  private isFieldOption(entry: MultiSelect.ValueEntry): entry is SmartForm.FieldOption {
    return this.valueType === MultiSelect.ValueType.Object;
  }
}
