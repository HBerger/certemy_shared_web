import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { Location } from './location.component';
import { SmartForm } from '../../smart-form';
import { Injectable } from '@angular/core';

@Injectable()
export class LocationService {
  constructor() {
    this.init().subscribe();
  }

  init(): Observable<boolean> {
    return this.getCountries().pipe(mapTo(true));
  }

  getCountries(): Observable<SmartForm.FieldOption[]> {
    return of([]);
  }

  getStatesOrCities(
    countryId: string,
  ): Observable<{ states: SmartForm.FieldOption[]; cities: SmartForm.FieldOption[]; countryId: string }> {
    return of({ states: [], cities: [], countryId });
  }

  getCitiesByState(stateId: string): Observable<Location.StateChange> {
    return of({ cities: [], stateId });
  }
}
