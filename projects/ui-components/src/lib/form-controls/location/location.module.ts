import { ModuleWithProviders, NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedCommonModule } from '@certemy/common';
import { SelectModule } from '../select/select.module';
import { LocationComponent } from './location.component';
import { LocationService } from './location.service';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, SharedCommonModule, SelectModule],
  exports: [LocationComponent],
  declarations: [LocationComponent],
})
export class LocationModule {
  static providers = [LocationService];
  static forRoot(): ModuleWithProviders<LocationModule> {
    return {
      ngModule: LocationModule,
      providers: [LocationService],
    };
  }
}
