import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import {
  ControlValueAccessor,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator,
  Validators,
} from '@angular/forms';
import { LocationService } from './location.service';
import { distinctUntilChanged, filter, map, switchMap, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SmartForm } from '../../smart-form';
import { of } from 'rxjs';
import { Observable } from 'rxjs';
import { noop } from '@certemy/common';

const Selectors = {
  getCountry: map<Location.Value, string>(location => location.country),
  getState: map<Location.Value, string>(location => location.state),
  getCity: map<Location.Value, string>(location => location.city),
};

export namespace Location {
  export interface Value {
    country: string;
    state: string;
    city: string;
  }

  export interface FormState {
    country: FormControlState;
    state: FormControlState;
    city: FormControlState;
  }

  export interface FormControlState {
    value: string;
    disabled: boolean;
    required: boolean;
  }

  export interface CountryChange {
    states: SmartForm.FieldOption[];
    cities: SmartForm.FieldOption[];
    countryId: string;
  }

  export interface StateChange {
    cities: SmartForm.FieldOption[];
    stateId: string;
  }
}

@Component({
  selector: 'cm-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LocationComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => LocationComponent),
      multi: true,
    },
  ],
})
export class LocationComponent implements OnInit, OnDestroy, ControlValueAccessor, Validator {
  @Input() showErrors = false;
  locationForm: UntypedFormGroup = this.fb.group({
    country: ['', Validators.required],
    state: ['', Validators.required],
    city: ['', Validators.required],
  });
  countries: SmartForm.FieldOption[] = [{ id: '4', name: 'United States' }];
  states: SmartForm.FieldOption[] = [];
  cities: SmartForm.FieldOption[] = [];

  get location() {
    const { country, state, city } = this.formState;
    return {
      country: country.value,
      state: state.value === this.NO_STATES_OPTION.id ? '' : state.value,
      city: city.value,
    };
  }
  private readonly NO_STATES_OPTION = {
    id: 'NO_STATES',
    name: 'No states for this country',
  };
  formState = {
    country: { value: '', disabled: false, required: true },
    state: { value: '', disabled: false, required: true },
    city: { value: '', disabled: true, required: true },
  };
  onTouched = noop;

  private countriesSubscription;
  private onDestroy$ = new Subject<void>();
  private location$ = this.locationForm.valueChanges.pipe(takeUntil(this.onDestroy$));
  private country$ = this.location$.pipe(Selectors.getCountry);
  private state$ = this.location$.pipe(Selectors.getState);
  private city$ = this.location$.pipe(Selectors.getCity);
  private propagateChange = noop;

  constructor(
    private fb: UntypedFormBuilder,
    private locationService: LocationService,
    private changeDetector: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.countriesSubscription = this.locationService
      .getCountries()
      .subscribe(countries => (this.countries = countries));

    this.country$
      .pipe(
        filter(countryId => !!countryId),
        distinctUntilChanged(),
        switchMap(countryId => this.locationService.getStatesOrCities(countryId)),
      )
      .subscribe((result: Location.CountryChange) => this.onCountrySelect(result));
    this.state$
      .pipe(
        filter(stateId => !!stateId),
        distinctUntilChanged(),
        switchMap(stateId => this.locationService.getCitiesByState(stateId)),
      )
      .subscribe((result: Location.StateChange) => this.onStateSelect(result));
    this.city$
      .pipe(
        filter(cityId => !!cityId),
        distinctUntilChanged(),
      )
      .subscribe((cityId: string) => this.onCitySelect(cityId));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  writeValue(location: Location.Value) {
    if (!location || !location.country) {
      this.setFormValue({ country: '4', state: '', city: '' }, { emitValue: true });
      return;
    }

    if (!this.countriesSubscription.closed) {
      this.countriesSubscription.add(() => this.setFormValue(location));
    } else {
      this.setFormValue(location);
    }
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  validate(c: UntypedFormControl) {
    return this.locationForm.valid ? null : { invalidLocation: true };
  }

  private onCountrySelect({ states, cities, countryId }: Location.CountryChange) {
    this.states = states;
    this.cities = cities;

    if (this.states.length) {
      this.formState = {
        country: { value: countryId, disabled: false, required: true },
        state: { value: '', disabled: false, required: true },
        city: { value: '', disabled: true, required: true },
      };
    } else if (!this.states.length && this.cities.length) {
      this.states = [this.NO_STATES_OPTION];
      this.formState = {
        country: { value: countryId, disabled: false, required: true },
        state: { value: this.NO_STATES_OPTION.id, disabled: true, required: false },
        city: { value: '', disabled: false, required: true },
      };
    }

    this.updateForm(this.formState);
    this.propagateChange(this.location);
  }

  private onStateSelect({ cities, stateId }: Location.StateChange) {
    this.cities = cities;
    this.formState = {
      ...this.formState,
      state: { value: stateId, disabled: false, required: true },
      city: { value: '', disabled: false, required: true },
    };
    this.updateForm(this.formState);
    this.propagateChange(this.location);
  }

  private onCitySelect(cityId: string) {
    this.formState = {
      ...this.formState,
      city: { value: cityId, disabled: false, required: true },
    };
    this.propagateChange(this.location);
  }

  private updateForm(formState: Location.FormState) {
    Object.keys(formState).forEach(controlName => {
      const control = this.locationForm.get(controlName);
      const { value, disabled } = formState[controlName];
      const action = disabled ? 'disable' : 'enable';

      control.setValue(value, { onlySelf: true, emitEvent: false });
      control[action]({ emitEvent: false });
    });
  }

  private updateFormState(currentState: Location.FormState, stateUpdate): Location.FormState {
    const newState: Location.FormState = { ...currentState };

    Object.keys(currentState).forEach(locationKey => {
      const currentElemState: Location.FormControlState = currentState[locationKey];
      const elemUpdateValue: string = stateUpdate[locationKey];
      newState[locationKey] = {
        ...currentElemState,
        value: elemUpdateValue || currentElemState.value,
      };
    });

    return newState;
  }

  private setFormValue(newValue: Location.Value, options?: { emitValue: boolean }) {
    if (newValue.state || newValue.city) {
      this.formState = {
        ...this.formState,
        city: {
          ...this.formState.city,
          disabled: false,
        },
      };
    }

    this.locationService
      .getStatesOrCities(newValue.country)
      .pipe(
        switchMap(
          ({
            states,
            cities,
          }: Location.CountryChange): Observable<{
            states: SmartForm.FieldOption[];
            cities: SmartForm.FieldOption[];
          }> => {
            if (!newValue.state) {
              return of({ states, cities });
            }

            if (states.length && newValue.state) {
              return this.locationService
                .getCitiesByState(newValue.state)
                .pipe(map((result: Location.StateChange) => ({ states, cities: result.cities })));
            } else {
              return of({ states: [this.NO_STATES_OPTION], cities });
            }
          },
        ),
      )
      .subscribe(({ states, cities }) => {
        this.states = states;
        this.cities = cities;

        this.formState = this.updateFormState(this.formState, newValue);
        this.updateForm(this.formState);

        if (options && options.emitValue) {
          this.propagateChange(this.location);
        }
      });
  }
}
