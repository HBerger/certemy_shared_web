import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CustomControlComponent } from './custom-control.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  exports: [CustomControlComponent],
  declarations: [CustomControlComponent],
})
export class CustomControlModule {}
