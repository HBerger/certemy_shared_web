import { Component, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'cm-custom-control',
  templateUrl: './custom-control.component.html',
})
export class CustomControlComponent {
  @Input() template: TemplateRef<any>;
}
