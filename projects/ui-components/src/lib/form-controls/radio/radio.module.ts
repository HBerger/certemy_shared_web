import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { RadioComponent } from './radio.component';

@NgModule({
  imports: [ReactiveFormsModule],
  exports: [RadioComponent],
  declarations: [RadioComponent],
})
export class RadioModule {}
