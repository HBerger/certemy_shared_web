import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'cm-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
})
export class RadioComponent implements OnInit {
  @Input() name: string;
  @Input() checked = false;
  @Input() disabled = false;

  id = Math.random();

  ngOnInit() {
    if (!this.name) {
      throw new Error('Provide name property for Radio component.');
    }
  }
}
