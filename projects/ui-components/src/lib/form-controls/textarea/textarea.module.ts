import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextareaComponent } from './textarea.component';
import { ReactiveFormsModule } from '@angular/forms';

const components = [TextareaComponent];

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  declarations: components,
  exports: components,
  providers: [],
})
export class TextareaModule {}
