import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { SmartForm } from '../../smart-form/smart-form.component';
import { takeUntil } from 'rxjs/operators';
import { noop } from '@certemy/common';
import { Dimensions } from './textarea.interface';
import { DEFAULT_DIMENSIONS } from './textarea.constants';
import _cloneDeep from 'lodash-es/cloneDeep';

@Component({
  selector: 'cm-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextareaComponent),
      multi: true,
    },
  ],
})
export class TextareaComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @Input() id = Math.random();
  @Input() options: SmartForm.FieldOption[] = [];
  @Input() placeholder = '';
  @Input() maxlength = '';
  @Input() dimensions: Dimensions = _cloneDeep(DEFAULT_DIMENSIONS);
  @Input()
  set disabled(value: boolean) {
    this.setDisabledState(value);
  }

  DEFAULT_DIMENSIONS = _cloneDeep(DEFAULT_DIMENSIONS);
  control = new UntypedFormControl('');
  onTouched = noop;

  private onDestroy$ = new Subject<void>();
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.control.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(value => this.propagateChange(value));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  writeValue(value = '') {
    this.control.setValue(value, { emitEvent: false });
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    const command = isDisabled ? 'disable' : 'enable';
    this.control[command]({ emitEvent: false });
  }
}
