export interface Dimensions {
  cols?: number;
  rows?: number;
}
