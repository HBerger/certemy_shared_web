import { Dimensions } from './textarea.interface';

export const DEFAULT_DIMENSIONS: Dimensions = {
  rows: 4,
  cols: 50,
};
