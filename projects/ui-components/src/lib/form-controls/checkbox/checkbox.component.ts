import { ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';

@Component({
  selector: 'cm-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
})
export class CheckboxComponent implements ControlValueAccessor, OnInit {
  @Input()
  set checked(value: boolean) {
    this.value = value || false;
  }

  @Input() required = false;
  @Input() disabled = false;
  @Output() changeValue: EventEmitter<boolean> = new EventEmitter<boolean>();

  value = false;
  id = Math.random();
  onTouched = noop;

  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {}

  writeValue(value = false) {
    this.value = value;
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  onInputClick() {
    this.value = !this.value;
    this.propagateChange(this.value);
    this.changeValue.emit(this.value);
  }

  onLabelClick(event) {
    event.stopPropagation();
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }
}
