import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateRangeComponent } from './date-range.component';
import { FormsModule } from '@angular/forms';
import { DatePickerModule } from '../datepicker/datepicker.module';

const components = [DateRangeComponent];

@NgModule({
  imports: [CommonModule, FormsModule, DatePickerModule],
  declarations: components,
  exports: components,
  providers: [],
})
export class DateRangeModule {}
