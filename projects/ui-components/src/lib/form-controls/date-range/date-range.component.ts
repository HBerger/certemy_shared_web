import { ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';

export interface DateRangeFormat {
  from: number;
  to: number;
}

@Component({
  selector: 'cm-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateRangeComponent),
      multi: true,
    },
  ],
})
export class DateRangeComponent implements ControlValueAccessor {
  @Input() id = Math.random();
  @Input() minDate: Date | string;
  @Input() maxDate: Date | string;
  value: DateRangeFormat = { from: null, to: null };
  disabled = false;
  onTouched = noop;

  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  writeValue(value: DateRangeFormat) {
    value = value || { from: null, to: null };
    this.value = value;
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  onValueChange(newValue: Partial<DateRangeFormat>) {
    this.value = { ...this.value, ...newValue };
    this.propagateChange(this.value);
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.disabled = isDisabled;
  }
}
