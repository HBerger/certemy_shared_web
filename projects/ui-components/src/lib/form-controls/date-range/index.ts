import { DateRangeFormat as DateRangeFormatInterface } from './date-range.component';
export * from './date-range.module';
export * from './date-range.component';
export type DateRangeFormat = DateRangeFormatInterface;
