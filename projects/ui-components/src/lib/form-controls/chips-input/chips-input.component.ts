import { Component, ChangeDetectorRef, forwardRef, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { UntypedFormControl, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';
import _get from 'lodash-es/get';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'cm-chips-input',
  templateUrl: './chips-input.component.html',
  styleUrls: ['./chips-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ChipsInputComponent),
      multi: true,
    },
  ],
})
export class ChipsInputComponent implements ControlValueAccessor, OnInit {
  @Input() placeholder = '';
  @Input() validators = [];
  @Input()
  set disabled(isDisabled: boolean) {
    if (this.formControl) {
      this.formControl[isDisabled ? 'disable' : 'enable']();
    }

    this.isDisabled = isDisabled;
  }
  @Input()
  params = {
    compactMode: false,
  };
  @Input() displayError = false;
  @Output() formErrorChanged = new EventEmitter<boolean>();
  formControl: UntypedFormControl;
  onTouched = noop;
  private inputValues = new Set();
  private propagateChange = noop;
  private isDisabled = false;

  get selectedValues() {
    return Array.from(this.inputValues).map((value: string) => ({ name: value }));
  }

  get errorMessage() {
    return Object.values(this.formControl.errors).find(errorMessage => errorMessage);
  }

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.formControl = new UntypedFormControl({ value: '', disabled: this.isDisabled }, this.validators);
    this.formControl.valueChanges.pipe(filter(value => value)).subscribe(() => {
      this.displayError = false;
    });
  }

  addValue() {
    const inputValue = (this.formControl.value || '').trim();

    if (this.formControl.errors) {
      this.displayError = true;

      return;
    }

    if (inputValue) {
      this.inputValues.add(inputValue);
      this.formControl.reset();
      this.propagateChange(Array.from(this.inputValues));
    }
  }

  removeValue(value) {
    this.inputValues.delete(value.name);
    this.propagateChange(Array.from(this.inputValues));

    if (this.formControl.errors) {
      this.displayError = true;
    }
  }

  writeValue(values = []) {
    values.forEach(value => this.inputValues.add(value));
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.disabled = isDisabled;
  }

  isFormError() {
    let isError = false;

    if (this.displayError) {
      isError = _get(this.formControl, 'errors.required')
        ? !Array.from(this.inputValues).length
        : !!this.formControl.errors;
    }

    this.formErrorChanged.emit(isError);

    return isError;
  }
}
