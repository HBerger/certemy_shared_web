import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ChipsInputComponent } from './chips-input.component';
import { TextInputModule } from '../text-input';
import { ChipsModule } from '../../chips';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, TextInputModule, ChipsModule],
  declarations: [ChipsInputComponent],
  exports: [ChipsInputComponent],
})
export class ChipsInputModule {}
