import {
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  forwardRef,
  Injectable,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALIDATORS, NG_VALUE_ACCESSOR, Validator } from '@angular/forms';
import { NgbDateParserFormatter, NgbDatepicker, NgbDateStruct, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { noop } from '@certemy/common';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { fromEvent } from 'rxjs';

const MONTHS: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

/**
 * This Service handles how the date is rendered and parsed from keyboard i.e. in the bound input field.
 */
@Injectable()
export class CustomDateParserFormatter extends NgbDateParserFormatter {
  readonly DELIMITER = '-';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        year: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        day: parseInt(date[2], 10),
      };
    }
    return null;
  }

  format(date: NgbDateStruct | null): string {
    return date ? MONTHS[date.month - 1] + this.DELIMITER + date.day : '';
  }
}

@Component({
  selector: 'cm-datepicker-month-date',
  templateUrl: './datepicker-month-date.component.html',
  styleUrls: ['./datepicker-month-date.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerMonthDateComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DatePickerMonthDateComponent),
      multi: true,
    },
    { provide: CustomDateParserFormatter, useClass: CustomDateParserFormatter },
  ],
})
export class DatePickerMonthDateComponent implements ControlValueAccessor, Validator, OnInit, OnDestroy {
  @ViewChild('d', { static: true })
  datePicker: NgbInputDatepicker;
  @Input() id = `datepicker-item${Math.floor(Math.random() * 100000)}`;
  @Input() markDisabled;
  @Input()
  set minDate(date) {
    if (date) {
      this.minDateParsed = this.dateService.parse(date);
    }
  }
  @Input()
  set maxDate(date) {
    if (date) {
      this.maxDateParsed = this.dateService.parse(date);
    }
  }
  @Input()
  set enabled(value: boolean) {
    this.setDisabledState(!value);
  }
  @Input() placeholder = 'mm-dd';

  /** deprecated, for backward compatibility **/
  @Input() useDeprecatedDateOutputFormat = false;

  date;
  parseError: boolean;
  isDisabled = false;
  minDateParsed: NgbDateStruct;
  maxDateParsed: NgbDateStruct;
  onTouched = noop;

  private propagateChange = noop;
  private readonly onDestroy$ = new Subject<void>();

  constructor(
    private dateService: NgbDateParserFormatter,
    private elemRef: ElementRef,
    private changeDetector: ChangeDetectorRef,
    private ngZone: NgZone,
  ) {}

  ngOnInit() {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(window.document, 'click')
        .pipe(
          filter((event: MouseEvent) => {
            return this.datePicker.isOpen() && this.isClickOutside(event) && this.isClickOutsideNgbDatePicker(event);
          }),
          takeUntil(this.onDestroy$),
        )
        .subscribe(() => {
          this.ngZone.run(() => {
            this.datePicker.close();
          });
        });
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  toggle(event?) {
    if (event) {
      event.preventDefault();

      // prevent focus at IE
      event.currentTarget.blur();
    }

    if (!this.isDisabled) {
      this.datePicker.toggle();
      this.addCustomClassStyle();
    }
  }

  private addCustomClassStyle() {
    if (this.datePicker['_cRef']) {
      this.datePicker['_cRef'].location.nativeElement.classList.add('custom-dropdown');
    }
  }

  writeValue(value: any) {
    if (value) {
      if (typeof value === 'string') {
        try {
          this.date = this.dateService.parse(value);
          this.parseError = false;
        } catch (err) {
          this.parseError = true;
        }
      } else if (typeof value === 'object') {
        this.date = value;
      } else {
        this.parseError = true;
      }
    } else {
      this.date = null;
    }
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  validate(c: UntypedFormControl) {
    return !this.parseError
      ? null
      : {
          dateParseError: {
            valid: false,
          },
        };
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  onDateChange(event: NgbDateStruct) {
    if (!event) {
      this.parseError = false;
      this.date = event;
      this.propagateChange('');
    } else if (typeof event === 'object') {
      this.parseError = false;
      this.date = event;
      this.propagateChange(this.formatDate(event));
    } else {
      this.parseError = true;
    }
    this.onTouched();
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.datePicker.setDisabledState(isDisabled);
  }

  private formatDate(dateStruct: NgbDateStruct): string {
    if (this.useDeprecatedDateOutputFormat) {
      return this.dateService.format(dateStruct);
    } else {
      const { day, month, year } = dateStruct;
      return new Date(year, month - 1, day, 12).toISOString();
    }
  }

  private isClickOutside(event: MouseEvent): boolean {
    return !this.elemRef.nativeElement.contains(event.target);
  }

  private isClickOutsideNgbDatePicker(event: MouseEvent): boolean {
    const componentRef: ComponentRef<NgbDatepicker> = this.datePicker['_cRef'];
    return componentRef && !componentRef.location.nativeElement.contains(event.target);
  }
}
