import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePickerMonthDateComponent } from './datepicker-month-date.component';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

const components = [DatePickerMonthDateComponent];

@NgModule({
  imports: [CommonModule, NgbDatepickerModule, FormsModule],
  declarations: components,
  exports: components,
  providers: [],
})
export class DatePickerMonthDateModule {}
