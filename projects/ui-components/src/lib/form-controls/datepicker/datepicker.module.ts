import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePickerComponent } from './datepicker.component';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

const components = [DatePickerComponent];

@NgModule({
  imports: [CommonModule, NgbDatepickerModule, FormsModule],
  declarations: components,
  exports: components,
})
export class DatePickerModule {}
