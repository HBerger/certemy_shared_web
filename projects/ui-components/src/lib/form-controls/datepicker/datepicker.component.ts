import {
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  forwardRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
  HostListener,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  UntypedFormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator,
} from '@angular/forms';
import { NgbDatepicker, NgbDateStruct, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { noop } from '@certemy/common';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import moment from 'moment-mini';

@Component({
  selector: 'cm-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    },
  ],
})
export class DatePickerComponent implements ControlValueAccessor, Validator, OnInit, OnDestroy {
  @ViewChild('d', { static: true })
  datePicker: NgbInputDatepicker;
  @Input() id = `datepicker-item${Math.floor(Math.random() * 100000)}`;
  @Input() markDisabled;
  @Input()
  set minDate(date: string) {
    if (date) {
      this.minDateParsed = this.fromModel(date);
    } else {
      this.minDateParsed = undefined;
    }
  }
  @Input()
  set maxDate(date: string) {
    if (date) {
      this.maxDateParsed = this.fromModel(date);
    } else {
      this.maxDateParsed = undefined;
    }
  }
  @Input()
  set enabled(value: boolean) {
    this.setDisabledState(!value);
  }
  @Input() placeholder = 'mm-dd-yyyy';

  /** deprecated, for backward compatibility **/
  @Input() useDeprecatedDateOutputFormat = false;

  date;
  parseError: boolean;
  isDisabled = false;
  minDateParsed: NgbDateStruct;
  maxDateParsed: NgbDateStruct;
  onTouched = noop;

  private propagateChange = noop;
  private readonly onDestroy$ = new Subject<void>();

  @HostListener('focusout')
  onMouseLeaveEvent() {
    if (this.parseError) {
      if (!this.date) {
        this.datePicker.writeValue(this.date);
        this.parseError = false;
      } else {
        const control = {
          value: this.date,
        };
        const isValid = !this.datePicker.validate(control as AbstractControl);
        if (isValid) {
          this.datePicker.writeValue(this.date);
          this.parseError = false;
        }
      }
    }
  }

  constructor(private elemRef: ElementRef, private changeDetector: ChangeDetectorRef, private ngZone: NgZone) {}

  ngOnInit() {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(window.document, 'click')
        .pipe(
          filter(
            (event: MouseEvent) =>
              this.datePicker.isOpen() && this.isClickOutside(event) && this.isClickOutsideNgbDatePicker(event),
          ),
          takeUntil(this.onDestroy$),
        )
        .subscribe(() => {
          this.ngZone.run(() => {
            this.datePicker.close();
          });
        });
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  toggle(event?) {
    if (event) {
      event.preventDefault();

      // prevent focus at IE
      event.currentTarget.blur();
    }

    if (!this.isDisabled) {
      this.datePicker.toggle();
    }
  }

  writeValue(value: any) {
    if (value) {
      if (typeof value === 'string') {
        try {
          this.date = this.fromModel(value);
          this.parseError = false;
        } catch (err) {
          this.parseError = true;
        }
      } else if (typeof value === 'object') {
        this.date = value;
      } else {
        this.parseError = true;
      }
    } else {
      this.date = null;
    }
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  validate(c: UntypedFormControl) {
    return !this.parseError
      ? null
      : {
          dateParseError: {
            valid: false,
          },
        };
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  onDateChange(event: NgbDateStruct | null, close = false) {
    if (!event) {
      this.parseError = false;
      this.date = event;
      this.propagateChange('');
    } else if (typeof event === 'object') {
      this.parseError = false;
      const control = {
        value: event,
      };
      const isValid = !this.datePicker.validate(control as AbstractControl);
      if (isValid) {
        this.date = event;
        this.propagateChange(this.formatDate(event));
      } else {
        this.date = null;
        this.parseError = true;
      }
    } else {
      this.parseError = true;
    }
    this.onTouched();
    if (close) {
      this.datePicker.close();
    }
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    this.datePicker.setDisabledState(isDisabled);
  }

  private formatDate(dateStruct: NgbDateStruct): string {
    if (this.useDeprecatedDateOutputFormat) {
      return this.toDeprecatedDateOutputFormat(dateStruct);
    } else {
      const { day, month, year } = dateStruct;
      return new Date(year, month - 1, day, 12).toISOString();
    }
  }

  private isClickOutside(event: MouseEvent): boolean {
    return !this.elemRef.nativeElement.contains(event.target);
  }

  private isClickOutsideNgbDatePicker(event: MouseEvent): boolean {
    const componentRef: ComponentRef<NgbDatepicker> = this.datePicker['_cRef'];
    return componentRef && !componentRef.location.nativeElement.contains(event.target);
  }

  fromModel(value: string): NgbDateStruct | null {
    if (value) {
      const date = moment(value, [moment.ISO_8601, 'YYYY-M-D'], true);
      if (date.isValid()) {
        return {
          day: date.date(),
          month: date.month() + 1,
          year: date.year(),
        };
      }
    }
    return null;
  }

  private toDeprecatedDateOutputFormat(date: NgbDateStruct | null): string {
    return date ? moment([date.year, date.month - 1, date.day]).format('YYYY-MM-DD') : null;
  }
}
