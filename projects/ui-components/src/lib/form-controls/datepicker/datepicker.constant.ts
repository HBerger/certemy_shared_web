import {InjectionToken} from '@angular/core';

export const DEFAULT_DATEPICKER_DELIMITER = '-';

export const DATEPICKER_DELIMITER_TOKEN = new InjectionToken<string>('DATEPICKER_DELIMITER_TOKEN');
