import { ChangeDetectorRef, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';
import { DEFAULT_MASK_CONFIG } from '@certemy/ui-components';
import { NumberMaskConfig } from '../number-input/number-input.interface';
import _cloneDeep from 'lodash-es/cloneDeep';

export interface RangeFormat {
  from: number;
  to: number;
}

@Component({
  selector: 'cm-range',
  templateUrl: './range.component.html',
  styleUrls: ['./range.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RangeComponent),
      multi: true,
    },
  ],
})
export class RangeComponent implements ControlValueAccessor {
  @Input() id = Math.random();
  @Input() maskConfig: NumberMaskConfig = _cloneDeep(DEFAULT_MASK_CONFIG);
  value: RangeFormat = { from: null, to: null };
  disabled = false;
  onTouched = noop;

  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  writeValue(value: RangeFormat) {
    value = value || { from: null, to: null };
    this.value = value;
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  onValueChange(newValue: Partial<RangeFormat>) {
    this.value = { ...this.value, ...newValue };
    this.propagateChange(this.value);
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.disabled = isDisabled;
  }
}
