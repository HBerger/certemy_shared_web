import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RangeComponent } from './range.component';
import { FormsModule } from '@angular/forms';
import { NumberInputModule } from '../number-input/number-input.module';

const components = [RangeComponent];

@NgModule({
  imports: [CommonModule, FormsModule, NumberInputModule],
  declarations: components,
  exports: components,
  providers: [],
})
export class RangeModule {}
