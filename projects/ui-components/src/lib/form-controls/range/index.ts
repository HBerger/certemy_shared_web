import { RangeFormat as RangeFormatInterface } from './range.component';
export * from './range.module';
export * from './range.component';
export type RangeFormat = RangeFormatInterface;
