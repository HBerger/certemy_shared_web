import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { noop } from '@certemy/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'cm-secure-text-input',
  templateUrl: './secure-text-input.component.html',
  styleUrls: ['./secure-text-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SecureTextInputComponent),
      multi: true,
    },
  ],
})
export class SecureTextInputComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @Input() id = Math.random();
  @Input() placeholder = '';
  @Input() disabled: boolean;
  @Output() blur = new EventEmitter();

  control = new UntypedFormControl('');
  onTouched = noop;
  visible = true;
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.control.valueChanges.pipe(untilDestroyed(this)).subscribe(value => this.propagateChange(value));
  }

  ngOnDestroy() {}

  writeValue(value = '') {
    this.control.setValue(value, { emitEvent: false });
    this.visible = !value;
    this.setDisabledState(!!value);

    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = () => {
      fn();
      this.blur.emit();
    };
  }

  setDisabledState(isDisabled: boolean = false): void {
    const command = isDisabled ? 'disable' : 'enable';
    this.control[command]({ emitEvent: false });
  }

  onToggleVisibility() {
    if (!this.control.value) {
      return;
    }

    this.visible = !this.visible;
    const isDisabled = this.disabled || !this.visible;
    this.setDisabledState(isDisabled);
  }
}
