import { NgModule } from '@angular/core';
import { SecureTextInputComponent } from './secure-text-input.component';
import { SharedCommonModule } from '@certemy/common';

@NgModule({
  imports: [SharedCommonModule],
  declarations: [SecureTextInputComponent],
  exports: [SecureTextInputComponent],
})
export class SecureTextInputModule {}
