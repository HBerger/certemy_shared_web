import { Component, ChangeDetectionStrategy, forwardRef, ChangeDetectorRef, OnDestroy, OnInit, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { noop } from '@certemy/common';
import { NgbTimeAdapter, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgbTimeStringAdapter } from './timepicker.service';

@UntilDestroy()
@Component({
  selector: 'cm-timepicker',
  templateUrl: './timepicker.component.html',
  styleUrls: ['./timepicker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TimePickerComponent),
      multi: true,
    },
    { provide: NgbTimeAdapter, useClass: NgbTimeStringAdapter }
  ],
})
export class TimePickerComponent implements ControlValueAccessor, OnDestroy, OnInit {
  private propagateChange = noop;
  control = new UntypedFormControl({} as NgbTimeStruct);
  onTouched = noop;
  @Input() meridian = true;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.control.valueChanges.pipe(untilDestroyed(this)).subscribe(value => this.propagateChange(value));
  }

  ngOnDestroy() {}

  writeValue(value = null) {
    this.control.setValue(value, { emitEvent: false });
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean = false): void {
    const command = isDisabled ? 'disable' : 'enable';
    this.control[command]({ emitEvent: false });
    this.changeDetector.markForCheck();
  }
}
