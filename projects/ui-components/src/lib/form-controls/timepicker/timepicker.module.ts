import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TimePickerComponent } from './timepicker.component';
import { NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgbTimepickerModule],
  declarations: [TimePickerComponent],
  exports: [TimePickerComponent],
})
export class TimePickerModule {}
