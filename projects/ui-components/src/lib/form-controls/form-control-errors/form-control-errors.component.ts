import { Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'cm-form-control-errors',
  templateUrl: './form-control-errors.component.html',
  styleUrls: ['./form-control-errors.component.scss'],
})
export class FormControlErrorsComponent {
  @Input() control: UntypedFormControl;

  getError(errors) {
    return errors ? Object.values(errors)[0] : null;
  }
}
