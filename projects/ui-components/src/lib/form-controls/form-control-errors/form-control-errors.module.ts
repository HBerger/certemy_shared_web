import { NgModule } from '@angular/core';

import { FormControlErrorsComponent } from './form-control-errors.component';
import { CommonModule } from '@angular/common';
import { SharedCommonModule } from '@certemy/common';

@NgModule({
  imports: [CommonModule, SharedCommonModule],
  exports: [FormControlErrorsComponent],
  declarations: [FormControlErrorsComponent],
})
export class FormControlErrorsModule {}
