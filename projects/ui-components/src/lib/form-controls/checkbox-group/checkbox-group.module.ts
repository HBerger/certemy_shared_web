import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CheckboxModule } from '../checkbox/checkbox.module';
import { CheckboxGroupComponent } from './checkbox-group.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, CheckboxModule],
  exports: [CheckboxGroupComponent],
  declarations: [CheckboxGroupComponent],
})
export class CheckboxGroupModule {}
