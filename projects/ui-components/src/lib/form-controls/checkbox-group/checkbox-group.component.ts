import { ChangeDetectorRef, Component, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  UntypedFormBuilder,
  UntypedFormGroup,
  NG_VALIDATORS,
  UntypedFormControl,
} from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ComputedFrom, noop } from '@certemy/common';

@Component({
  selector: 'cm-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxGroupComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CheckboxGroupComponent),
      multi: true,
    },
  ],
})
export class CheckboxGroupComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @Input() options = [];
  @Input() required = false;

  @ComputedFrom('options')
  get defaultCheckboxValue() {
    return this.options.reduce((optionGroup, option) => {
      optionGroup[option.id] = false;
      return optionGroup;
    }, {});
  }

  @Input()
  set disabled(isDisabled: boolean) {
    if (!!this.checkboxGroup) {
      this.setDisabledState(isDisabled);
    }
    this.isDisabled = isDisabled;
  }

  isDisabled = false;

  checkboxGroup: UntypedFormGroup;
  onTouched = noop;

  private onDestroy$ = new Subject<void>();
  private get hasValue() {
    return Object.values(this.checkboxGroup.value).some((value: boolean) => value);
  }
  private propagateChange = noop;

  constructor(private fb: UntypedFormBuilder, private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.createCheckboxGroup();
    this.setDisabledState(this.isDisabled);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.validate = () => null;
  }

  public writeValue(value) {
    if (value) {
      this.checkboxGroup.setValue(value, { emitEvent: false });
      // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    } else {
      this.checkboxGroup.patchValue(this.defaultCheckboxValue, { emitEvent: true });
    }
    this.changeDetector.markForCheck();
  }

  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  validate(c: UntypedFormControl) {
    return this.required && !this.hasValue ? { required: true } : null;
  }

  setDisabledState(disable: boolean): void {
    if (disable) {
      this.checkboxGroup.disable({ emitEvent: false });
    } else {
      this.checkboxGroup.enable({ emitEvent: false });
    }
  }

  private createCheckboxGroup() {
    this.checkboxGroup = this.fb.group(this.defaultCheckboxValue);
    this.propagateChange(this.checkboxGroup.value);
    this.checkboxGroup.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(value => this.propagateChange(value));
  }
}
