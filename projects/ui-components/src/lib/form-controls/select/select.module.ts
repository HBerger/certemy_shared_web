import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { SelectComponent } from './select.component';
import { SearchInputModule } from '../search-input';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, NgbDropdownModule, SearchInputModule, ScrollingModule, NgbTooltipModule],
  exports: [SelectComponent, ScrollingModule],
  declarations: [SelectComponent],
})
export class SelectModule {}
