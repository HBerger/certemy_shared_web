import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SmartForm } from '../../smart-form/smart-form.component';
import { noop } from '@certemy/common';
import { NgbDropdown } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'cm-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectComponent implements ControlValueAccessor, OnChanges {
  @Input() id;
  @Input()
  set options(value: SmartForm.FieldOption[]) {
    this.search = '';
    this.searchedOptions = [];
    this._options = value;
  }
  @Input() placeholder = 'Please select';
  @Input() disabled: boolean;
  @Input() ignoreSameValue = false;
  @Input() display: 'dynamic' | 'static' = 'dynamic';
  @Input() container: null | 'body' = null;
  @Input() opened: boolean = false;
  @Input() autoClose: boolean = true;
  @Input() defaultSearch = false;
  @Input() defaultValue = '';
  @Output() changeSearchValue = new EventEmitter<string>();
  @ViewChild('dropDown') dropDown: NgbDropdown;
  searchedOptions: SmartForm.FieldOption[] = [];
  search = '';
  _options: SmartForm.FieldOption[] = [];

  onTouched = noop;
  selectedOption: SmartForm.FieldOption;

  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges) {
    const {options} = changes;

    if (options && options.previousValue) {
      if (this.selectedOption && !this.defaultValue) {
        this.writeValue(this.selectedOption.id as string);
      }

      if (!this.selectedOption && this.defaultValue) {
        this.writeValue(this.defaultValue);
      }
    }
  }

  writeValue(value = '') {
    this.selectedOption = this._options.find(option => option.id === value);
    this.changeDetector.markForCheck();
  }

  onSearch(search = '') {
    this.search = search.trim();
    this.searchedOptions = this._options.filter(option => option.name.toLowerCase().includes(search.toLowerCase()));
    this.changeSearchValue.emit(search);
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  trackbyFn(_, field: SmartForm.FieldOption) {
    return field.id;
  }

  setDisabledState(isDisabled: boolean = false): void {
    this.disabled = isDisabled;
    this.changeDetector.markForCheck();
  }

  selectValue(option: SmartForm.FieldOption) {
    if (!option.disabled && (!this.ignoreSameValue || this.selectedOption !== option)) {
      this.selectedOption = option;
      this.propagateChange(option.id);
    }

    this.dropDown.toggle();
    this.onTouched();
  }

  toggle(event) {
    event.stopPropagation();

    if (!this.disabled) {
      this.dropDown.toggle();
    }
  }
}
