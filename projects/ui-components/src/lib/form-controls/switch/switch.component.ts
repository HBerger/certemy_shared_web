import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'cm-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SwitchComponent),
      multi: true,
    },
  ],
})
export class SwitchComponent implements ControlValueAccessor, Validator {
  value = false;
  isDisabled = false;
  @Input() required: false;
  @Input()
  set disabled(isDisabled: boolean) {
    this.isDisabled = isDisabled;
  }

  private propagateChange = (_: any) => {};

  writeValue(value) {
    this.value = value;
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  toggle() {
    if (this.isDisabled) {
      return;
    }

    this.value = !this.value;
    this.propagateChange(this.value);
  }

  registerOnTouched() {}

  validate(c: UntypedFormControl) {
    const err = {
      required: true,
    };

    return this.required && !this.value ? err : null;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
