import { Component, Output, EventEmitter, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'cm-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit, OnDestroy {
  @Input() value = '';
  @Input() minSearchLength = 3;
  @Input() isCancelButtonVisible = true;
  @Output() valueDebouncedChange: EventEmitter<string> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() valueChange = new EventEmitter<string>();

  private termChange$ = new Subject<string>();
  private onDestroy$ = new Subject<void>();

  ngOnInit() {
    this.termChange$
      .pipe(
        debounceTime(500),
        filter(term => this.isValidTerm(term)),
        distinctUntilChanged(),
        takeUntil(this.onDestroy$),
      )
      .subscribe(searchTerm => this.valueDebouncedChange.emit(searchTerm));
    this.termChange$
      .pipe(
        filter(term => this.isValidTerm(term)),
        takeUntil(this.onDestroy$),
      )
      .subscribe(term => this.valueChange.emit(term));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  cancelEmitter() {
    this.updateSearch('');
    this.cancel.emit('cancel');
  }

  updateSearch(term: string) {
    if (term !== this.value) {
      this.value = term;
      this.termChange$.next(term);
    }
  }

  private isValidTerm(term: string): boolean {
    return term.length === 0 || term.length >= this.minSearchLength;
  }
}
