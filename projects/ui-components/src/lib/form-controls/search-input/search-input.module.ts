import { NgModule } from '@angular/core';
import { SearchInputComponent } from './search-input.component';
import { SharedCommonModule } from '@certemy/common';

@NgModule({
  imports: [SharedCommonModule],
  declarations: [SearchInputComponent],
  exports: [SearchInputComponent],
})
export class SearchInputModule {}
