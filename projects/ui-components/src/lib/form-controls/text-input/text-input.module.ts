import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextInputComponent } from './text-input.component';
import { ReactiveFormsModule } from '@angular/forms';

const components = [TextInputComponent];

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  declarations: components,
  exports: components,
  providers: [],
})
export class TextInputModule {}
