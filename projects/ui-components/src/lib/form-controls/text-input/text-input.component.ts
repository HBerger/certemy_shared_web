import {
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
  Output,
  EventEmitter,
} from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs';
import { SmartForm } from '../../smart-form/smart-form.component';
import { takeUntil } from 'rxjs/operators';
import { noop } from '@certemy/common';

@Component({
  selector: 'cm-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextInputComponent),
      multi: true,
    },
  ],
})
export class TextInputComponent implements ControlValueAccessor, OnInit, OnDestroy {
  @Input() id = Math.random();
  @Input() options: SmartForm.FieldOption[] = [];
  @Input() placeholder = '';
  @Input() autocomplete = true;
  @Input() maxlength = '';
  @Input() readonly = false;
  @Output() blur = new EventEmitter();

  control = new UntypedFormControl('');
  onTouched = noop;

  private onDestroy$ = new Subject<void>();
  private propagateChange = noop;

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.control.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(value => this.propagateChange(value));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  writeValue(value = '') {
    this.control.setValue(value, { emitEvent: false });
    // NgModel works incorrectly when using onPush https://github.com/angular/angular/issues/10816
    this.changeDetector.markForCheck();
  }

  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => any): void {
    this.onTouched = () => {
      fn();
      this.blur.emit();
    };
  }

  setDisabledState(isDisabled: boolean = false): void {
    const command = isDisabled ? 'disable' : 'enable';
    this.control[command]({ emitEvent: false });
  }
}
