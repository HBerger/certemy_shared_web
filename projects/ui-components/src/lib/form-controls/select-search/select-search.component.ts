import { ChangeDetectionStrategy, Component, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { SelectSearchConfig } from './select-search.interface';
import { defaultConfig } from './select-search.constants';
import { SmartForm } from '@certemy/ui-components';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'cm-select-search',
  templateUrl: './select-search.component.html',
  styleUrls: ['./select-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectSearchComponent {
  @Input() disabled = false;
  @Input() minSearchLength = 2;
  @Input() selectedOption: SmartForm.FieldOption;
  @Input() config: SelectSearchConfig = defaultConfig;
  @Input() options: SmartForm.FieldOption[] = [];
  @Output() search = new EventEmitter<string>();
  @Output() optionSelect = new EventEmitter<{ option: SmartForm.FieldOption; dropdownRef: NgbDropdown }>();

  onOptionSelect(option: SmartForm.FieldOption, dropdownRef: NgbDropdown) {
    this.optionSelect.emit({ option, dropdownRef });
  }

  onSearch(search: string) {
    this.search.emit(search);
  }
}
