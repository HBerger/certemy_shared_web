import { NgModule } from '@angular/core';
import { SharedCommonModule } from '@certemy/common';
import { SelectSearchComponent } from './select-search.component';
import { SearchInputModule } from '../search-input/search-input.module';
import { CustomDropdownModule } from '../custom-dropdown';

@NgModule({
  imports: [SharedCommonModule, CustomDropdownModule, SearchInputModule],
  exports: [SelectSearchComponent],
  declarations: [SelectSearchComponent],
})
export class SelectSearchModule {}
