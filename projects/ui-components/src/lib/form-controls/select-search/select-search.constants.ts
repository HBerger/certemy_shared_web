import { SelectSearchConfig } from './select-search.interface';

export const defaultConfig: SelectSearchConfig = {
  noEntries: 'No record found',
};
