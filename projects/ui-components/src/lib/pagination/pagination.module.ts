import { NgModule } from '@angular/core';

import { SharedCommonModule } from '@certemy/common';
import { PaginationComponent } from './pagination.component';
import { PaginationDisplayedComponent } from './pagination-displayed.component';
import { PaginationPageSizeSelectorComponent } from './page-size-selector/page-size-selector.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

const components = [PaginationComponent, PaginationPageSizeSelectorComponent, PaginationDisplayedComponent];

@NgModule({
  imports: [SharedCommonModule, NgbPaginationModule],
  exports: components,
  declarations: components,
  providers: [],
})
export class PaginationModule {}
