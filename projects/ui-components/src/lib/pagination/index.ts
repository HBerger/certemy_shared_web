export { PaginationModule } from './pagination.module';
export { PaginationComponent } from './pagination.component';
export { PaginationDisplayedComponent } from './pagination-displayed.component';
export { PaginationPageSizeSelectorComponent } from './page-size-selector/page-size-selector.component';
export { DEFAULT_PAGE_SIZE_ITEMS, PAGE_SIZE_ITEMS_TOKEN } from './page-size-selector/page-size-selector.interface';
