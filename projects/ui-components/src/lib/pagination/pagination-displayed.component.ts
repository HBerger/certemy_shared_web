import { Component, Input } from '@angular/core';

@Component({
  selector: 'cm-pagination-displayed',
  templateUrl: './pagination-displayed.component.html',
  styleUrls: ['./pagination-displayed.component.scss'],
})
export class PaginationDisplayedComponent {
  @Input() page = 1;
  @Input() collectionSize = 0;
  @Input() pageSize = 10;

  getCount(page, pageSize, count) {
    const numberOfPages = Math.ceil(count / pageSize);
    const from = (page - 1) * pageSize + 1;
    const to = page < numberOfPages ? page * pageSize : count;

    return count ? `Displayed ${from}-${to} of ${count}` : '';
  }
}
