import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cm-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent {
  @Input() page = 1;
  @Input() collectionSize = 0;
  @Input() maxSize = 4;
  @Input() pageSize = 10;
  @Output() pageChange = new EventEmitter<number>();
}
