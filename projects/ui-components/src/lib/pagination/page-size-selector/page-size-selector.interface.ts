import { InjectionToken } from '@angular/core';

export const DEFAULT_PAGE_SIZE_ITEMS = [100, 50, 20, 10];

export const PAGE_SIZE_ITEMS_TOKEN = new InjectionToken<number[]>('PAGE_SIZE_ITEMS_TOKEN');
