import { Component, EventEmitter, Inject, Input, Optional, Output } from '@angular/core';
import { DEFAULT_PAGE_SIZE_ITEMS, PAGE_SIZE_ITEMS_TOKEN } from './page-size-selector.interface';

@Component({
  selector: 'cm-pagination-page-size-selector',
  templateUrl: './page-size-selector.component.html',
  styleUrls: ['./page-size-selector.component.scss'],
})
export class PaginationPageSizeSelectorComponent {
  @Input() pageSize = 10;
  @Input() collectionSize = 0;
  @Output() pageSizeChange = new EventEmitter<number>();

  constructor(
    @Optional()
    @Inject(PAGE_SIZE_ITEMS_TOKEN)
    public items: number[],
  ) {
    if (!items) {
      this.items = DEFAULT_PAGE_SIZE_ITEMS;
    }
  }
}
