import { Injectable, TemplateRef } from '@angular/core';
import { Message, MESSAGE_TYPES } from './interfaces/interfaces';
import { ReplaySubject } from 'rxjs';
import { Observable } from 'rxjs';

@Injectable()
export class MessageQueryService {
  private _messageAdded: ReplaySubject<Message> = new ReplaySubject();
  private _messageRemoved: ReplaySubject<Message> = new ReplaySubject();
  private _messagesErased: ReplaySubject<void> = new ReplaySubject();
  messageAdded: Observable<Message> = this._messageAdded.asObservable();
  messageRemoved: Observable<Message> = this._messageRemoved.asObservable();
  messagesErased: Observable<void> = this._messagesErased.asObservable();
  readonly timeout = 3000;
  private messages: Array<Message> = [];

  addMessage(text: string, timeout?: number) {
    this._addMessage(MESSAGE_TYPES.INFO, { text }, timeout || this.timeout);
  }

  addErrorMessage(text: string, timeout?: number) {
    this._addMessage(MESSAGE_TYPES.ERROR, { text }, timeout);
  }

  addCustomMessage(message: any, template: TemplateRef<any>, timeout?: number) {
    this._addMessage(MESSAGE_TYPES.CUSTOM, message, timeout, template);
  }

  getMessages() {
    return this.messages;
  }

  eraseQuery() {
    this.messages = [];
    this._messagesErased.next(null);
  }

  removeMessage(message) {
    this.messages = this.messages.filter(m => m.id !== message.id);
    this._messageRemoved.next(message);
    return message;
  }

  private _addMessage(type: MESSAGE_TYPES, context: any, timeout?: number, template?: TemplateRef<any>) {
    const message: Message = {
      timeout: timeout,
      id: this.generateGuid(),
      type: type,
      context: context,
      template: template,
    };

    this.messages.unshift(message);

    if (timeout) {
      setTimeout(() => {
        const mes: Message = this.messages.find(m => m.id === message.id);

        if (mes) {
          this.removeMessage(mes);
        }
      }, timeout);
    }

    this._messageAdded.next(message);
  }

  private generateGuid() {
    return Math.floor(Date.now() + Math.random() * 100);
  }
}
