import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Message, MESSAGE_TYPES } from '../interfaces/interfaces';
import { MessageQueryService } from '../message-query.service';

@Component({
  selector: 'cm-message-query',
  templateUrl: './message-query.component.html',
  styleUrls: ['./message-query.component.scss'],
})
export class MessageQueryComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  public TYPES = MESSAGE_TYPES;
  public messages: Array<Message>;

  constructor(private messageQueryService: MessageQueryService) {}

  ngOnInit() {
    this.messages = [];
    this.messageQueryService.messageAdded
      .pipe(takeUntil(this.onDestroy$.asObservable()))
      .subscribe((message: Message) => this.messages.push(message));
    this.messageQueryService.messagesErased
      .pipe(takeUntil(this.onDestroy$.asObservable()))
      .subscribe(() => (this.messages = []));
    this.messageQueryService.messageRemoved
      .pipe(takeUntil(this.onDestroy$.asObservable()))
      .subscribe((message: Message) => {
        const messageElement = document.getElementById(message.id.toString());
        if (messageElement) {
          messageElement.attributes.item(1).value += ' message-removed';
          setTimeout(() => {
            this.messages = this.messages.filter(m => m.id !== message.id);
          }, 1000);
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  removeMessage(message) {
    this.messageQueryService.removeMessage(message);
  }
}
