import { TemplateRef } from '@angular/core';

export interface SimpleMessage {
  id: number;
  timeout?: number;
  type: MESSAGE_TYPES;
  context: {
    text: string;
  };
}

export interface CustomMessage {
  id: number;
  timeout?: number;
  type: MESSAGE_TYPES;
  context: any;
  template: TemplateRef<any>;
}

export type Message = SimpleMessage | CustomMessage;

export enum MESSAGE_TYPES {
  INFO,
  ERROR,
  CUSTOM,
}
