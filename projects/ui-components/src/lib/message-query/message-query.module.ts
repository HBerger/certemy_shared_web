import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageQueryComponent } from './message-query/message-query.component';
import { MessageQueryService } from './message-query.service';

@NgModule({
  imports: [CommonModule],
  declarations: [MessageQueryComponent],
  exports: [MessageQueryComponent],
})
export class MessageQueryModule {
  public static forRoot(): ModuleWithProviders<MessageQueryModule> {
    return {
      ngModule: MessageQueryModule,
      providers: [MessageQueryService],
    };
  }
}
