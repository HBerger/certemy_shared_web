import { Component, Input, ElementRef, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ANIMATION_STYLE_START, ANIMATION_STYLE_END } from './toggle.constants';

@Component({
  selector: 'cm-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  animations: [
    trigger('toggleAnim', [
      state('void', style(ANIMATION_STYLE_END)),
      transition(':enter', animate('300ms ease-in-out', style(ANIMATION_STYLE_START))),
      transition(':leave', animate('300ms ease-in-out', style(ANIMATION_STYLE_END))),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToggleComponent {
  @Input() isDescriptionVisible: boolean = false;

  @Input() visibleElem: ElementRef;
  @Input() hiddenElem: ElementRef;
  @Output() onChangeVisibility = new EventEmitter<boolean>();

  constructor() {}

  changeDescriptionVisibility() {
    this.isDescriptionVisible = !this.isDescriptionVisible;
    this.onChangeVisibility.emit(this.isDescriptionVisible);
  }
}
