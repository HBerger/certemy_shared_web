import { Component, Input, OnInit } from '@angular/core';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'cm-popover-layout',
  templateUrl: './popover-layout.component.html',
  styleUrls: ['./popover-layout.component.scss'],
})
export class PopoverLayoutComponent implements OnInit {
  @Input() popover: NgbPopover;

  ngOnInit() {
    if (!this.popover) {
      throw new Error('popover input is required');
    }
  }
}
