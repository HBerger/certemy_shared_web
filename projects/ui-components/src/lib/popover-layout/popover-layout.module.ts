import { NgModule } from '@angular/core';

import { PopoverLayoutComponent } from './popover-layout.component';
import { SharedCommonModule } from '@certemy/common';
import { NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { IconModule } from '../icon/index';

@NgModule({
  imports: [SharedCommonModule, NgbPopoverModule, IconModule],
  exports: [PopoverLayoutComponent],
  declarations: [PopoverLayoutComponent],
})
export class PopoverLayoutModule {}
