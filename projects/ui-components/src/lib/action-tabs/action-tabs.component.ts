import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

export interface ActionTab {
  id: number;
  display: string;
  tooltip?: string;
  disabled?: boolean;
}

@Component({
  selector: 'cm-action-tabs',
  templateUrl: './action-tabs.component.html',
  styleUrls: ['./action-tabs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionTabsComponent {
  @Input() tabs: ActionTab[] = [];
  @Input() disabled = false;
  @Input() activeTabId: number;
  @Output() tabsChange = new EventEmitter<number>();

  onTabsChange(tabId: number) {
    this.tabsChange.emit(tabId);
  }
}
