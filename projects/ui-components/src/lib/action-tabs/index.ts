import { ActionTab as ActionTabInterface } from './action-tabs.component';
export * from './action-tabs.component';
export * from './action-tabs.module';
export type ActionTab = ActionTabInterface;
