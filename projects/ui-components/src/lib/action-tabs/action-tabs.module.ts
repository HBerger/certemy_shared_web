import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedCommonModule } from '@certemy/common';
import { ActionTabsComponent } from './action-tabs.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [SharedCommonModule, RouterModule, NgbTooltipModule],
  exports: [ActionTabsComponent],
  declarations: [ActionTabsComponent],
  providers: [],
})
export class ActionTabsModule {}
