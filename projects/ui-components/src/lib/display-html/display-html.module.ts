import { NgModule } from '@angular/core';
import { DisplayHtmlComponent } from './display-html.component';
import { SharedCommonModule } from '@certemy/common';

@NgModule({
  imports: [SharedCommonModule],
  declarations: [DisplayHtmlComponent],
  exports: [DisplayHtmlComponent],
})
export class DisplayHtmlModule {}
