import { Component, Input } from '@angular/core';

@Component({
  selector: 'cm-display-html',
  templateUrl: './display-html.component.html',
  styleUrls: ['./display-html.component.scss'],
})
export class DisplayHtmlComponent {
  @Input() content = '';
  @Input() markAsTrusted = false;
}
