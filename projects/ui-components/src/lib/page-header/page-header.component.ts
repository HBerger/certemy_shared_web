import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'cm-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss'],
})
export class PageHeaderComponent {
  @Input() text = '';
  @Input() subtext = '';
  @Input() backUrl: string;
  @Input() hideBackUrl = false;
  @Input() hideBackButton = true;
  @Input() queryParamsHandling: 'merge' | 'preserve' | '' = '';
  @Output() onBack: EventEmitter<void> = new EventEmitter();
  public defaultUrl = '../';
}
