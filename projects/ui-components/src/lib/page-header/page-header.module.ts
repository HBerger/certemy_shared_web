import { NgModule } from '@angular/core';

import { PageHeaderComponent } from './page-header.component';
import { SharedCommonModule } from '@certemy/common';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [SharedCommonModule, RouterModule],
  exports: [PageHeaderComponent],
  declarations: [PageHeaderComponent],
  providers: [],
})
export class PageHeaderModule {}
