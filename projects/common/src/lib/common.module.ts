import { NgModule } from '@angular/core';
import { PipesModule } from './pipes/index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DirectivesModule } from './directives/index';

@NgModule({
  imports: [],
  declarations: [],
  providers: [],
  exports: [PipesModule, CommonModule, FormsModule, ReactiveFormsModule, RouterModule, DirectivesModule],
})
export class SharedCommonModule {}
