export enum ResponseStatus {
  INITIAL = 'INITIAL',
  PENDING = 'PENDING',
  COMPLETE = 'COMPLETE',
  ERROR = 'ERROR',
}

export enum FileTypes {
  XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
}

export const CSS_VARIABLES_NAMES = {
  FONT_BASE: '--baseFont',
  FONT_HEADING: '--headingFont',
};
