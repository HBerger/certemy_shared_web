export enum DateFormats {
  defaultFormat = 'MM/DD/YYYY',
  dateTime = 'MM/DD/YYYY hh:mm A',
  time = 'hh:mm A',
  year = 'YYYY',
  dateWithMonthName = 'MMMM DD, YYYY',
}
