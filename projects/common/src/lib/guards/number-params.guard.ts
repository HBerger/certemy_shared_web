import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';
import { of } from 'rxjs';
import { Observable } from 'rxjs';

export const NumberParamGuard: CanActivateFn = (
  activatedRouteSnapshot: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
): Observable<boolean> => {
  const router = inject(Router);
  const allNumbers = activatedRouteSnapshot.data['numberParams']
    .map(paramName => activatedRouteSnapshot.params[paramName])
    .every(param => !isNaN(param));

  if (!allNumbers) {
    router.navigate(['/not-found']);
  }

  return of(allNumbers);
};
