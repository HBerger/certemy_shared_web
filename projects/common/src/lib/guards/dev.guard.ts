import { inject, Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateFn } from '@angular/router';
import { Observable } from 'rxjs';
import { ENVIRONMENT } from '../environment.token';

@Injectable()
export class DevGuardService  {
  constructor(private router: Router, @Inject(ENVIRONMENT) private environment: any) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.environment.production) {
      return true;
    }

    this.router.navigate(['/main']);
    return false;
  }
}

export const DevGuard: CanActivateFn = (
  activatedRouteSnapshot: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
): Observable<boolean> | Promise<boolean> | boolean => {
  return inject(DevGuardService).canActivate(activatedRouteSnapshot, state);
};
