import { UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { DontBeEqualParams } from './custom.validator.interface';
import { URL_REGEX } from './custom.validator.constants';

function isEmptyInputValue(value: any): boolean {
  // we don't check for string here so it also works with arrays
  return value == null || value.length === 0;
}

const emailRegex = /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i;

export const email = (control: UntypedFormControl) => {
  if (isEmptyInputValue(control.value)) {
    return null; // don't validate empty values to allow optional controls
  }
  return emailRegex.test(control.value.trim()) ? null : { email: true };
};

export const isValidEmail = (emailVal: string) => {
  return emailRegex.test(emailVal);
};

export const equalTo = (equalControl: UntypedFormControl): ValidatorFn => {
  let subscribe = false;

  return (control: UntypedFormControl) => {
    if (!subscribe) {
      subscribe = true;
      equalControl.valueChanges.subscribe(() => {
        control.updateValueAndValidity();
      });
    }

    return equalControl.value === control.value ? null : { equalTo: true };
  };
};

export const patternValidator = (pattern, text = 'Pattern mismatch.'): ValidatorFn => {
  let regexp;
  if (typeof pattern === 'string') {
    regexp = new RegExp(pattern);
  } else if (pattern instanceof RegExp) {
    regexp = pattern;
  } else {
    throw new Error('Unknown pattern format.');
  }

  return (control: UntypedFormControl) => {
    const { value } = control;

    if (isEmptyInputValue(value)) {
      return null; // don't validate empty values to allow optional controls
    }

    return regexp.test(value) ? null : { patternMismatch: text };
  };
};

export const numberValidator = (text = 'Please provide valid number.'): ValidatorFn => {
  return (control: UntypedFormControl) => {
    const { value } = control;

    if (isEmptyInputValue(value)) {
      return null; // don't validate empty values to allow optional controls
    }

    return /^-?[0-9]*(\.[0-9]*)?$/.test(value) ? null : { invalidNumber: text };
  };
};

export const trimmingDot = (): ValidatorFn => {
  return (control: UntypedFormControl) => {
    const { value } = control;

    if (isEmptyInputValue(value)) {
      return null; // don't validate empty values to allow optional controls
    }

    return /\.+$/.test(value) ? { invalidNumber: true } : null;
  };
};

export const phoneLengthValidator = (digitLimit = 10): ValidatorFn => {
  return (control: UntypedFormControl) => {
    const { value } = control;

    if (isEmptyInputValue(value)) {
      return null;
    }

    return `${value}`.replace(/\D/g, '').length === digitLimit ? null : { phoneLength: true };
  };
};

export const oneDigitRule = (): ValidatorFn => {
  return (control: UntypedFormControl) => {
    const re = /\d/;

    if (!re.test(control.value)) {
      return { digitCharacterRule: true };
    }

    return null;
  };
};

export const lowercaseCharacterRule = (): ValidatorFn => {
  return (control: UntypedFormControl) => {
    const re = /[a-z]/;

    if (!re.test(control.value)) {
      return { lowercaseCharacterRule: true };
    }

    return null;
  };
};

export const uppercaseCharacterRule = (): ValidatorFn => {
  return (control: UntypedFormControl) => {
    const re = /[A-Z]/;

    if (!re.test(control.value)) {
      return { uppercaseCharacterRule: true };
    }

    return null;
  };
};

export const reduceTo = (propName: string, validators: ValidatorFn[]): ValidatorFn => {
  return control => (validators.some(validator => !!validator(control)) ? { [propName]: true } : null);
};

export const positiveNumber = (control: UntypedFormControl) => {
  const re = /^([1-9]\d*)$/;
  const hasValue = !!control.value || control.value === 0;

  if (hasValue && !re.test(control.value)) {
    return { positiveNumber: true };
  }

  return null;
};

export const phone = (control: UntypedFormControl) => {
  const reDigits = /^\d{7,11}$/;
  const reNonDigits = /\D/;
  const hasValue = control.value.toString().length > 0;
  const errorMessage = 'The phone number contains';

  if (hasValue && reNonDigits.test(control.value)) {
    return {
      phone: true,
      message: `${errorMessage} non-numeric symbols`,
    };
  } else if (hasValue && !reDigits.test(control.value)) {
    return {
      phone: true,
      message: `${errorMessage} less than 7 / more than 11 digits`,
    };
  } else {
    return null;
  }
};

export const required = (control: UntypedFormControl) => {
  const { value } = control;
  const requiredResponse = { required: true };

  switch (typeof value) {
    case 'string':
      return value.trim().length === 0 ? requiredResponse : null;
    default:
      return value == null || value.length === 0 ? requiredResponse : null;
  }
};

export const url = (control: UntypedFormControl) => {
  const { value } = control;
  const urlError = { url: true };
  return URL_REGEX.test(value) ? null : urlError;
};

function updateValidationMessage(error: ValidationErrors, message: any): ValidationErrors {
  return Object.entries(error)
    .map(([key, value]) => [key, message])
    .reduce((obj, [k, v]) => ({ ...obj, [k]: v }), {});
}

export const withMessage = (message: string, validator: ValidatorFn) => (control: UntypedFormControl) => {
  const validationError: ValidationErrors = validator(control);
  return validationError ? updateValidationMessage(validationError, message) : validationError;
};

const getMinLengthError = (fieldName: string, fieldLength: number): string =>
  `${fieldName} should contain ${fieldLength} or more characters`;

const getMaxLengthError = (fieldName: string, fieldLength: number): string =>
  `${fieldName} should contain ${fieldLength} or less characters`;

export const rangeLength = (fieldName: string, minSize: number, maxSize: number): ValidatorFn => {
  return Validators.compose([
    withMessage(getMinLengthError(fieldName, minSize), Validators.minLength(minSize)),
    withMessage(getMaxLengthError(fieldName, maxSize), Validators.maxLength(maxSize)),
  ]);
};

export const textRangeLength = (fieldName: string) => rangeLength(fieldName, 2, 50);

export const isExist = (dict: Object) => (control: UntypedFormControl) => {
  const { value } = control;

  if (isEmptyInputValue(value)) {
    return null;
  }
  return value in dict ? { isExist: true } : null;
};

export const dontBeEqual = ({ definedValues, errorType }: DontBeEqualParams): ValidatorFn => {
  return (control: UntypedFormControl) => {
    if (definedValues.includes(control.value)) {
      return { [errorType]: true };
    }

    return null;
  };
};

export const dontContainEqual = (ids: string, id: string): ValidatorFn => {
  return (formGroup: UntypedFormGroup) => {
    const idsControl = formGroup.controls[ids];
    const idControl = formGroup.controls[id];
    const isIdsControlValue = idsControl && idsControl.value;

    if (idControl && isIdsControlValue && idsControl.value.includes(idControl.value)) {
      idsControl.setErrors({ containEqual: true });
      return { containEqual: true };
    }
    return null;
  };
};

export const multiSelectRequired = (control: UntypedFormControl) => {
  return !Object.values(control.value || {}).some((value: boolean) => value) ? { required: true } : null;
};

export const hoursValidator = ({ label = 'hours', number_required, number_completed = 0, fraction = 2 }) => {
  const text = `The ${label} submitted should not exceed ${label} required`;
  return (control: UntypedFormControl) => {
    const { value } = control;
    if (!value) {
      return null;
    }
    if (number_required && +(number_required - number_completed).toFixed(fraction) < value) {
      return { invalidHours: text };
    }
    return null;
  };
};
