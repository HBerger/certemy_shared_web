export interface DontBeEqualParams {
  definedValues: any[];
  errorType: string;
}
