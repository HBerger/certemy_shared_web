function shallowEq(obj1, obj2) {
  return Object.keys(obj1).every(el => obj1[el] === obj2[el]);
}

const PRIVATE_NAMESPACE: symbol = Symbol('ComputedFrom_PRIVATE_NAMESPACE');

interface ComponentInstance {
  // As it impossible to specify key for Symbol
  PRIVATE_NAMESPACE: PrivateNamespace;
}

interface AttributesMap {
  [key: string]: any;
}

class Instance {
  prevAttributesMap: AttributesMap = {};
  prevResult: any;
  isFirstcall = true;
}

class PrivateNamespace {
  instances: { [key: string]: Instance } = {};
}

function createSelectorOn(component: ComponentInstance, prop: string) {
  function getOrCreateNamespace(): PrivateNamespace {
    if (!(PRIVATE_NAMESPACE in component) || !component[PRIVATE_NAMESPACE]) {
      component[PRIVATE_NAMESPACE] = new PrivateNamespace();
    }
    return component[PRIVATE_NAMESPACE];
  }

  function getOrCreateInstance(): Instance {
    const instances = getOrCreateNamespace().instances;
    if (!(prop in instances)) {
      instances[prop] = new Instance();
    }
    return instances[prop];
  }

  return {
    getOrCreateNamespace,
    getOrCreateInstance,
  };
}

function getNewAttributesMap(component: ComponentInstance, attributes: string[]): AttributesMap {
  return attributes.reduce((acc, attr) => ({ ...acc, [attr]: component[attr] }), {});
}

function getMissedAttributes(component: ComponentInstance, attributes: string[]): string[] {
  return attributes.filter(attr => !(attr in component));
}

// can be used as decorator to cache getter calculated from instance attributes
export function ComputedFrom(...attributes: string[]) {
  return function(target: any, propertyKey: string, descriptor: PropertyDescriptor): PropertyDescriptor {
    const getterFn = descriptor.get;
    if (typeof getterFn !== 'function') {
      throw new Error(
        `@ComputedFrom decorator in class: ${target.constructor.name} on: ${name} - can only be applied to getters.`,
      );
    }
    return {
      get() {
        const componentInstance: ComponentInstance = this;
        const selector = createSelectorOn(componentInstance, propertyKey);

        if (!attributes.length) {
          return getterFn.call(componentInstance);
        }

        if (selector.getOrCreateInstance().isFirstcall) {
          selector.getOrCreateInstance().isFirstcall = false;
          const missedAttributes = getMissedAttributes(componentInstance, attributes);
          if (missedAttributes.length) {
            throw new Error(
              `@ComputedFrom decorator in class: ${
                target.constructor.name
              } on: ${name} - have missed attributes: ${missedAttributes.join(' ,')}.`,
            );
          }
        }

        const newAttributesMap = getNewAttributesMap(componentInstance, attributes);
        const prevAttributesMap = selector.getOrCreateInstance().prevAttributesMap;

        if (shallowEq(newAttributesMap, prevAttributesMap)) {
          return selector.getOrCreateInstance().prevResult;
        } else {
          const newResult = getterFn.call(this);
          selector.getOrCreateInstance().prevAttributesMap = newAttributesMap;
          selector.getOrCreateInstance().prevResult = newResult;
          return newResult;
        }
      },
    };
  };
}
