import { ComputedFrom } from './computed-from';
import { noop } from './helpers';

function descriptorSpyFactory() {
  const descriptor = { get: noop };
  const getSpy = spyOn(descriptor, 'get').and.callThrough();
  return {
    get: descriptor.get,
    getSpy,
  };
}

function prototypeStubFactory() {
  return { constructor: { name: null } };
}

class MockComponent {
  attr1 = 999;

  @ComputedFrom('attr1')
  get getter1() {
    return this.attr1;
  }

  @ComputedFrom()
  get freeGetter() {
    return 42;
  }
}

describe('ComputedFrom', () => {
  afterEach(() => {
    const component = new MockComponent();
    if (component.getter1 !== 999 || component.freeGetter !== 42) {
      throw new Error('No test should change initial value');
    }
  });

  it('Should work without cachedOnFields', () => {
    const component = new MockComponent();
    expect(component.freeGetter).toEqual(42);
  });

  it('Should call getter with correct context', () => {
    const descriptorStub = {
      get() {
        return this;
      },
    };
    const prototypeStub = prototypeStubFactory();
    const newDescriptor = ComputedFrom('attr')(prototypeStub, 'somePropertyKey', descriptorStub);
    const context = { attr: null };
    expect(newDescriptor.get.call(context)).toBe(context);
  });

  it('Should throw exception once if called with unknown cachedOnFields', () => {
    const descriptorStub = descriptorSpyFactory();
    const prototypeStub = prototypeStubFactory();
    const newDescriptor = ComputedFrom('missedKey')(prototypeStub, 'attr', descriptorStub);
    expect(() => newDescriptor.get()).toThrowError();
    expect(() => newDescriptor.get()).not.toThrowError();
  });

  it('Should throw exception if applied to non getter value', () => {
    const descriptorWithoutGetterStub = { value: null };
    const prototypeStub = prototypeStubFactory();
    expect(() => ComputedFrom('missedKey')(prototypeStub, 'attr', descriptorWithoutGetterStub)).toThrowError();
  });

  it('Should not target access descriptor before new descriptor was called', () => {
    const descriptorSpy = descriptorSpyFactory();
    const prototypeStub = prototypeStubFactory();
    ComputedFrom()(prototypeStub, 'cachedProps', descriptorSpy);

    expect(descriptorSpy.getSpy.calls.count()).toEqual(0);

    const descriptorSpy2 = descriptorSpyFactory();
    const prototypeStub2 = prototypeStubFactory();
    ComputedFrom('cachedProps')(prototypeStub2, 'attr', descriptorSpy2);

    expect(descriptorSpy2.getSpy.calls.count()).toEqual(0);
  });

  it('Should not memoize getter without cachedOnFields', () => {
    const descriptorSpy = descriptorSpyFactory();
    const prototypeStub = prototypeStubFactory();
    const newDescriptor = ComputedFrom()(prototypeStub, 'attr', descriptorSpy);

    newDescriptor.get.call({});
    expect(descriptorSpy.getSpy.calls.count()).toEqual(1);

    newDescriptor.get.call({});
    newDescriptor.get.call({});
    expect(descriptorSpy.getSpy.calls.count()).toEqual(3);
  });

  it('Should return only cached value if cachedOnFields the same', () => {
    function instanceFactory(val1, val2, val3) {
      return {
        cachedProps1: val1,
        cachedProps2: val2,
        cachedProps3: val3,
      };
    }

    const descriptorSpy = descriptorSpyFactory();
    const prototypeStub = prototypeStubFactory();
    const newDescriptor = ComputedFrom('cachedProps1', 'cachedProps2', 'cachedProps3')(
      prototypeStub,
      'attr',
      descriptorSpy,
    );

    const instance = instanceFactory(1, 11, 21);

    newDescriptor.get.call(instance);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(1);

    newDescriptor.get.call(instance);
    newDescriptor.get.call(instance);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(1);
  });

  it('Should compute value once if cachedOnFields the same', () => {
    function instanceFactory(val1, val2, val3) {
      return {
        cachedProps1: val1,
        cachedProps2: val2,
        cachedProps3: val3,
      };
    }

    const descriptorSpy = descriptorSpyFactory();
    const prototypeStub = prototypeStubFactory();
    const newDescriptor = ComputedFrom('cachedProps1', 'cachedProps2', 'cachedProps3')(
      prototypeStub,
      'attr',
      descriptorSpy,
    );

    const instance1 = instanceFactory(1, 11, 21);
    newDescriptor.get.call(instance1);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(1);

    const instance2 = instanceFactory(2, 11, 21);
    newDescriptor.get.call(instance2);
    newDescriptor.get.call(instance2);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(2);

    const instance3 = instanceFactory(2, 11, 22);
    newDescriptor.get.call(instance3);
    newDescriptor.get.call(instance3);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(3);
  });

  it('Should not depend on another ComputedFrom cache within the same instance (will also check dependence on another instance or class)', () => {
    function instanceFactory(val) {
      return { cachedProps: val };
    }

    const descriptorSpy = descriptorSpyFactory();
    const prototypeStub = prototypeStubFactory();
    const newDescriptor = ComputedFrom('cachedProps')(prototypeStub, 'attr', descriptorSpy);

    const instance = instanceFactory(1);
    newDescriptor.get.call(instance);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(1);

    function instanceFactory2(val) {
      return { cachedProps2: val };
    }

    const descriptorSpy2 = descriptorSpyFactory();
    const prototypeStub2 = prototypeStubFactory();
    const newDescriptor2 = ComputedFrom('cachedProps2')(prototypeStub2, 'attr', descriptorSpy2);

    const instance2 = instanceFactory2(1);
    newDescriptor2.get.call(instance2);
    expect(descriptorSpy.getSpy.calls.count()).toEqual(1);
    expect(descriptorSpy2.getSpy.calls.count()).toEqual(1);

    newDescriptor.get.call(instanceFactory(2));
    expect(descriptorSpy.getSpy.calls.count()).toEqual(2);
    expect(descriptorSpy2.getSpy.calls.count()).toEqual(1);

    newDescriptor2.get.call(instanceFactory2(2));
    expect(descriptorSpy.getSpy.calls.count()).toEqual(2);
    expect(descriptorSpy2.getSpy.calls.count()).toEqual(2);
  });
});
