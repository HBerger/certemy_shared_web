import _memoize from 'lodash-es/memoize';
import { share } from 'rxjs/operators';
import hash from 'object-hash';

export function ShareRequest(target, key, descriptor) {
  const originalMethod = descriptor.value;
  const getRequestObservableFn = function(...args) {
    return originalMethod.apply(this, args).pipe(share());
  };

  descriptor.value = _memoize(getRequestObservableFn, (...args) => hash(args));

  return descriptor;
}
