import { ChangeDetectorRef, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { Unsubscribable } from 'rxjs';

function noop(...args: any[]): any {}

function likeObservable(maybeObservable: Observable<any>): boolean {
  return !!maybeObservable && typeof maybeObservable.subscribe === 'function';
}

function invariant(isPassed: boolean, msg: string) {
  if (!isPassed) {
    throw new Error(msg);
  }
}

export function liftObservableType<T>(obs: Observable<T>): T {
  return (obs as any) as T;
}

const PRIVATE_NAMESPACE: symbol = Symbol('AutoSub_PRIVATE_NAMESPACE');
const ON_DESTROY_EXTENDED = Symbol('AutoSub_ON_DESTROY_EXTENDED');

interface ComponentInstance {
  injector: Injector;
  // As it impossible to specify key for Symbol
  PRIVATE_NAMESPACE: PrivateNamespace;
  ON_DESTROY_EXTENDED: boolean;
}

class Instance {
  value: any;
  subscription: Unsubscribable = { unsubscribe: noop };
}

class PrivateNamespace {
  instances: { [key: string]: Instance } = {};
  changeDetector: ChangeDetectorRef;

  constructor(instance: ComponentInstance) {
    const { injector } = instance;
    invariant(
      injector && typeof injector.get === 'function',
      `${instance.constructor.name} should provide public instance of Injector as injector.`,
    );
    this.changeDetector = injector.get(ChangeDetectorRef);
  }
}

function getOrCreateNamespace(component: ComponentInstance): PrivateNamespace {
  if (!(PRIVATE_NAMESPACE in component) || !component[PRIVATE_NAMESPACE]) {
    component[PRIVATE_NAMESPACE] = new PrivateNamespace(component);
  }
  return component[PRIVATE_NAMESPACE];
}

function getOrCreateInstance(component: ComponentInstance, key: string): Instance {
  const instances = getOrCreateNamespace(component).instances;
  if (!(key in instances)) {
    instances[key] = new Instance();
  }
  return instances[key];
}

function setValue(component: ComponentInstance, prop: string, value: any) {
  getOrCreateNamespace(component).changeDetector.markForCheck();
  getOrCreateInstance(component, prop).value = value;
}

function subscribe(component: ComponentInstance, prop: string, obs: Observable<any>) {
  getOrCreateInstance(component, prop).subscription = obs.subscribe(value => setValue(component, prop, value));
}

function unsubscribe(component: ComponentInstance, prop: string) {
  getOrCreateInstance(component, prop).subscription.unsubscribe();
}

function getValue(component: ComponentInstance, prop: string): any {
  return getOrCreateInstance(component, prop).value;
}

function unsubscribeAll(component: ComponentInstance) {
  const namespace = getOrCreateNamespace(component);
  Object.values(namespace.instances).forEach(instance => instance.subscription.unsubscribe());
}

export function AutoSub<T>(): any {
  return function(target: any, propertyKey: string): PropertyDescriptor {
    const proto = target.constructor.prototype;
    if (!(ON_DESTROY_EXTENDED in proto) || !proto[ON_DESTROY_EXTENDED]) {
      const targetOnDestroy = proto['ngOnDestroy'];

      proto['ngOnDestroy'] = function() {
        unsubscribeAll(this);
        (targetOnDestroy || { call: noop }).call(this);
      };

      proto[ON_DESTROY_EXTENDED] = true;
    }

    return {
      get() {
        return getValue(this, propertyKey);
      },
      set(value: Observable<T>) {
        invariant(likeObservable(value), `AutoSub accept only observable value, got ${typeof value}`);
        unsubscribe(this, propertyKey);
        subscribe(this, propertyKey, value);
      },
    };
  };
}
