import { ShareRequest } from './share-request';
import { forkJoin, timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { waitForAsync } from '@angular/core/testing';

const getRandomValue$ = () => timer(200).pipe(mapTo(Math.random()));

class MockService {
  requestWithoutSharing() {
    return getRandomValue$();
  }

  @ShareRequest
  requestWithoutParams() {
    return getRandomValue$();
  }

  @ShareRequest
  requestWithSimpleParams(arg1, arg2, arg3) {
    return getRandomValue$();
  }

  @ShareRequest
  requestWithComplexParams(arr, object) {
    return getRandomValue$();
  }
}

describe('ShareReplay', () => {
  it('Request without decorator will return different values', waitForAsync(() => {
    const service = new MockService();

    const firstRequest$ = service.requestWithoutSharing();
    const secondRequest$ = service.requestWithoutSharing();

    forkJoin(firstRequest$, secondRequest$).subscribe(([firstValue, secondValue]) =>
      expect(firstValue).not.toEqual(secondValue),
    );
  }));

  it('Should return the same request for requests without params', waitForAsync(() => {
    const service = new MockService();

    const firstRequest$ = service.requestWithoutParams();
    const secondRequest$ = service.requestWithoutParams();

    forkJoin(firstRequest$, secondRequest$).subscribe(([firstValue, secondValue]) =>
      expect(firstValue).toEqual(secondValue),
    );
  }));

  it('Should return the same request only for requests with the same simple params', waitForAsync(() => {
    const service = new MockService();

    const firstRequest$ = service.requestWithSimpleParams(123, 'string', false);
    const secondRequest$ = service.requestWithSimpleParams(123, 'string', false);
    const thirdRequest$ = service.requestWithSimpleParams(123, 'another string', false);

    forkJoin(firstRequest$, secondRequest$, thirdRequest$).subscribe(([firstValue, secondValue, thirdValue]) => {
      expect(firstValue).toEqual(secondValue);
      expect(firstValue).not.toEqual(thirdValue);
    });
  }));

  it('Should return the same request only requests with the same complex params', waitForAsync(() => {
    const service = new MockService();

    const firstRequest$ = service.requestWithComplexParams([1, 2, 3], { string: 'string', boolean: false });
    const secondRequest$ = service.requestWithComplexParams([1, 2, 3], { string: 'string', boolean: false });
    const thirdRequest$ = service.requestWithComplexParams([], { boolean: false });

    forkJoin(firstRequest$, secondRequest$, thirdRequest$).subscribe(([firstValue, secondValue, thirdValue]) => {
      expect(firstValue).toEqual(secondValue);
      expect(firstValue).not.toEqual(thirdValue);
    });
  }));
});
