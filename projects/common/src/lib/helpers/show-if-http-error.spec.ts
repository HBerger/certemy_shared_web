import { ShowIfHttpError } from './show-if-http-error';
import { of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpEventType, HttpHeaders } from '@angular/common/http';
import { waitForAsync } from '@angular/core/testing';
import { noop } from '@certemy/common';
import { HttpErrorResponse } from '@angular/common/http';

class HttpError {
  readonly errorType = 'HTTP_ERROR';
  error = true;
  status: number;
  body: any;
  handled = false;
  showIfNotHandled = 'Unknown error occurred. Please try again later.';

  static isHttpError(err: any): err is HttpError {
    return err instanceof HttpError;
  }

  constructor(error: HttpErrorResponse) {
    this.body = error.error;
    this.status = error.status;
  }
}

class ServiceWithHttpDep {
  http = {
    showIfHttpError() {
      return catchError(() => this.addError());
    },
    addError() {},
  };

  @ShowIfHttpError('Http error')
  getValueWithoutErrors() {
    return of('Response without errors');
  }

  @ShowIfHttpError('Http error')
  getValueWithErrorsMessage() {
    return throwError(
      new HttpError({
        error: '',
        status: 404,
        name: 'HttpErrorResponse',
        message: 'some error',
        ok: false,
        statusText: '',
        url: '',
        type: HttpEventType.Response,
        headers: new HttpHeaders(),
      }),
    );
  }
}

class ServiceWithoutHttpDep {
  @ShowIfHttpError('Http error')
  getValue() {
    return of('Response without errors');
  }
}

let service;
let serviceWithoutHttp;

describe('ShowIfHttpError', () => {
  beforeEach(() => {
    service = new ServiceWithHttpDep();
    serviceWithoutHttp = new ServiceWithoutHttpDep();
    spyOn(service.http, 'addError').and.returnValue(throwError('Error'));
  });

  it('Should not call showIfHttpError method of http service, if no errors during request', waitForAsync(() => {
    service.getValueWithoutErrors().subscribe();
    expect(service.http.addError).not.toHaveBeenCalled();
  }));

  it('Should call showIfHttpError method of http service, if errors  occur during request', waitForAsync(() => {
    service.getValueWithErrorsMessage().subscribe(noop, noop);
    expect(service.http.addError).toHaveBeenCalled();
  }));

  it('Should throw an error if service has no http service', waitForAsync(() => {
    try {
      serviceWithoutHttp.getValue().subscribe(noop, noop);
    } catch (error) {
      expect(error.message).toEqual('Please, provide http service.');
    }
  }));
});
