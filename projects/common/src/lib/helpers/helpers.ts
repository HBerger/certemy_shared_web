import { ResponseStatus } from '../constants';
import { filter, map, share, take } from 'rxjs/operators';
import { saveAs } from 'file-saver-es';
import { Observable, partition } from 'rxjs';

export function requestCompleted<T extends { status: ResponseStatus }>(
  observable$: Observable<T>,
): Observable<boolean> {
  return observable$.pipe(
    filter(req => [ResponseStatus.COMPLETE, ResponseStatus.ERROR].includes(req.status)),
    map(req => req.status === ResponseStatus.COMPLETE),
    take(1),
  );
}

export function capitalizeFirstChar(str: string): string {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export function saveFileFromUrl({ url, name }: { url: string; name: string }) {
  const link = document.createElement('a');
  link.style.display = 'none';
  link.href = url;
  link.download = name;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
}

export function saveFileFromBuffer({
  fileData,
  fileName,
  fileType,
}: {
  fileData: ArrayBuffer;
  fileName: string;
  fileType: string;
}) {
  const blob = new Blob([fileData], { type: fileType });
  saveAs(blob, fileName);
}

export function arrayBufferToJson(data: ArrayBuffer) {
  const decodedString = String.fromCharCode.apply(null, new Uint8Array(data));
  return JSON.parse(decodedString);
}

export function splitObservable<T>(
  source: Observable<T>,
  predicate: (value: T, index: number) => boolean,
): [Observable<T>, Observable<T>] {
  return partition(source.pipe(share()), predicate);
}

export function noop(...args: any[]): any {}

export function generateGuid() {
  return Math.floor(Date.now() + Math.random() * 1000000);
}
