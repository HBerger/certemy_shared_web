import { noop } from './helpers';
import { AutoSub } from './auto-sub';
import { of } from 'rxjs';
import { Subscribable } from 'rxjs';
import { Unsubscribable } from 'rxjs';
import { Subject } from 'rxjs';
import { OnDestroy } from '@angular/core';

class MockComponent {
  @AutoSub() prop;
  @AutoSub() prop2;
  @AutoSub() prop3;

  constructor(public injector) {}
}

class MockComponentWithOnDestroy implements OnDestroy {
  @AutoSub() prop;
  @AutoSub() prop2;
  @AutoSub() prop3;

  constructor(public injector) {}

  ngOnDestroy() {}
}

interface ObservableSpy extends Subscribable<any> {
  unsubSpy: jasmine.Spy;
  subSpy: jasmine.Spy;
}

function injectorSpyFactory() {
  const changeDetectorStub = {
    markForCheck: noop,
  };

  const injectorStub = {
    get: () => changeDetectorStub,
  };

  const getChangeDetectorSpy = spyOn(injectorStub, 'get').and.callThrough();
  const markForCheckSpy = spyOn(changeDetectorStub, 'markForCheck');

  return {
    get: injectorStub.get,
    getChangeDetectorSpy,
    markForCheckSpy,
  };
}

function observableSpyFactory(): ObservableSpy {
  const subscriptionStub: Unsubscribable = {
    unsubscribe: noop,
  };

  const observableStub: Subscribable<any> = {
    subscribe: () => subscriptionStub,
  };

  const unsubSpy = spyOn(subscriptionStub, 'unsubscribe');
  const subSpy = spyOn(observableStub, 'subscribe').and.callThrough();

  return {
    subscribe: observableStub.subscribe,
    unsubSpy,
    subSpy,
  };
}

describe('AutoSub', () => {
  afterEach(() => {
    const injectorSpy = injectorSpyFactory();
    const component = new MockComponent(injectorSpy);
    if (component.prop !== undefined) {
      throw new Error('No test should change initial value');
    }
  });

  it('Impossible to set non Observable value', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    expect(() => (mockComponent.prop = 'anyStr')).toThrowError();
    expect(() => (mockComponent.prop = {})).toThrowError();
    expect(() => (mockComponent.prop = () => {})).toThrowError();
  });

  it('Should check once for ChangeDetectorRef on value set', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    const { getChangeDetectorSpy } = injectorSpy;

    expect(getChangeDetectorSpy.calls.count()).toEqual(0);

    mockComponent.prop = of(null);
    expect(getChangeDetectorSpy.calls.count()).toEqual(1);

    mockComponent.prop2 = of(null);
    expect(getChangeDetectorSpy.calls.count()).toEqual(1);

    mockComponent.prop3 = of(null);
    mockComponent.prop3 = of(null);
    expect(getChangeDetectorSpy.calls.count()).toEqual(1);
  });

  it('Should unwrap Observable', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    mockComponent.prop = of(42);
    expect(mockComponent.prop).toEqual(42);

    mockComponent.prop2 = of(99);
    expect(mockComponent.prop2).toEqual(99);

    mockComponent.prop = of(666);
    expect(mockComponent.prop).toEqual(666);
  });

  it('Should unsubsribe from previous observable', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    const observableSpy = observableSpyFactory();
    const { subSpy, unsubSpy } = observableSpy;

    mockComponent.prop = observableSpy;
    expect(subSpy.calls.count()).toEqual(1);
    expect(unsubSpy.calls.count()).toEqual(0);

    const observableSpy2 = observableSpyFactory();
    mockComponent.prop2 = observableSpy2;
    expect(observableSpy2.subSpy.calls.count()).toEqual(1);
    expect(observableSpy2.unsubSpy.calls.count()).toEqual(0);

    mockComponent.prop = observableSpy;
    expect(subSpy.calls.count()).toEqual(2);
    expect(unsubSpy.calls.count()).toEqual(1);
  });

  it('Getter should not trigger sub/unsub', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    const observableSpy = observableSpyFactory();
    const { subSpy, unsubSpy } = observableSpy;

    mockComponent.prop = observableSpy;
    expect(subSpy.calls.count()).toEqual(1);
    expect(unsubSpy.calls.count()).toEqual(0);

    const _ = mockComponent.prop && mockComponent.prop && mockComponent.prop;

    expect(subSpy.calls.count()).toEqual(1);
    expect(unsubSpy.calls.count()).toEqual(0);
  });

  it('Should markForCheck when Observable emit value', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    const { markForCheckSpy } = injectorSpy;
    const subject = new Subject();

    mockComponent.prop = subject;
    expect(markForCheckSpy.calls.count()).toEqual(0);

    subject.next(1);
    expect(markForCheckSpy.calls.count()).toEqual(1);

    subject.next(2);
    subject.next(3);
    subject.next(4);
    expect(markForCheckSpy.calls.count()).toEqual(4);
  });

  it('Should be ok to call OnDestroy with no value seted', () => {
    const injectorSpy = injectorSpyFactory();

    const mockComponent = new MockComponent(injectorSpy);
    mockComponent['ngOnDestroy']();

    const mockComponentWithOnDestroy = new MockComponentWithOnDestroy(injectorSpy);
    mockComponentWithOnDestroy['ngOnDestroy']();
  });

  it('Should unsubscribe on OnDestroy. Component has no own OnDestroy.', () => {
    const injectorSpy = injectorSpyFactory();
    const mockComponent = new MockComponent(injectorSpy);

    const observableSpy = observableSpyFactory();
    const { unsubSpy } = observableSpy;

    mockComponent.prop = observableSpy;
    expect(unsubSpy.calls.count()).toEqual(0);

    mockComponent['ngOnDestroy']();
    expect(unsubSpy.calls.count()).toEqual(1);
  });

  it('Should unsubscribe on OnDestroy and call component OnDestroy. Component implements OnDestroy.', () => {
    const injectorSpy = injectorSpyFactory();
    const component = new MockComponentWithOnDestroy(injectorSpy);
    const onDestroySpy = spyOn(component, 'ngOnDestroy').and.callThrough();

    const observableSpy = observableSpyFactory();
    const { unsubSpy } = observableSpy;

    component.prop = observableSpy;
    expect(unsubSpy.calls.count()).toEqual(0);

    component['ngOnDestroy']();
    expect(unsubSpy.calls.count()).toEqual(1);
    expect(onDestroySpy.calls.count()).toEqual(1);
  });

  it('Should extend OnDestroy just once.', () => {
    const injectorSpy = injectorSpyFactory();

    const component1 = new MockComponentWithOnDestroy(injectorSpy);
    const onDestroySpy1 = spyOn(component1, 'ngOnDestroy').and.callThrough();
    const observableSpy1 = observableSpyFactory();
    component1.prop = observableSpy1;

    const component2 = new MockComponentWithOnDestroy(injectorSpy);
    const onDestroySpy2 = spyOn(component2, 'ngOnDestroy').and.callThrough();
    const observableSpy2 = observableSpyFactory();
    component2.prop = observableSpy2;

    component1['ngOnDestroy']();
    expect(observableSpy1.unsubSpy.calls.count()).toEqual(1);
    expect(onDestroySpy1.calls.count()).toEqual(1);

    expect(observableSpy2.unsubSpy.calls.count()).toEqual(0);
    expect(onDestroySpy2.calls.count()).toEqual(0);

    component2['ngOnDestroy']();
    expect(observableSpy1.unsubSpy.calls.count()).toEqual(1);
    expect(onDestroySpy1.calls.count()).toEqual(1);

    expect(observableSpy2.unsubSpy.calls.count()).toEqual(1);
    expect(onDestroySpy2.calls.count()).toEqual(1);
  });
});
