import { requestCompleted, splitObservable, capitalizeFirstChar } from './helpers';
import { TestScheduler } from 'rxjs/testing';
import { ResponseStatus } from '../constants';

describe('Helpers', () => {
  describe('requestCompleted', () => {
    let rxTest: TestScheduler;

    beforeEach(() => {
      rxTest = new TestScheduler(assertDeepEquals);
    });

    const requestStatuses = {
      I: { status: ResponseStatus.INITIAL },
      P: { status: ResponseStatus.PENDING },
      C: { status: ResponseStatus.COMPLETE },
      E: { status: ResponseStatus.ERROR },
    };
    const expectedValues = { T: true, F: false };

    it('should return true for successful request', () => {
      rxTest.run(({ cold, expectObservable }) => {
        const request$ = cold<{ status: ResponseStatus }>('---I---P---(C|)', requestStatuses);
        const expected = '                                 -----------(T|)';

        expectObservable(requestCompleted(request$)).toBe(expected, expectedValues);
      });
    });

    it('should return false for request with error', () => {
      rxTest.run(({ cold, expectObservable }) => {
        const request$ = cold<{ status: ResponseStatus }>('---I---P---(E|)', requestStatuses);
        const expected = '                                 -----------(F|)';

        expectObservable(requestCompleted(request$)).toBe(expected, expectedValues);
      });
    });

    it('should end after successful or error result was received', () => {
      rxTest.run(({ cold, expectObservable }) => {
        const request$ = cold<{ status: ResponseStatus }>('---I---P---E---', requestStatuses);
        const expected = '                                 -----------(F|)';

        expectObservable(requestCompleted(request$)).toBe(expected, expectedValues);
      });
    });
  });

  describe('splitObservable', () => {
    let rxTest: TestScheduler;

    beforeEach(() => {
      rxTest = new TestScheduler(assertDeepEquals);
    });

    it('should return two observables, with values splitted based on predicate', () => {
      rxTest.run(({ cold, expectObservable }) => {
        const source = cold('--a--a--b-|');
        const expectedA = '  --a--a----|';
        const expectedB = '  --------b-|';
        const [sourceA, sourceB] = splitObservable(source, value => value === 'a');

        expectObservable(sourceA).toBe(expectedA);
        expectObservable(sourceB).toBe(expectedB);
      });
    });
  });

  describe('capitalizeFirstChar', () => {
    it('Should capitalize only first char in the string', () => {
      expect(capitalizeFirstChar('hello world')).toBe('Hello world');
    });
  });
});

function assertDeepEquals(a: any, b: any) {
  expect(a).toEqual(b);
}
