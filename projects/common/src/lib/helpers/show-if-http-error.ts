export function ShowIfHttpError(errorMessage: string = '') {
  return function(target, key, descriptor) {
    const originalMethod = descriptor.value;
    const getRequestObservableFn = function(...args) {
      if (this.http && this.http.showIfHttpError) {
        return originalMethod.apply(this, args).pipe(this.http.showIfHttpError(errorMessage));
      } else {
        throw new Error('Please, provide http service.');
      }
    };

    descriptor.value = getRequestObservableFn;
    return descriptor;
  };
}
