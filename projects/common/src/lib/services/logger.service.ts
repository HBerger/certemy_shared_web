import { Inject, Injectable, Optional, InjectionToken } from '@angular/core';
import { ENVIRONMENT } from '../environment.token';
import { Environment } from '../environment.interface';
import * as Sentry from "@sentry/angular-ivy";
import _merge from 'lodash-es/merge';
import { ScopeContext } from '@sentry/types/types/scope';

export type LoggerOptions = Sentry.BrowserOptions;
export const LOGGER_OPTIONS = new InjectionToken('LOGGER_OPTIONS');

// @dynamic
@Injectable()
export class LoggerService {
  private isSentryEnabled = false;
  private options: Partial<ScopeContext>;

  constructor(
    @Inject(ENVIRONMENT) private environment: Environment,
    @Inject(LOGGER_OPTIONS)
    @Optional()
    private loggerOptions: LoggerOptions,
  ) {
    if (this.environment.sentryKey) {
      Sentry.init({
        ...(this.loggerOptions || {}),
        dsn: environment.sentryKey,
        environment: this.environment.name,
        release: this.environment.release,
        autoSessionTracking: false,
      });
      this.isSentryEnabled = true;
      this.options = {
        tags: {
          component: 'Frontend',
          git_commit: this.environment.GIT_COMMIT,
        },
      };
    }
  }

  info(message, options?: Partial<ScopeContext>) {
    if (!this.isSentryEnabled) {
      return;
    }

    Sentry.captureException(message, _merge(this.options, options, { level: 'info' }));
  }

  warning(message, options?: Partial<ScopeContext>) {
    if (!this.isSentryEnabled) {
      return;
    }

    Sentry.captureException(message, _merge(this.options, options, { level: 'warning' }));
  }

  error(message, options?: Partial<ScopeContext>) {
    if (!this.isSentryEnabled) {
      return;
    }

    Sentry.captureException(message, _merge(this.options, options, { level: 'error' }));
  }

  captureException(exception: Error, options?: Partial<ScopeContext>) {
    if (!this.isSentryEnabled) {
      return;
    }

    Sentry.captureException(exception, _merge(this.options, options));
  }

  setUserContext(user?: { profile_id: number }) {
    if (!this.isSentryEnabled) {
      return;
    }

    Sentry.setUser({ id: user?.profile_id })
  }
}
