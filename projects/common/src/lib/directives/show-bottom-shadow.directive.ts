import { AfterViewInit, Directive, ElementRef, Input, OnDestroy, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[showBottomShadow]',
})
export class ShowBottomShadowDirective implements OnInit, AfterViewInit, OnDestroy {
  @Input()
  set targetData(_data: any) {
    if (this.bottomShadowElement) {
      setTimeout(this.applyStyles, 0);
    }
  }
  @Input() private bottomShadowElement: HTMLElement;
  private previousState: boolean;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    this.applyStyles = this.applyStyles.bind(this);
  }

  public ngOnInit(): void {
    this.bottomShadowElement.addEventListener('scroll', this.applyStyles);
  }

  public ngAfterViewInit(): void {
    setTimeout(this.applyStyles, 0);
  }

  public ngOnDestroy(): void {
    this.bottomShadowElement.removeEventListener('scroll', this.applyStyles);
  }

  private applyStyles(): void {
    const isScrollbar =
      this.bottomShadowElement.scrollHeight - this.bottomShadowElement.scrollTop >
      this.bottomShadowElement.offsetHeight;
    if (this.previousState === isScrollbar) {
      return;
    }
    this.renderer.setStyle(this.elementRef.nativeElement, 'visibility', isScrollbar ? 'visible' : 'hidden');
    this.previousState = isScrollbar;
  }
}
