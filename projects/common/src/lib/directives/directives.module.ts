import { NgModule } from '@angular/core';

import { LetDirective } from './let.directive';
import { ScrollTopDirective } from './scroll-top.directive';
import { ClickOutsideDirective } from './click-outside.directive';
import { InfiniteScrollDirective } from './infinite-scroll.directive';
import { DndAutoScrollDirective } from './dnd-auto-scroll.directive';
import { AutoHeightDirective } from './auto-height.directive';
import { AutoFocusDirective } from './auto-focus.directive';
import { DisableControlDirective } from './disable-control.directive';
import { OverflowTooltipDirective } from './overflow-tooltip.directive';
import { AssigneeColumnValueDirective } from './assignee-column.directive';
import { ShowBottomShadowDirective } from './show-bottom-shadow.directive';
import { StopPropagationDirective } from './stop-propagation';
import { NgxCardModule } from './card/ngx-card';


const directives = [
  LetDirective,
  ScrollTopDirective,
  ClickOutsideDirective,
  InfiniteScrollDirective,
  DndAutoScrollDirective,
  AutoHeightDirective,
  AutoFocusDirective,
  DisableControlDirective,
  OverflowTooltipDirective,
  AssigneeColumnValueDirective,
  ShowBottomShadowDirective,
  StopPropagationDirective,
];

@NgModule({
  imports: [NgxCardModule],
  declarations: directives,
  providers: [],
  exports: [directives, NgxCardModule],
})
export class DirectivesModule {}
