import { Directive, ElementRef, Input, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Directive({
  selector: '[cmScrollTop]',
})
export class ScrollTopDirective implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  private scrollTop$ = new Subject<Observable<void>>();
  @Input()
  set cmScrollTop(observable$: Observable<void>) {
    this.scrollTop$.next(observable$);
  }
  constructor(private el: ElementRef) {
    this.scrollTop$
      .pipe(
        switchMap(observable$ => observable$),
        takeUntil(this.onDestroy$),
      )
      .subscribe(() => (this.el.nativeElement.scrollTop = 0));
  }
  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
