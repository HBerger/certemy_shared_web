import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';
import _get from 'lodash-es/get';
import isFunction from 'lodash-es/isFunction';

@Directive({
  selector: '[cmDisableControl]',
})
export class DisableControlDirective {
  @Input()
  set cmDisableControl(isDisable: boolean) {
    const state = isDisable ? 'disable' : 'enable';
    const action = _get(this.ngControl, `control[${state}]`);

    if (isFunction(action)) {
      this.ngControl.control[state]();
    }
  }

  constructor(private ngControl: NgControl) {}
}
