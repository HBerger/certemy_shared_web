import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import _get from 'lodash-es/get';

@Directive({ selector: '[cmAutoFocus]' })
export class AutoFocusDirective implements OnInit {
  private autoFocus = true;

  @Input()
  set cmAutoFocus(value: boolean) {
    this.autoFocus = coerceBooleanProperty(value);
  }

  constructor(private elRef: ElementRef) {}

  ngOnInit(): void {
    if (this.autoFocus) {
      setTimeout(() => {
        const nativeElement = (typeof _get(this.elRef.nativeElement, 'firstElementChild.focus') === 'function')
          ? this.elRef.nativeElement.firstElementChild
          : this.elRef.nativeElement;
        nativeElement.focus();
      });
    }
  }
}
