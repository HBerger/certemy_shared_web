import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[assigneeColumnValue]',
})
export class AssigneeColumnValueDirective {
  @Input() isAssigned: boolean;

  constructor(public elementRef: ElementRef) {}

  @HostListener('mouseenter')
  onMouseEnter() {
    this.elementRef.nativeElement.classList.add(this.isAssigned ? 'unassign-state' : 'assign-state');
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.elementRef.nativeElement.classList.remove(this.isAssigned ? 'unassign-state' : 'assign-state');
  }
}
