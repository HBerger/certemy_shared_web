import { Directive, ElementRef, Output, EventEmitter, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[cmClickOutside]',
})
export class ClickOutsideDirective {
  clickCount = 0;
  constructor(private element: ElementRef) {}

  // skipFirstClick in order to not catch the first click which component initialization triggered
  @Input() skipFirstClick = true;
  @Output() cmClickOutside = new EventEmitter();

  @HostListener('document:click', ['$event.target'])
  onClick(targetElement) {
    this.clickCount++;

    if (!this.skipFirstClick || this.clickCount !== 1) {
      const clickedInside = this.element.nativeElement.contains(targetElement);

      if (!clickedInside) {
        this.cmClickOutside.emit(targetElement);
      }
    }
  }
}
