import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[stopPropagationOnClick]',
})
export class StopPropagationDirective {
  @HostListener('click', ['$event'])
  onMouseClick($event) {
    $event.stopPropagation();
  }
}
