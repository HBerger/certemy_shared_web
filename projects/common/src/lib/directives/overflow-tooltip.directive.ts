import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';

@Directive({
  selector: '[overflowTooltip]',
})
export class OverflowTooltipDirective {
  @Input() public overflowTooltip: NgbTooltip;

  constructor(private elementRef: ElementRef) {}

  @HostListener('mouseover')
  public openTooltip(): void {
    if (this.elementRef.nativeElement.offsetWidth < this.elementRef.nativeElement.scrollWidth) {
      this.overflowTooltip.open();
    }
  }

  @HostListener('mouseleave')
  public closeTooltip(): void {
    this.overflowTooltip.close();
  }
}
