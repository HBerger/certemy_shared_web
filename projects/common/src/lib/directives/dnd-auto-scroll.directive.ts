import { Directive, ElementRef, OnInit, Input, OnDestroy } from '@angular/core';
import _get from 'lodash-es/get';

interface ExtendedObserver {
  mutationObserver: MutationObserver;
  temporal: boolean;
}

const MIN_INDENT = 50;

@Directive({
  selector: '[cmDndAutoScroll]',
})
export class DndAutoScrollDirective implements OnInit, OnDestroy {
  @Input() scrollStep = 15;
  private observers: ExtendedObserver[] = [];

  constructor(private element: ElementRef) {}

  ngOnInit() {
    this.initDraggableItemObservable();
  }

  ngOnDestroy() {
    this.unsubscribe();
  }

  private initDraggableItemObservable() {
    const draggableItemClassName = 'gu-mirror';
    const callback = mutationsList => {
      mutationsList.forEach(mutation => {
        const addedNode = _get(mutation, 'addedNodes[0]') as Node;
        const addedNodeClassName = _get(mutation, 'addedNodes[0].className', '');
        const removedNodeClassName = _get(mutation, 'removedNodes[0].className', '');

        if (removedNodeClassName.includes(draggableItemClassName)) {
          this.unsubscribeTemporalObservables();
          return;
        }

        if (addedNodeClassName.includes(draggableItemClassName)) {
          this.initDraggableItemPositionObservable(addedNode);
        }
      });
    };

    this.initMutationObservables({ childList: true }, callback, document.body);
  }

  private initDraggableItemPositionObservable(element: Node) {
    const callback = mutationsList => {
      mutationsList.forEach(mutation => {
        if (mutation.attributeName !== 'style') {
          return;
        }

        const elementRects = this.element.nativeElement.getClientRects()[0];
        const draggableItemTop = parseInt(mutation.target.style.top, 10);
        const topIndent = draggableItemTop - elementRects.top;
        const bottomIndent = elementRects.bottom - draggableItemTop;

        if (this.isScrollRequired(topIndent)) {
          this.scrollElement(true);
          return;
        }

        if (this.isScrollRequired(bottomIndent)) {
          this.scrollElement();
          return;
        }
      });
    };

    this.initMutationObservables({ attributes: true }, callback, element, { temporal: true });
  }

  private isScrollRequired(indent: number): boolean {
    return indent < MIN_INDENT;
  }

  private scrollElement(scrollTop = false) {
    const currentScrollPosition = this.element.nativeElement.scrollTop;

    this.element.nativeElement.scrollTop = scrollTop
      ? currentScrollPosition - this.scrollStep
      : currentScrollPosition + this.scrollStep;
  }

  private initMutationObservables(
    { attributes = false, childList = false, subtree = false },
    callback,
    targetNode: Node,
    { temporal = false } = {},
  ) {
    const config = { attributes, childList, subtree };
    const mutationObserver = new MutationObserver(callback);

    this.observers.push({ mutationObserver, temporal });
    mutationObserver.observe(targetNode, config);
  }

  private unsubscribe() {
    this.observers.forEach((observer: ExtendedObserver) => observer.mutationObserver.disconnect());
    this.observers = [];
  }

  private unsubscribeTemporalObservables() {
    this.observers = this.observers.filter((observer: ExtendedObserver) => {
      if (observer.temporal) {
        observer.mutationObserver.disconnect();

        return false;
      }

      return true;
    });
  }
}
