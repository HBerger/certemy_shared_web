import { Directive, ElementRef, HostListener, OnInit, Renderer2 } from '@angular/core';

enum NODE_NAMES {
  TEXTAREA = 'TEXTAREA',
}

@Directive({ selector: '[cmAutoHeight]' })
export class AutoHeightDirective implements OnInit {
  private nativeElement;

  constructor(private elRef: ElementRef, private renderer: Renderer2) {}

  @HostListener('input', ['$event'])
  onPaste() {
    this.adjustHeight();
  }

  ngOnInit() {
    setTimeout(() => {
      this.nativeElement = this.elRef.nativeElement.firstElementChild
        ? this.elRef.nativeElement.firstElementChild
        : this.elRef.nativeElement;

      if (this.isSpecificNodeName(NODE_NAMES.TEXTAREA)) {
        this.adjustTextArea();
      }

      this.adjustHeight();
    });
  }

  private adjustHeight() {
    this.renderer.setStyle(this.nativeElement, 'height', 'auto');
    this.renderer.setStyle(this.nativeElement, 'height', `${this.nativeElement.scrollHeight}px`);
  }

  private adjustTextArea() {
    this.nativeElement.rows = 1;
    this.renderer.setStyle(this.nativeElement, 'min-height', '34px');
    this.renderer.setStyle(this.nativeElement, 'overflow', 'hidden');
    this.renderer.setStyle(this.nativeElement, 'resize', 'none');
  }

  private isSpecificNodeName(nodeName: NODE_NAMES): boolean {
    return this.nativeElement.nodeName === nodeName;
  }
}
