import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

const INFINITE_SCROLL_TRIGGER_PERCENT = 80;

@Directive({
  selector: '[cmInfiniteScroll]',
})
export class InfiniteScrollDirective {
  @Output() load = new EventEmitter();

  @HostListener('scroll', ['$event'])
  private scroll(event) {
    const { scrollTop, scrollHeight, clientHeight } = event.target;
    const lowestScrollTop = scrollHeight - clientHeight;
    const triggerScrollTop = (INFINITE_SCROLL_TRIGGER_PERCENT / 100) * lowestScrollTop;

    if (scrollTop > triggerScrollTop) {
      this.load.emit();
    }
  }
}
