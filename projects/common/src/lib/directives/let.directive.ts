import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({ selector: '[cmLet]' })
export class LetDirective {
  constructor(private viewContainerRef: ViewContainerRef, private templateRef: TemplateRef<null>) {}

  @Input()
  set cmLet(value) {
    const view: any = this.viewContainerRef.get(0);

    if (view && view.context) {
      view.context.cmLet = value;
    } else {
      this.viewContainerRef.clear();
      this.viewContainerRef.createEmbeddedView(this.templateRef, { cmLet: value });
    }
  }
}
