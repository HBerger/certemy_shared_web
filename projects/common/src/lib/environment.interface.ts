export interface Environment {
  sentryKey?: string;
  GIT_COMMIT: string;
  name: string;
  release: string;
}
