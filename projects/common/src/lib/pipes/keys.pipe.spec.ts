import { KeysPipe } from './keys.pipe';

describe('KeysPipe', () => {
  const keysPipe = new KeysPipe();

  it('Should return object keys as array', () => {
    expect(keysPipe.transform({ a: 1, b: 2 })).toEqual(['a', 'b']);
  });
});
