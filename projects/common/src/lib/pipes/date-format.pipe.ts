import { Pipe, PipeTransform } from '@angular/core';
import { DateFormats } from '../constants';
import moment from 'moment-mini';

@Pipe({
  name: 'dateFormat',
})
export class DateFormatPipe implements PipeTransform {
  // inUtc - use when you need to apply format without using the browser timezone
  transform(value, formatKey: string = 'defaultFormat', inUtc: boolean = false) {
    const format = DateFormats[formatKey];

    if (!value) {
      console.error(`Value for dateFormat should not be falsy: ${value}`);
      return '';
    }

    if (inUtc) {
      return moment.utc(value).format(format);
    }

    return moment(value).format(format);
  }
}
