import { Pipe, PipeTransform } from '@angular/core';
import { MINUTE, HOUR } from '../constants/time.constants';

@Pipe({
  name: 'time',
})
export class TimePipe implements PipeTransform {
  private readonly hoursPhrase = 'hours';
  private readonly hourPhrase = 'hour';
  private readonly minutesPhrase = 'min';

  transform(value: number, format: 'default' | 'timeonly' = 'default'): string {
    if (!Number.isFinite(value)) {
      return 'N/A';
    }

    switch (format) {
      case 'timeonly':
        return this.timeonlyFormatTime(value);
      default:
        return this.defaultFormatTime(value);
    }
  }

  private timeonlyFormatTime(value: number) {
    const time = this.parseTime(value);
    return `${time.hours}:${this.formatMinute(time.minutes)}`;
  }

  private formatMinute(minutes: number): string {
    if (minutes === 0) {
      return '00';
    } else if (minutes < 10) {
      return `0${minutes}`;
    } else {
      return minutes.toString();
    }
  }

  private parseTime(value: number): { hours: number; timeRemaining: number; minutes: number } {
    const hours = Math.floor(value / HOUR);
    const timeRemaining = value - hours * HOUR;
    const minutes = Math.round(timeRemaining / MINUTE);

    return {
      hours: hours,
      timeRemaining: timeRemaining,
      minutes: minutes,
    };
  }

  private defaultFormatTime(value: number) {
    if (value < MINUTE) {
      return `0 ${this.hoursPhrase}`;
    }

    let string = '';
    const time = this.parseTime(value);

    if (time.hours) {
      string += `${time.hours} ${time.hours === 1 ? this.hourPhrase : this.hoursPhrase}`;
    }

    if (time.minutes) {
      string += time.hours ? ` ${time.minutes} ${this.minutesPhrase}` : `${time.minutes} ${this.minutesPhrase}`;
    }

    return string;
  }
}
