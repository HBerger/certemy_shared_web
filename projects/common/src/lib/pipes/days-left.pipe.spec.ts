import { DaysLeftPipe } from './days-left.pipe';
import moment from 'moment-mini';

describe('DaysLeftPipe', () => {
  const daysLeftPipe = new DaysLeftPipe();

  it("Should return days left if it's more than 1 day left", () => {
    const moreThanOneDayLeft = moment()
      .add(1, 'day')
      .add(2, 'hours');
    expect(daysLeftPipe.transform(moreThanOneDayLeft)).toBe('(2 days left)');
  });

  it("Should return 1 day left if it's more then 23 hours left", () => {
    const moreThen23Hours = moment().add(23.5, 'hours');
    expect(daysLeftPipe.transform(moreThen23Hours)).toBe('(1 day left)');
  });

  it("Should return hours left if it's more than 1 hour left", () => {
    const moreThenOneHourLeft = moment().add(1.2, 'hour');
    expect(daysLeftPipe.transform(moreThenOneHourLeft)).toBe('(2 hours left)');
  });

  it("Should return 1 hour left if it's more then 59 minutes left", () => {
    const moreThen59Minutes = moment().add(59.5, 'minutes');
    expect(daysLeftPipe.transform(moreThen59Minutes)).toBe('(1 hour left)');
  });

  it("Should return minutes left if it's more than 1 minute left", () => {
    const moreThenOneMinuteLeft = moment().add(1.2, 'minute');
    expect(daysLeftPipe.transform(moreThenOneMinuteLeft)).toBe('(2 minutes left)');
  });

  it("Should return 1 minute left if it's less then 1 minute left", () => {
    const secondsLeft = moment().add(50, 'seconds');
    expect(daysLeftPipe.transform(secondsLeft)).toBe('(1 minute left)');
  });

  it('Should return empty string if date has passed', () => {
    expect(daysLeftPipe.transform(moment())).toBe('');
  });
});
