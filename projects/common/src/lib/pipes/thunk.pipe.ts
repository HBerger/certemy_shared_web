import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thunk',
})
export class ThunkPipe implements PipeTransform {
  transform<T>(fn: (...fnArgs: any[]) => T, ...args: any[]): T {
    return fn(...args);
  }
}
