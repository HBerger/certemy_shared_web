import { SortByOrderPipe } from './sort-by-order.pipe';

describe('SortByOrderPipe', () => {
  const sortByOrderPipe = new SortByOrderPipe();

  it('Should sort array by order property', () => {
    const unorderedList = [{ order: 1 }, { order: 3 }, { order: 2 }];
    expect(sortByOrderPipe.transform(unorderedList)).toEqual([{ order: 1 }, { order: 2 }, { order: 3 }]);
  });
});
