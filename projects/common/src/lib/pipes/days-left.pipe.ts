import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment-mini';

@Pipe({
  name: 'daysLeft',
})
export class DaysLeftPipe implements PipeTransform {
  transform(value) {
    const dayLeft = moment(value).diff(moment(), 'd', true);
    let message;

    if (dayLeft > 1) {
      message = `(${Math.ceil(dayLeft)} days left)`;
    } else {
      const hoursLeft = moment(value).diff(moment(), 'h', true);

      if (hoursLeft > 1) {
        const roundedHoursLeft = Math.ceil(hoursLeft);

        if (roundedHoursLeft === 24) {
          message = `(1 day left)`;
        } else {
          message = `(${roundedHoursLeft} hours left)`;
        }
      } else {
        const minutesLeft = moment(value).diff(moment(), 'm', true);

        if (minutesLeft > 1) {
          const roundedMinutesLeft = Math.ceil(minutesLeft);

          if (roundedMinutesLeft === 60) {
            return `(1 hour left)`;
          } else {
            message = `(${roundedMinutesLeft} minutes left)`;
          }
        } else if (minutesLeft <= 0) {
          message = '';
        } else {
          message = '(1 minute left)';
        }
      }
    }

    return message;
  }
}
