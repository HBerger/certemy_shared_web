import { Pipe, PipeTransform } from '@angular/core';

interface OrderedItem {
  order: number;
}

@Pipe({
  name: 'sortByOrder',
})
export class SortByOrderPipe implements PipeTransform {
  transform<T extends OrderedItem>(array: T[]): T[] {
    return array.sort((a: T, b: T) => {
      if (a.order < b.order) {
        return -1;
      } else if (a.order > b.order) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
