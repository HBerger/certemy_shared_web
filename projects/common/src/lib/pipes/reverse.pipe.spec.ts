import { ReversePipe } from './reverse.pipe';

describe('ReversePipe', () => {
  const reversePipe = new ReversePipe();

  it('Should reverse array items', () => {
    expect(reversePipe.transform([1, 2])).toEqual([2, 1]);
  });
});
