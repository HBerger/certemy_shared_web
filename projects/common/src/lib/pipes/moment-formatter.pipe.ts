import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment-mini';

/** @deprecated - use DateFormat instead */
@Pipe({
  name: 'momentFormatter',
})
export class MomentFormatterPipe implements PipeTransform {
  transform(value, format?: string, utcToLocal?: boolean) {
    format = format || 'll';

    if (!value) {
      // With empty value moment show the current date-time
      console.error(`Value for momentFormatter should not be falsy: ${value}`);
    }

    if (utcToLocal) {
      return moment
        .utc(value)
        .local()
        .format(format);
    }

    return moment(value).format(format);
  }
}
