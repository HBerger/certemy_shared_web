import { CurrencyPipe } from './currency.pipe';

describe('CurrencyPipe', () => {
  const currencyPipe = new CurrencyPipe();

  it('Should transform number to currency string', () => {
    expect(currencyPipe.transform(12)).toBe('$12.00');
  });

  it('Should round up to 2 decimals after dot', () => {
    expect(currencyPipe.transform(12.3456)).toBe('$12.35');
  });
});
