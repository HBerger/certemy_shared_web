import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThunkPipe } from './thunk.pipe';
import { CurrencyPipe } from './currency.pipe';
import { DaysLeftPipe } from './days-left.pipe';
import { KeysPipe } from './keys.pipe';
import { MomentFormatterPipe } from './moment-formatter.pipe';
import { DateFormatPipe } from './date-format.pipe';
import { ReversePipe } from './reverse.pipe';
import { SafePipe } from './safe.pipe';
import { SortByOrderPipe } from './sort-by-order.pipe';
import { TimePipe } from './time.pipe';

const pipes = [
  CurrencyPipe,
  DaysLeftPipe,
  KeysPipe,
  MomentFormatterPipe,
  DateFormatPipe,
  ReversePipe,
  SafePipe,
  SortByOrderPipe,
  ThunkPipe,
  TimePipe,
];

@NgModule({
  imports: [CommonModule],
  declarations: pipes,
  exports: pipes,
})
export class PipesModule {}
