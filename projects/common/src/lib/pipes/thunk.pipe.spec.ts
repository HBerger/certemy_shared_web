import { ThunkPipe } from './thunk.pipe';

describe('ThunkPipe', () => {
  const thunkPipe = new ThunkPipe();

  it('Should execute function with passed arguments', () => {
    const sum = (a, b) => a + b;
    expect(thunkPipe.transform(sum, 2, 4)).toBe(6);
  });

  it('Should execute function without passed arguments', () => {
    const simpleReturn = () => 42;
    expect(thunkPipe.transform(simpleReturn)).toBe(42);
  });
});
