import { TimePipe } from './time.pipe';
import { MINUTE, HOUR, SECOND } from '../constants/time.constants';

describe('TimePipe', () => {
  const timePipe = new TimePipe();

  it('Should display N/A if non number value was passed', () => {
    expect(timePipe.transform(null)).toBe('N/A');
  });

  it('Should display time in default format', () => {
    expect(timePipe.transform(10 * SECOND)).toBe('0 hours');
    expect(timePipe.transform(5 * HOUR)).toBe('5 hours');
    expect(timePipe.transform(HOUR)).toBe('1 hour');
    expect(timePipe.transform(6 * HOUR + 15 * MINUTE)).toBe('6 hours 15 min');
  });

  it('Should display time in "timeonly" format', () => {
    expect(timePipe.transform(0, 'timeonly')).toBe('0:00');
    expect(timePipe.transform(5 * MINUTE, 'timeonly')).toBe('0:05');
    expect(timePipe.transform(16 * HOUR + 45 * MINUTE, 'timeonly')).toBe('16:45');
  });
});
